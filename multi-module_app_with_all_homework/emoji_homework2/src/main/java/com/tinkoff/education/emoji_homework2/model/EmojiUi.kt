package com.tinkoff.education.emoji_homework2.model

data class Emoji(
    val name: String,
    val code: String
) {
    fun getCodeString() = String(Character.toChars(code.toInt(16)))
}

val emojiList = listOf(
    Emoji("rolling_eyes", "1f644"),
    Emoji("smile", "1f642"),
    Emoji("sweat_smile", "1f605")
)