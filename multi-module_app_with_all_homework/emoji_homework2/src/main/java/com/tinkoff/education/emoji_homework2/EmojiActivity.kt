package com.tinkoff.education.emoji_homework2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tinkoff.education.emoji_homework2.databinding.ActivityEmojiBinding
import com.tinkoff.education.emoji_homework2.model.Emoji
import com.tinkoff.education.emoji_homework2.model.emojiList
import com.tinkoff.education.emoji_homework2.view.EmojiView
import kotlin.random.Random

class EmojiActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEmojiBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmojiBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        observeButton()
    }

    private fun observeButton() {
        demoTask1()
        demoTask2()
        demoTask3()
    }

    private fun demoTask1() {
        binding.emoji.emojiCounter = 13
        val emoji: Emoji = emojiList[2]
        binding.emoji.emoji = emoji.getCodeString()

        binding.emoji.setOnClickListener {
            it.isSelected = !it.isSelected
        }
    }

    private fun demoTask2() {
        binding.addEmojiTask2.setOnClickListener {
            binding.flexBoxLayout.addView(getRandomEmoji(), 0)
        }
    }

    private fun demoTask3() {
        binding.addName.setOnClickListener {
            binding.messageCardView.setName("New Name:!")
        }
        binding.addMessage.setOnClickListener {
            binding.messageCardView.setMessage("Андроид - это опенсорсная операционная система, основанная на ядре Linux, которая была первоначально разработана компанией Android Inc. до того, как Google приобрел её в 2005 году.")
        }
        binding.addEmoji.setOnClickListener {
            val emojiView: EmojiView = getRandomEmoji()
            emojiView.setOnClickListener { emoji -> emoji.isSelected = !emoji.isSelected }
            binding.messageCardView.addEmoji(emojiView)
        }
        binding.addImage.setOnClickListener {
            binding.messageCardView.setAvatar(R.drawable.jaskier)
        }
    }

    private fun getRandomEmoji(): EmojiView {
        val randomIndex = Random.nextInt(emojiList.size)
        val emoji = EmojiView(this, defTheme = R.style.EmojiStyle)
        emoji.emoji = emojiList[randomIndex].getCodeString()
        emoji.emojiCounter = Random.nextInt(99)
        return emoji
    }
}