package com.tinkoff.education.contacts_homework1.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Contact(
    val id: String,
    val name: String,
    val hasPhone: Int,
    val phoneNumber: String
): Parcelable