package com.tinkoff.education.contacts_homework1.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.tinkoff.education.contacts_homework1.SecondActivity
import com.tinkoff.education.contacts_homework1.model.Contact

class MyBroadcastReceiver(private val sendListOfContacts: (list: ArrayList<Contact>) -> Unit) : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val listOfContacts: ArrayList<Contact>? = intent?.getParcelableArrayListExtra(SecondActivity.EXTRA_LIST_OF_CONTACTS)
        listOfContacts?.let { sendListOfContacts.invoke(it) }
    }
}