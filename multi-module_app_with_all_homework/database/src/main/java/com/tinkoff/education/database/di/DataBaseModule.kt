package com.tinkoff.education.database.di

import android.content.Context
import androidx.room.Room
import com.tinkoff.education.database.MessengerDatabase
import com.tinkoff.education.database.dao.MessageDao
import com.tinkoff.education.database.dao.ReactionDao
import com.tinkoff.education.database.dao.StreamDao
import com.tinkoff.education.database.dao.TopicDao
import com.tinkoff.education.database.dao.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DataBaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(appContext: Context): MessengerDatabase {
        return Room.databaseBuilder(
            appContext,
            MessengerDatabase::class.java,
            "messenger_database"
        ).build()
    }

    @Singleton
    @Provides
    fun provideUserDao(dataBase: MessengerDatabase): UserDao =
        dataBase.userDao

    @Singleton
    @Provides
    fun provideStreamDao(dataBase: MessengerDatabase): StreamDao =
        dataBase.streamDao

    @Singleton
    @Provides
    fun provideTopicDao(dataBase: MessengerDatabase): TopicDao =
        dataBase.topicDao

    @Singleton
    @Provides
    fun provideMessageDao(dataBase: MessengerDatabase): MessageDao =
        dataBase.messageDao

    @Singleton
    @Provides
    fun provideReactionDao(dataBase: MessengerDatabase): ReactionDao =
        dataBase.reactionDao
}