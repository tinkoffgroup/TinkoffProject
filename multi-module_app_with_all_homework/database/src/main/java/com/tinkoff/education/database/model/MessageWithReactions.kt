package com.tinkoff.education.database.model

import androidx.room.Embedded
import androidx.room.Relation

data class MessageWithReactions(
    @Embedded val message: MessageTable,
    @Relation(
        parentColumn = "messageId",
        entityColumn = "messageId"
    )
    val reactions: List<ReactionTable>
)
