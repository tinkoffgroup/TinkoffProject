package com.tinkoff.education.database

import androidx.paging.PagingSource
import androidx.room.withTransaction
import com.tinkoff.education.database.dao.MessageDao
import com.tinkoff.education.database.dao.ReactionDao
import com.tinkoff.education.database.dao.StreamDao
import com.tinkoff.education.database.dao.TopicDao
import com.tinkoff.education.database.dao.UserDao
import com.tinkoff.education.database.model.AllStreamTable
import com.tinkoff.education.database.model.MessageTable
import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.database.model.ReactionTable
import com.tinkoff.education.database.model.StreamEntity
import com.tinkoff.education.database.model.SubscribedStreamTable
import com.tinkoff.education.database.model.TopicTable
import com.tinkoff.education.database.model.UserTable
import javax.inject.Inject

class DataBaseDataSource @Inject constructor(
    private val database: MessengerDatabase,
    private val userDao: UserDao,
    private val streamDao: StreamDao,
    private val topicDao: TopicDao,
    private val messageDao: MessageDao,
    private val reactionDao: ReactionDao
) {

    suspend fun saveMessagesAndReactions(messages: List<MessageTable>, reactions: List<ReactionTable>) {
        database.withTransaction {
            messageDao.saveMessages(messages)
            reactionDao.saveReactions(reactions)
        }
    }

    suspend fun deleteMessagesAndReactions(hashCodeOfStreamsAndTopics: Int) {
        database.withTransaction {
            messageDao.deleteMessages(hashCodeOfStreamsAndTopics)
            reactionDao.deleteReactions(hashCodeOfStreamsAndTopics)
        }
    }

    fun getMessages(hashCodeOfStreamsAndTopics: Int): PagingSource<Int, MessageWithReactions> {
        return messageDao.getMessages(hashCodeOfStreamsAndTopics)
    }

    suspend fun deleteStreams(amISubscribed: Boolean) {
        val listOfId = if (amISubscribed) streamDao.getSubscribedStreams().map { it.id } else streamDao.getAllStreams().map { it.id }
        topicDao.deleteTopicsByStreamIds(listOfId)
        if (amISubscribed) streamDao.deleteSubscribedStreams() else streamDao.deleteAllStreams()
    }

    suspend fun getTopicsByStream(streamName: String): List<TopicTable> {
        return topicDao.getTopicsByStream(streamName)
    }

    suspend fun getStreams(amISubscribed: Boolean): List<StreamEntity> {
        val topics = topicDao.getTopics()
        val streams = if (amISubscribed) streamDao.getSubscribedStreams() else streamDao.getAllStreams()

        return streams.map { stream ->
            if (stream is AllStreamTable) {
                val streamTopics = topics.filter { topic -> topic.streamId == stream.id }
                StreamEntity(stream.id, stream.title, stream.isOpened, false, streamTopics)
            } else {
                val stream = stream as SubscribedStreamTable
                val streamTopics = topics.filter { topic -> topic.streamId == stream.id }
                StreamEntity(stream.id, stream.title, stream.isOpened, true, streamTopics)
            }
        }.filter { it.amISubscribedToStream == amISubscribed }
    }

    suspend fun saveStreams(streams: List<StreamEntity>, amISubscribed: Boolean) {
        val allTopics: List<TopicTable> = streams.flatMap { it.topics }
        topicDao.saveTopics(allTopics)
        if (amISubscribed) {
            val subscribedStream = streams.map {
                SubscribedStreamTable(it.id, it.title, it.isOpened)
            }
            streamDao.saveSubscribedStreams(subscribedStream)
        } else {
            val allStream = streams.map {
                AllStreamTable(it.id, it.title, it.isOpened)
            }
            streamDao.saveAllStreams(allStream)
        }
    }

    suspend fun saveUsers(users: List<UserTable>): List<Long> {
        return userDao.saveUsers(users)
    }

    suspend fun getUsers(): List<UserTable> {
        return userDao.getUsers()
    }

    suspend fun saveUser(user: UserTable): Long {
        return userDao.saveUser(user)
    }

    suspend fun getUserById(id: Int): UserTable? {
        return userDao.getUserById(id)
    }
}