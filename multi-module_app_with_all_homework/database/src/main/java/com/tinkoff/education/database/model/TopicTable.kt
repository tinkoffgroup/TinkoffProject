package com.tinkoff.education.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "topic_table")
data class TopicTable(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    val mes: Int,
    val streamName: String,
    val streamId: Int
)
