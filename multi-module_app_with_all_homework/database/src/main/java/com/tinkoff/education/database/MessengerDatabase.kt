package com.tinkoff.education.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tinkoff.education.database.dao.MessageDao
import com.tinkoff.education.database.dao.ReactionDao
import com.tinkoff.education.database.dao.StreamDao
import com.tinkoff.education.database.dao.TopicDao
import com.tinkoff.education.database.dao.UserDao
import com.tinkoff.education.database.model.AllStreamTable
import com.tinkoff.education.database.model.MessageTable
import com.tinkoff.education.database.model.ReactionTable
import com.tinkoff.education.database.model.SubscribedStreamTable
import com.tinkoff.education.database.model.TopicTable
import com.tinkoff.education.database.model.UserTable

@Database(entities = arrayOf(UserTable::class, AllStreamTable::class, SubscribedStreamTable::class, TopicTable::class, MessageTable::class, ReactionTable::class), version = 1, exportSchema = false)
abstract class MessengerDatabase : RoomDatabase() {

    abstract val userDao: UserDao

    abstract val streamDao: StreamDao

    abstract val topicDao: TopicDao

    abstract val messageDao: MessageDao

    abstract val reactionDao: ReactionDao
}