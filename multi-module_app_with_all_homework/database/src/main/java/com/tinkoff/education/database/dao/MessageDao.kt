package com.tinkoff.education.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.tinkoff.education.database.model.MessageTable
import com.tinkoff.education.database.model.MessageWithReactions

@Dao
interface MessageDao {

    @Transaction
    @Query("SELECT * FROM message_table WHERE hashCodeOfStreamsAndTopics = :hashCodeOfStreamsAndTopics")
    fun getMessages(hashCodeOfStreamsAndTopics: Int): PagingSource<Int, MessageWithReactions>

    @Query("DELETE FROM message_table WHERE hashCodeOfStreamsAndTopics = :hashCodeOfStreamsAndTopics")
    suspend fun deleteMessages(hashCodeOfStreamsAndTopics: Int)

    @Insert
    suspend fun saveMessages(messages: List<MessageTable>)
}