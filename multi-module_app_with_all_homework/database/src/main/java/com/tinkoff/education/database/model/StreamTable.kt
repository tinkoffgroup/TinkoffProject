package com.tinkoff.education.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "all_stream_table")
data class AllStreamTable(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    val isOpened: Boolean
)

@Entity(tableName = "subscribed_stream_table")
data class SubscribedStreamTable(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    val isOpened: Boolean
)

data class StreamEntity(
    val id: Int,
    val title: String,
    val isOpened: Boolean,
    val amISubscribedToStream: Boolean,
    val topics: List<TopicTable>
)