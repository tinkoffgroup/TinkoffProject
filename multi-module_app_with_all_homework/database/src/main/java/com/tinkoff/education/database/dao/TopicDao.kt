package com.tinkoff.education.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tinkoff.education.database.model.TopicTable

@Dao
interface TopicDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveTopics(users: List<TopicTable>): List<Long>

    @Query("SELECT * FROM topic_table")
    suspend fun getTopics(): List<TopicTable>

    @Query("SELECT * FROM topic_table WHERE streamName = :streamName")
    suspend fun getTopicsByStream(streamName: String): List<TopicTable>

    @Query("DELETE FROM topic_table WHERE streamId IN (:streamIds)")
    suspend fun deleteTopicsByStreamIds(streamIds: List<Int>)
}