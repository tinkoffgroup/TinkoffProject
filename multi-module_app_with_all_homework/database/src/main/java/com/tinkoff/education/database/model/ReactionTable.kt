package com.tinkoff.education.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reaction_table")
data class ReactionTable (
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val emojiName: String,
    val emojiCode: String,
    val userId: Int,
    val messageId: Int,
    val hashCodeOfStreamsAndTopics: Int
)