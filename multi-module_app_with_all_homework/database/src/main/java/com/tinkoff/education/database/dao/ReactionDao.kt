package com.tinkoff.education.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.tinkoff.education.database.model.ReactionTable

@Dao
interface ReactionDao {

    @Query("DELETE FROM reaction_table WHERE hashCodeOfStreamsAndTopics = :hashCodeOfStreamsAndTopics")
    suspend fun deleteReactions(hashCodeOfStreamsAndTopics: Int)

    @Insert
    suspend fun saveReactions(messages: List<ReactionTable>)
}