package com.tinkoff.education.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "message_table")
data class MessageTable(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val messageId: Int,
    val message: String,
    val avatarUrl: String,
    val timestamp: Long,
    val subject: String,
    val isMeMessage: Boolean,
    val senderId: Int,
    val myId: Int = 0,
    val senderFullName: String,
    val hashCodeOfStreamsAndTopics: Int
)