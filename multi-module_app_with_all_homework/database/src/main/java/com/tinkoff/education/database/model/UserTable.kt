package com.tinkoff.education.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class UserTable(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    val userId: Int,
    val userName: String,
    val userEmail: String,
    val userUrl: String,
    val userStatus: String
)
