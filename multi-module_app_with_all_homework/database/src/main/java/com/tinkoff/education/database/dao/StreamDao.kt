package com.tinkoff.education.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tinkoff.education.database.model.AllStreamTable
import com.tinkoff.education.database.model.SubscribedStreamTable

@Dao
interface StreamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAllStreams(users: List<AllStreamTable>): List<Long>

    @Query("SELECT * FROM all_stream_table")
    suspend fun getAllStreams(): List<AllStreamTable>

    @Query("DELETE FROM all_stream_table")
    suspend fun deleteAllStreams()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveSubscribedStreams(users: List<SubscribedStreamTable>): List<Long>

    @Query("SELECT * FROM subscribed_stream_table")
    suspend fun getSubscribedStreams(): List<SubscribedStreamTable>

    @Query("DELETE FROM subscribed_stream_table")
    suspend fun deleteSubscribedStreams()
}