package com.tinkoff.education.fintech.features.channels

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import com.google.android.material.tabs.TabLayoutMediator
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.databinding.FragmentChannelsBinding
import com.tinkoff.education.fintech.features.channels.stream.presentation.StreamFragment
import com.tinkoff.education.fintech.features.channels.pager.PagerAdapter
import com.tinkoff.education.fintech.features.channels.vm.SharedViewModel

class ChannelsFragment : Fragment() {

    private val sharedViewModel: SharedViewModel by activityViewModels()

    private var _binding: FragmentChannelsBinding? = null
    private val binding get() = _binding!!

    private lateinit var pagerAdapter: PagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChannelsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpPagerAdapter()
        observerToolBar()
    }

    private fun setUpPagerAdapter() {
        pagerAdapter = PagerAdapter(childFragmentManager, lifecycle)
        binding.streamPager.adapter = pagerAdapter
        pagerAdapter.update(
            listOf(
                StreamFragment.createInstance(amISubscribed = true),
                StreamFragment.createInstance(amISubscribed = false)
            )
        )

        val tabs = listOf("Subscribed", "All Streams")

        TabLayoutMediator(binding.tabs, binding.streamPager) {tab, position ->
            tab.text = tabs[position]
        }.attach()
    }

    override fun onResume() {
        super.onResume()
        sharedViewModel.getAllStreamsAndTopics()
    }

    private fun observerToolBar() {
        if (activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        }
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.tool_bar, menu)
            }
            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_search -> {
                        val searchView = menuItem.actionView as? SearchView
                        searchView?.queryHint = getText(R.string.search)

                        menuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
                            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                                return true
                            }

                            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                                sharedViewModel.getAllStreamsAndTopics()
                                return true
                            }

                        })

                        val handler = Handler(Looper.getMainLooper())
                        var workRunnable: Runnable? = null
                        val delay: Long = 500
                        var lastQuery: String? = null

                        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                            override fun onQueryTextSubmit(p0: String?): Boolean {
                                searchView.clearFocus()
                                return true
                            }
                            override fun onQueryTextChange(newText: String): Boolean {
                                if (newText.isNotBlank()) {
                                    workRunnable?.let { handler.removeCallbacks(it) }

                                    workRunnable = Runnable {
                                        if (newText != lastQuery) {
                                            searchTopic(newText)
                                            lastQuery = newText
                                        }
                                    }
                                    workRunnable?.let { handler.postDelayed(it, delay) }
                                }
                                return true
                            }
                        })
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun searchTopic(topicName: String) {
        sharedViewModel.searchTopic(topicName)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}