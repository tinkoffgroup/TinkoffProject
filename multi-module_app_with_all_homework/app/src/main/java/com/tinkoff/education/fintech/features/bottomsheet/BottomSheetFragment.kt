package com.tinkoff.education.fintech.features.bottomsheet

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.databinding.FragmentBottomSheetBinding
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.bottomsheet.model.emojiList
import com.tinkoff.education.fintech.features.bottomsheet.presenter.elm.BottomSheetEffect
import com.tinkoff.education.fintech.features.bottomsheet.presenter.elm.BottomSheetEvent
import com.tinkoff.education.fintech.features.bottomsheet.presenter.elm.BottomSheetState
import com.tinkoff.education.fintech.features.bottomsheet.presenter.elm.BottomSheetStoreFactory
import com.tinkoff.education.fintech.features.bottomsheet.recyclerview.EmojiAdapter
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.util.extensions.animateView
import com.tinkoff.education.fintech.features.util.extensions.changeTopicMessage
import com.tinkoff.education.fintech.features.util.extensions.shareMessage
import com.tinkoff.education.fintech.features.util.extensions.showDeleteRequestDialog
import com.tinkoff.education.fintech.features.util.extensions.showEditRequestDialog
import com.tinkoff.education.fintech.features.util.extensions.showErrorDialog
import vivid.money.elmslie.android.renderer.ElmRendererDelegate
import vivid.money.elmslie.android.renderer.elmStoreWithRenderer
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class BottomSheetFragment(
    private val messageModelUi: MessageModelUi,
    private val streamName: String,
    private val topicName: String,
    private val onEmojiClickListener: (
        emojiUi: EmojiUi
    ) -> Unit,
    private val reloadPage: () -> Unit
)  : BottomSheetDialogFragment(), ElmRendererDelegate<BottomSheetEffect, BottomSheetState> {

    private var _binding: FragmentBottomSheetBinding? = null
    private val binding get() = _binding!!

    private lateinit var emojiAdapter: EmojiAdapter
    private lateinit var recyclerView: RecyclerView

    private val dismiss: (
    ) -> Unit = {
        dismiss()
    }

    private val changeTopicMessage: ( newTopic: String
    ) -> Unit = { newTopic ->
        store.accept(BottomSheetEvent.Ui.ChangeTopicMessage(newTopic, messageModelUi.id))
    }

    @Inject
    lateinit var bottomSheetStoreFactory: BottomSheetStoreFactory
    override fun onCreate(savedInstanceState: Bundle?) {
        (requireActivity().applicationContext as App).appComponent.createBottomSheetComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    val store: Store<BottomSheetEvent, BottomSheetEffect, BottomSheetState> by elmStoreWithRenderer(
        elmRenderer = this
    ) {
        bottomSheetStoreFactory.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpVisibilityButton()
        setUpRecyclerView()
        fillRecyclerView()
        observeButton()
    }

    private fun setUpVisibilityButton() {
        if (messageModelUi.isMyMessage ) {
            binding.change.visibility = View.VISIBLE
            binding.delete.visibility = View.VISIBLE
            binding.edit.visibility = View.VISIBLE
        }
    }

    private fun setUpRecyclerView() {
        emojiAdapter = EmojiAdapter(onEmojiClickListener, dismiss)
        recyclerView = binding.recyclerView
        recyclerView.apply {
            adapter = emojiAdapter
            layoutManager = GridLayoutManager(requireContext(), 7)
        }
    }

    private fun fillRecyclerView() {
        emojiAdapter.submitList(emojiList)
    }

    private val editMessageLambda: ( newMessage: String
    ) -> Unit = { newMessage ->
        store.accept(BottomSheetEvent.Ui.EditMessage(newMessage, messageModelUi.id))
    }

    private val changeMessageTopicLambda: (
    ) -> Unit = {
        store.accept(BottomSheetEvent.Ui.ClickOnChangeTopic(streamName, topicName))
    }

    private val showDeleteRequestDialog: () -> Unit = {
        showDeleteRequestDialog(context, deleteMessageLambda)
    }

    private val deleteMessageLambda: (
    ) -> Unit = {
        store.accept(BottomSheetEvent.Ui.DeleteMessage(messageModelUi.id))
    }

    private val shareMessageLambda: () -> Unit = {
        shareMessage(context, messageModelUi)
    }

    private val showEditRequestDialog: () -> Unit = {
        showEditRequestDialog(context, messageModelUi, editMessageLambda)
    }

    private val copyMessageLambda: () -> Unit = {
        val clipboard = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("Copied Text", messageModelUi.userMessage)
        clipboard.setPrimaryClip(clip)
        dismiss()
    }

    private fun observeButton() {
        with(binding) {
            copy.setOnClickListener {
                animateView(it as AppCompatImageView, R.drawable.copy_icon_colored, copyMessageLambda)
            }
            delete.setOnClickListener {
                animateView(it as AppCompatImageView, R.drawable.delete_icon_colored, showDeleteRequestDialog)
            }
            edit.setOnClickListener {
                animateView(it as AppCompatImageView, R.drawable.edit_icon_colored, showEditRequestDialog)
            }
            share.setOnClickListener {
                animateView(it as AppCompatImageView, R.drawable.share_icon_colored, shareMessageLambda)
            }
            change.setOnClickListener {
                animateView(it as AppCompatImageView, R.drawable.change_icon_colored, changeMessageTopicLambda)
            }
        }
    }

    override fun render(state: BottomSheetState) {
        if (state.listOfTopic.isNotEmpty()) {
            val listOfTopicName = state.listOfTopic.map {
                it.title
            }
            changeTopicMessage(context, listOfTopicName, changeTopicMessage)
        }
        when (state.bottomSheetState) {
            is ResultState.Success -> {
                dismiss()
                reloadPage.invoke()
            }
        }
    }

    override fun handleEffect(effect: BottomSheetEffect){
        when (effect) {
            is BottomSheetEffect.ShowError -> {
                dismiss()
                showErrorDialog(context, reloadPage)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}