package com.tinkoff.education.fintech.features.people.domain.usecase

import com.tinkoff.education.fintech.features.people.domain.UserRepository
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import javax.inject.Inject

class GetListOfUsersBySearchQueryUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend fun execute(query: String): List<UserDomain> {
        val listOfUsers: List<UserDomain> = userRepository.getUsersFromDataBase()

        return listOfUsers.filter { it.userName.startsWith(query, ignoreCase = true) }
    }
}