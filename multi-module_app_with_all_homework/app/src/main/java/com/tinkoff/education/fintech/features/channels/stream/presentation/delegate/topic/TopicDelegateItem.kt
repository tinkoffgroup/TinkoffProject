package com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic

import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class TopicDelegateItem(
    val id: Int,
    private val topicModelUi: TopicModelUi
) : DelegateItem {
    override fun content(): Any = topicModelUi
    override fun id(): Int = topicModelUi.id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as TopicDelegateItem).topicModelUi == this.topicModelUi
    }
}