package com.tinkoff.education.fintech.features.channels.stream.domain.model

data class UnreadMessagesByTopicDomain(
    val topicName: String,
    val numberOfUnreadMessages: Int
)
