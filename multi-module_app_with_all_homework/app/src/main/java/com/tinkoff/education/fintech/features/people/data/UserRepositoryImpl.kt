package com.tinkoff.education.fintech.features.people.data

import com.tinkoff.education.database.DataBaseDataSource
import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.people.data.mapper.UsersApiToDomainMapper
import com.tinkoff.education.fintech.features.people.data.mapper.UsersPresenceApiToDomainMapper
import com.tinkoff.education.fintech.features.people.data.mapper.UsersTableToDomainMapper
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.UserRepository
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain
import com.tinkoff.education.networking.datasource.api.ChatNetworkDataSource
import com.tinkoff.education.networking.datasource.api.model.UsersApi
import com.tinkoff.education.networking.datasource.api.model.UsersPresenceApi
import javax.inject.Inject

class UserRepositoryImpl  @Inject constructor(
    private val chatNetworkDataSource: ChatNetworkDataSource,
    private val dataBaseDataSource: DataBaseDataSource
) : UserRepository {

    override suspend fun saveUsersToDataBase(users: List<UserTable>): List<Long> {
        return dataBaseDataSource.saveUsers(users)
    }

    override suspend fun getUsersFromDataBase(): List<UserDomain> {
        return UsersTableToDomainMapper.map(dataBaseDataSource.getUsers())
    }

    override suspend fun getListOfUsers(): List<UserDomain> {
        val listOfUsers: UsersApi = chatNetworkDataSource.getListOfUsers()
        return UsersApiToDomainMapper.map(listOfUsers.users)
    }

    override suspend fun getUsersPresence(): List<UserPresenceDomain> {
        val usersPresence: UsersPresenceApi = chatNetworkDataSource.getUsersPresence()
        return UsersPresenceApiToDomainMapper.map(usersPresence)
    }
}