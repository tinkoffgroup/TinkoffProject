package com.tinkoff.education.fintech.features.people.presentation.recyclerview

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.databinding.UserItemBinding
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi
import com.tinkoff.education.fintech.features.util.extensions.loadImage
import com.tinkoff.education.fintech.features.util.extensions.setStatusColor

class ViewHolderUser(val binding: UserItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(userUi: UserUi) {
        binding.userName.text = userUi.userName
        binding.userEmail.text = userUi.userEmail
        binding.userAvatar.loadImage(userUi.userUrl)
        binding.dot.setStatusColor(userUi.userStatus)

        setTransitionNames(binding.userAvatar, binding.userName, userUi)
    }

    private fun setTransitionNames(imageView: AppCompatImageView, textView: TextView, userUi: UserUi) {
        imageView.transitionName = userUi.userUrl
        textView.transitionName = userUi.userName
    }
}