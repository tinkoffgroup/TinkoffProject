package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

data class StreamState(
    val listOfStreams: ResultState<List<DelegateItem>>
)
