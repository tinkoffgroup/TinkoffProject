package com.tinkoff.education.fintech.features.bottomsheet.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.databinding.EmojiItemBinding
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi

class EmojiAdapter(
    private val onEmojiClickListener: (
        emojiUi: EmojiUi
    ) -> Unit,
    private val dismiss: (
    ) -> Unit
) : ListAdapter<EmojiUi, ViewHolderEmoji>(
    EmojiDiffCallback()
)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderEmoji {
        val itemViewHolder = EmojiItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        val viewHolder = ViewHolderEmoji(itemViewHolder)
        setEmojiListener(viewHolder)
        return viewHolder
    }

    private fun setEmojiListener(viewHolder: ViewHolderEmoji) {
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                onEmojiClickListener.invoke(getItem(position))
                dismiss.invoke()
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolderEmoji, position: Int) {
        holder.apply {
            val current: EmojiUi = getItem(position)
            bind(current)
        }
    }
}