package com.tinkoff.education.fintech.features.channels.stream.presentation.mapper

import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain

object StreamsDomainToUiMapper {
    fun map(type: List<StreamDomain>): List<StreamModelUi> {
        return type.map {
            StreamModelUi(
                id = it.id,
                title = it.name,
                isOpened = it.isOpened,
                topics = TopicsDomainToUiMapper.map(it.topics, it.name)
            )
        }
    }
}

object TopicsDomainToUiMapper {
    fun map(type: List<TopicDomain>, streamName: String): List<TopicModelUi> {
        return type.map {
            TopicModelUi(
                id = it.hashCode(),
                title = it.name,
                mes = it.numberOfUnreadMessages,
                streamName = streamName
            )
        }
    }
}