package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.usecase.GetListOfTopicsBySearchQueryUseCase
import com.tinkoff.education.fintech.features.channels.stream.domain.usecase.GetStreamsWithTopicsUseCase
import com.tinkoff.education.fintech.features.channels.stream.domain.usecase.GetTopicsByStreamUseCase
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.mapper.StreamsDomainToUiMapper
import com.tinkoff.education.fintech.features.util.extensions.StreamUiAndTopicUiToDelegateMapper
import com.tinkoff.education.fintech.features.util.extensions.TopicUiToDelegateMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import vivid.money.elmslie.core.store.Actor
import javax.inject.Inject

class StreamActor @Inject constructor(
    private val getStreamsWithTopicsUseCase: GetStreamsWithTopicsUseCase,
    private val getTopicsByStreamUseCase: GetTopicsByStreamUseCase,
    private val getListOfTopicsBySearchQueryUseCase: GetListOfTopicsBySearchQueryUseCase
) : Actor<StreamCommand, StreamEvent>() {

    override fun execute(command: StreamCommand): Flow<StreamEvent> {
        return when (command) {
            is StreamCommand.GetListOfStreams -> flow {
                val flow: Flow<Result<List<StreamDomain>>> = getStreamsWithTopicsUseCase.execute(command.amISubscribed)
                flow.map { result ->
                    result.fold(
                        onSuccess = { listOfStreams: List<StreamDomain> ->
                            val delegate = StreamUiAndTopicUiToDelegateMapper.map(StreamsDomainToUiMapper.map(listOfStreams))
                            emit(StreamEvent.Domain.DataLoaded(delegate))
                        },
                        onFailure = {
                            emit(StreamEvent.Domain.Error(it))
                        }
                    )
                }.collect()
            }

            is StreamCommand.AddTopicsByStream -> flow {
                runCatching {
                    getTopicsByStreamUseCase.execute(command.amISubscribed, command.stream)
                }.fold(
                    onSuccess = {
                        val delegate = StreamUiAndTopicUiToDelegateMapper.map(StreamsDomainToUiMapper.map(it))
                        emit(StreamEvent.Domain.DataLoaded(delegate))
                    },
                    onFailure = {
                        emit(StreamEvent.Domain.Error(it))
                    }
                )
            }

            is StreamCommand.GetListOfTopicsBySearchQuery -> flow {
                runCatching {
                    getListOfTopicsBySearchQueryUseCase.execute(command.amISubscribed, command.query)
                }.fold(
                    onSuccess = {
                        val listOfStreams: List<StreamModelUi> = StreamsDomainToUiMapper.map(it)

                        val listOfTopics = listOfStreams.flatMap { it.topics }
                        val delegate = TopicUiToDelegateMapper.map(listOfTopics)
                        emit(StreamEvent.Domain.DataLoaded(delegate))
                    },
                    onFailure = {
                        emit(StreamEvent.Domain.Error(it))
                    }
                )
            }
        }
    }
}