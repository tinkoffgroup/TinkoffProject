package com.tinkoff.education.fintech.features.message.presentation.mapper

import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import com.tinkoff.education.fintech.features.message.domain.model.MessageDomain
import com.tinkoff.education.fintech.features.message.domain.model.ReactionDomain
import com.tinkoff.education.fintech.features.message.presentation.model.ReactionUi

object MessageWithReactionsTableToDomainMapper {
    fun map(type: MessageWithReactions): MessageDomain {
        return MessageDomain(
            id = type.message.messageId,
            message = type.message.message,
            avatarUrl = type.message.avatarUrl,
            timestamp= type.message.timestamp,
            subject = type.message.subject,
            isMeMessage = type.message.isMeMessage,
            senderId = type.message.senderId,
            myId = type.message.myId,
            senderFullName = type.message.senderFullName,
            listOfReactions = type.reactions.map {
                ReactionDomain(it.emojiName, it.emojiCode, it.userId)
            }
        )
    }
}

object MessageDomainToUiMapper {
    fun map(type: MessageDomain): MessageModelUi {
        return MessageModelUi(
            id = type.id,
            userId = type.senderId,
            myId = type.myId,
            userName = type.senderFullName,
            userMessage = type.message,
            isMyMessage = type.isMeMessage,
            timestamp = type.timestamp,
            subject = type.subject,
            avatarUrl = type.avatarUrl,
            listOfReactions = ReactionsDomainToUiMapper.map(type.listOfReactions)
        )
    }
}

object ReactionsDomainToUiMapper {
    fun map(type: List<ReactionDomain>): List<ReactionUi> {
        return type.map {
            ReactionUi(
                emojiUi = EmojiUi(it.emojiName, it.emojiCode),
                userId = it.userId
            )
        }
    }
}