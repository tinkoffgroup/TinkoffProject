package com.tinkoff.education.fintech.features.profile.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.ElmStore
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class ProfileStoreFactory @Inject constructor(private val actor: ProfileActor) {

    fun create(): Store<ProfileEvent, ProfileEffect, ProfileState> {
        return ElmStore(
            initialState = ProfileState(user = ResultState.Loading),
            reducer = ProfileReducer(),
            actor = actor
        )
    }
}