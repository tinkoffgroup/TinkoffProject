package com.tinkoff.education.fintech.features.message.data.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.tinkoff.education.database.DataBaseDataSource
import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.fintech.features.message.data.mapper.MessagesApiGetReactionTableMapper
import com.tinkoff.education.fintech.features.message.data.mapper.MessagesApiToTableMapper
import com.tinkoff.education.fintech.features.util.extensions.createNarrow
import com.tinkoff.education.networking.datasource.api.ChatNetworkDataSource
import com.tinkoff.education.networking.datasource.api.model.MessageApi

@OptIn(ExperimentalPagingApi::class)
class MessageRemoteMediator(
    private val chatNetworkDataSource: ChatNetworkDataSource,
    private val dataBaseDataSource: DataBaseDataSource,
    private val stream: String,
    private val topic: String
) : RemoteMediator<Int, MessageWithReactions>() {

    private var anchorMessageId: Int? = null

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, MessageWithReactions>
    ): MediatorResult {
        return try {

            val anchor: String = when (loadType) {
                LoadType.REFRESH -> "newest"
                LoadType.APPEND -> anchorMessageId.toString()
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
            }

            val messages: List<MessageApi> = chatNetworkDataSource.getMessages(anchor, createNarrow(stream, topic)).messages

            anchorMessageId = messages.first().id

            val userMe = chatNetworkDataSource.getUserMe()
            val messagesTable = MessagesApiToTableMapper.map(messages, userMe, (stream + topic).hashCode())
            val newMessagesTable = messagesTable.reversed()

            val listOfReactions = MessagesApiGetReactionTableMapper.map(messages, (stream + topic).hashCode())

            if (loadType == LoadType.REFRESH) {
                dataBaseDataSource.deleteMessagesAndReactions((stream + topic).hashCode())
                dataBaseDataSource.saveMessagesAndReactions(newMessagesTable, listOfReactions)
            } else {
                dataBaseDataSource.saveMessagesAndReactions(newMessagesTable, listOfReactions)
            }

            MediatorResult.Success(endOfPaginationReached = messages.size < 20)
        } catch (exception: Exception) {
            MediatorResult.Error(exception)
        }
    }
}