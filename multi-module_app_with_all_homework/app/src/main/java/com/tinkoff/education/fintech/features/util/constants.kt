package com.tinkoff.education.fintech.features.util

const val URL = "https://tinkoff-android-spring-2024.zulipchat.com/api/v1/"
const val URL_LOCAL_HOST = "http://localhost:8080"