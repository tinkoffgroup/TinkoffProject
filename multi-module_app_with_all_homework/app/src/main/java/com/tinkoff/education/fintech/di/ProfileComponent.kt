package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.profile.presentation.ProfileFragment
import com.tinkoff.education.fintech.features.profile.presentation.elm.ProfileStoreFactory
import dagger.Subcomponent

@Subcomponent
interface ProfileComponent {

    fun inject(fragment: ProfileFragment)
    fun getProfileStoreFactory(): ProfileStoreFactory
}