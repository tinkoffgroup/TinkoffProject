package com.tinkoff.education.fintech.features.profile.presentation.elm

import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.profile.domain.usecase.GetUserMeUseCase
import com.tinkoff.education.fintech.features.profile.presentation.mapper.UserDomainToUiMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import vivid.money.elmslie.core.store.Actor
import javax.inject.Inject

class ProfileActor @Inject constructor(
    private val getUserMeUseCase: GetUserMeUseCase
) : Actor<ProfileCommand, ProfileEvent>() {
    override fun execute(command: ProfileCommand): Flow<ProfileEvent> {
        return when (command) {
            is ProfileCommand.GetInformationAboutMe -> flow {

                val flow: Flow<Result<UserDomain>> = getUserMeUseCase.execute()
                flow.map { result ->
                    result.fold(
                        onSuccess = { user: UserDomain ->
                            emit(ProfileEvent.Domain.DataLoaded(UserDomainToUiMapper.map(user)))
                        },
                        onFailure = {
                            emit(ProfileEvent.Domain.Error(it))
                        }
                    )
                }.collect()
            }
        }
    }

}