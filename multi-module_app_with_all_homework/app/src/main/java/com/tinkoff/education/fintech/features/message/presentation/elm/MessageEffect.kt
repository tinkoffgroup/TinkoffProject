package com.tinkoff.education.fintech.features.message.presentation.elm

sealed interface MessageEffect {

    data class ShowError(val throwable: Throwable) : MessageEffect

    data object HandleClick : MessageEffect
}