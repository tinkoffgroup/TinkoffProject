package com.tinkoff.education.fintech.features.bottomsheet.data

import com.tinkoff.education.database.DataBaseDataSource
import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import com.tinkoff.education.fintech.features.channels.stream.data.mapper.TopicsTableToDomainMapper
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain
import com.tinkoff.education.networking.datasource.api.ChatNetworkDataSource
import javax.inject.Inject

class BottomSheetRepositoryImpl @Inject constructor(
    private val chatNetworkDataSource: ChatNetworkDataSource,
    private val dataBaseDataSource: DataBaseDataSource
) : BottomSheetRepository {

    override suspend fun editMessage(editedMessage: String, messageId: Int): Any {
        return chatNetworkDataSource.editMessage(editedMessage, messageId)
    }

    override suspend fun changeTopicMessage(newTopic: String, messageId: Int): Any {
        return chatNetworkDataSource.changeTopicMessage(newTopic, messageId)
    }

    override suspend fun deleteMessage(messageId: Int): Any {
        return chatNetworkDataSource.deleteMessage(messageId)
    }

    override suspend fun getTopicsByStream(streamName: String): List<TopicDomain> {
        return TopicsTableToDomainMapper.map(dataBaseDataSource.getTopicsByStream(streamName))
    }
}