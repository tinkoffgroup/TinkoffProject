package com.tinkoff.education.fintech.features.bottomsheet.domain.usecase

import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import javax.inject.Inject

class ChangeTopicMessageUseCase @Inject constructor(
    private val bottomSheetRepository: BottomSheetRepository
) {

    suspend fun execute(newTopic: String, messageId: Int): Any {
        return bottomSheetRepository.changeTopicMessage(newTopic, messageId)
    }
}