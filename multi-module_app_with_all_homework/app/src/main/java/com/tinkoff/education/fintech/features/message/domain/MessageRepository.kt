package com.tinkoff.education.fintech.features.message.domain

import androidx.paging.PagingData
import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import kotlinx.coroutines.flow.Flow
import java.io.File

interface MessageRepository {

    fun getMessages(stream: String, topic: String): Flow<PagingData<MessageWithReactions>>

    suspend fun uploadFile(file: File): String

    suspend fun sendMessage(stream: String, topic: String, message: String): Any

    suspend fun getUserMe(): UserDomain

    suspend fun addMessageReaction(messageId: Int, reactionName: String): Any

    suspend fun removeMessageReaction(messageId: Int, reactionName: String): Any
}