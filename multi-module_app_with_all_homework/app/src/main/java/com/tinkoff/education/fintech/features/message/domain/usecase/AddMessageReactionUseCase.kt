package com.tinkoff.education.fintech.features.message.domain.usecase

import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import javax.inject.Inject

class AddMessageReactionUseCase @Inject constructor(
    private val messageRepository: MessageRepository
) {

    suspend fun execute(messageModelUi: MessageModelUi, emojiUi: EmojiUi): Any {
        val reactions = messageModelUi.listOfReactions.filter { it.emojiUi == emojiUi }


        return if (reactions.isEmpty()) {
            messageRepository.addMessageReaction(messageModelUi.id, emojiUi.emojiName)
        } else {
            var isMyEmoji = false
            reactions.forEach { reaction ->
                if (reaction.userId == messageModelUi.myId) {
                    isMyEmoji = true
                }
            }
            if (isMyEmoji) {
                // Содержит мой Emoji, просто скипнем добавление нового Emoji тогда
            } else {
                messageRepository.addMessageReaction(messageModelUi.id, emojiUi.emojiName)
            }
        }
    }
}