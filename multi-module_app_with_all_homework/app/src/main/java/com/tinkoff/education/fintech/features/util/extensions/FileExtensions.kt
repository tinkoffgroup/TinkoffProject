package com.tinkoff.education.fintech.features.util.extensions

import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import java.io.File

fun getFileFromUri(uri: Uri, context: Context): File {
    val file = File.createTempFile("file", ".${getFileExtension(context, uri)}", context.cacheDir)
    file.outputStream().use {
        context.contentResolver.openInputStream(uri)?.copyTo(it)
    }
    return file
}

private fun getFileExtension(context: Context, uri: Uri): String? {
    val fileType: String? = context.contentResolver.getType(uri)
    return MimeTypeMap.getSingleton().getExtensionFromMimeType(fileType)
}
