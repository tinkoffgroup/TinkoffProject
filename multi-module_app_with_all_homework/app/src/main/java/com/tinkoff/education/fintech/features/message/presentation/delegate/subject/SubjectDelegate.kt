package com.tinkoff.education.fintech.features.message.presentation.delegate.subject

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.databinding.SubjectItemBinding
import com.tinkoff.education.fintech.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class SubjectDelegate(
    private val onTopicListener: (
        subjectModelUi: SubjectModelUi
    ) -> Unit
) : AdapterDelegate {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            SubjectItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onTopicListener
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int,
        payloads: List<Any>
    ) {
        (holder as ViewHolder).bind(item.content() as SubjectModelUi)
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is SubjectDelegateItem

    class ViewHolder(
        private val binding: SubjectItemBinding,
        private val onTopicListener: (
            subjectModelUi: SubjectModelUi
        ) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: SubjectModelUi) {
            with(binding) {
                subject.text = model.subject
                itemView.setOnClickListener {
                    onTopicListener.invoke(model)
                }
            }
        }
    }
}