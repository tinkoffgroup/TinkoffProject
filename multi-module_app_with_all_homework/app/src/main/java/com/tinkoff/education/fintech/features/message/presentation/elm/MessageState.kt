package com.tinkoff.education.fintech.features.message.presentation.elm

import androidx.paging.PagingData
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

data class MessageState(
    val listOfMessages: ResultState<PagingData<DelegateItem>>
)
