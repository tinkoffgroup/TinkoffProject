package com.tinkoff.education.fintech.features.message.presentation

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.databinding.FragmentMessageBinding
import com.tinkoff.education.fintech.features.bottomsheet.BottomSheetFragment
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.date.DateDelegate
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageDelegate
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.subject.SubjectDelegate
import com.tinkoff.education.fintech.features.message.presentation.delegate.subject.SubjectModelUi
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageEffect
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageEvent
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageState
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageStoreFactory
import com.tinkoff.education.fintech.features.message.presentation.paging.MessageLoadStateAdapter
import com.tinkoff.education.fintech.features.message.presentation.paging.PagingAdapter
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem
import com.tinkoff.education.fintech.features.util.extensions.getFileFromUri
import com.tinkoff.education.fintech.features.util.extensions.showErrorDialog
import kotlinx.coroutines.launch
import vivid.money.elmslie.android.renderer.ElmRendererDelegate
import vivid.money.elmslie.android.renderer.elmStoreWithRenderer
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class MessageFragment : Fragment(), ElmRendererDelegate<MessageEffect, MessageState> {

    private var _binding: FragmentMessageBinding? = null
    private val binding get() = _binding!!

    private val args: MessageFragmentArgs by navArgs()

    private lateinit var pagingAdapter: PagingAdapter
    private lateinit var recyclerView: RecyclerView

    val store: Store<MessageEvent, MessageEffect, MessageState> by elmStoreWithRenderer(
        elmRenderer = this
    ) {
        messageStoreFactory.create()
    }

    private val launcher = registerForActivityResult(
        ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        val file = uri?.let {
            getFileFromUri(it, requireContext())
        }
        file?.let {
            MessageEvent.Ui.LongClickOnSendButton(args.streamName, args.topicName, it)
        }?.let { store.accept(it) }
    }

    private val init: () -> Unit = {
        store.accept(MessageEvent.Ui.Init(args.streamName, args.topicName))
    }

    private val onMessageListener: (
        messageModelUi: MessageModelUi
    ) -> Unit = { messageModelUi ->
        val onEmojiClickListener: (
            emojiUi: EmojiUi
        ) -> Unit = { emojiUi ->
            store.accept(
                MessageEvent.Ui.AddEmoji(
                    emojiUi,
                    messageModelUi,
                    args.streamName,
                    args.topicName
                )
            )
        }
        val bottom = BottomSheetFragment(
            messageModelUi,
            args.streamName,
            args.topicName,
            onEmojiClickListener,
            init
        )
        bottom.show(parentFragmentManager, bottom.tag)
    }

    private val onEmojiListener: (
        emojiUi: EmojiUi, message: MessageModelUi
    ) -> Unit = { emoji, message ->
        store.accept(MessageEvent.Ui.ClickOnEmoji(emoji, message, args.streamName, args.topicName))
    }

    private val onTopicListener: (
        subjectModelUi: SubjectModelUi
    ) -> Unit = { subjectModelUi ->
        val action = MessageFragmentDirections.actionMessageFragmentSelf(subjectModelUi.subject, args.streamName, false)
        findNavController().navigate(action)
    }

    @Inject
    lateinit var messageStoreFactory: MessageStoreFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        (requireActivity().applicationContext as App).appComponent.createMessageComponent()
            .inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMessageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            store.accept(MessageEvent.Ui.Init(args.streamName, args.topicName))
        }
        fillUserLayout()
        setUpViewElementVisibility()
        observerButton()
        setUpRecyclerView()
        setLoadState()
    }

    private fun setLoadState() {
        binding.recyclerView.adapter = pagingAdapter
            .withLoadStateFooter(
                footer = MessageLoadStateAdapter()
            )
        pagingAdapter.addLoadStateListener { loadState ->

            if (loadState.refresh is LoadState.Loading) {
                binding.recyclerView.visibility = View.INVISIBLE
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.recyclerView.visibility = View.VISIBLE
                binding.progressBar.visibility = View.INVISIBLE
            }
        }
    }

    private fun setUpRecyclerView() {
        pagingAdapter = PagingAdapter()
        pagingAdapter.addDelegate(MessageDelegate(onMessageListener, onEmojiListener))
        pagingAdapter.addDelegate(DateDelegate())
        pagingAdapter.addDelegate(SubjectDelegate(onTopicListener))
        recyclerView = binding.recyclerView
        val linerLayoutManager = LinearLayoutManager(requireContext())
        linerLayoutManager.reverseLayout = true
        recyclerView.apply {
            adapter = pagingAdapter

            layoutManager = linerLayoutManager
        }
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
    }

    private fun setUpViewElementVisibility() {
        if (args.isAllMessagesStream) {
            binding.topic.visibility = View.GONE
            binding.inputField.visibility = View.GONE
            binding.sendButtton.visibility = View.GONE
        }
    }

    private fun fillUserLayout() {
        binding.stream.text = args.streamName
        binding.topic.text = resources.getString(R.string.topic, args.topicName)
    }

    private fun observerButton() {
        binding.backArrow.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.inputField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (binding.inputField.text.toString().isEmpty()) {
                    binding.sendButtton.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.cross_circle))
                } else {
                    binding.sendButtton.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.paper_plane))
                }
            }
            override fun afterTextChanged(superfBatie: Editable?) {}
        })
        binding.sendButtton.setOnClickListener {
            if(binding.inputField.text.toString().trim().isNotEmpty()) {
                store.accept(MessageEvent.Ui.ClickOnSendButton(args.streamName, args.topicName, binding.inputField.text.toString()))
            }
            hideKeyboard()
            binding.inputField.setText("")
        }
        binding.sendButtton.setOnLongClickListener {
            openFileIntent()
            true
        }
    }

    private fun openFileIntent() {
        launcher.launch("image/*")
    }

    private fun hideKeyboard() {
        val inputManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
            binding.inputField.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    override fun handleEffect(effect: MessageEffect) {
        when (effect) {
            is MessageEffect.ShowError -> {
                binding.recyclerView.visibility = View.VISIBLE
                binding.progressBar.visibility = View.INVISIBLE
                showErrorDialog(context, init)
            }
            is MessageEffect.HandleClick -> {}
        }
    }

    override fun render(state: MessageState) {
        when (val dataToRender = state.listOfMessages) {
            is ResultState.Success -> {
                val data: PagingData<DelegateItem> = dataToRender.data
                lifecycleScope.launch {
                    pagingAdapter.submitData(data)
                }
            }
            is ResultState.Loading -> {
                binding.progressBar.visibility = View.VISIBLE
                binding.recyclerView.visibility = View.INVISIBLE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}