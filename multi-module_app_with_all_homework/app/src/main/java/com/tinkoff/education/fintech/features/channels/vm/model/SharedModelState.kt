package com.tinkoff.education.fintech.features.channels.vm.model

data class SharedModelState(
    val searchQuery: String,
    val getAllStreamsAndTopics: Boolean
)
