package com.tinkoff.education.fintech.features.message.presentation.delegate.message

import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class MessageDelegateItem(
    val id: Int,
    private val value: MessageModelUi
) : DelegateItem {
    override fun content(): Any = value
    override fun id(): Int = id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as MessageDelegateItem).value == content()
    }

    override fun payload(other: Any): DelegateItem.Payloadable {
        if (other is MessageDelegateItem) {
            if (value.listOfReactions != other.value.listOfReactions) {
                return ChangePayload.ListOfReactionChanged
            }
        }
        return DelegateItem.Payloadable.None
    }

    sealed class ChangePayload: DelegateItem.Payloadable {
        data object ListOfReactionChanged: ChangePayload()
    }
}