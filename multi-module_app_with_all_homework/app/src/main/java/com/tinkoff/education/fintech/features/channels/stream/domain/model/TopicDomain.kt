package com.tinkoff.education.fintech.features.channels.stream.domain.model

data class TopicDomain(
    val id: Int,
    val name: String,
    val maxId: Int,
    val numberOfUnreadMessages: Int = 0
)
