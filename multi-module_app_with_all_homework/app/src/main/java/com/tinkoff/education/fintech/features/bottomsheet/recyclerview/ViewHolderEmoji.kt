package com.tinkoff.education.fintech.features.bottomsheet.recyclerview

import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.databinding.EmojiItemBinding
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi

class ViewHolderEmoji(private val binding: EmojiItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(emoji: EmojiUi) {
        binding.emoji.text = emoji.getCodeString()
    }
}