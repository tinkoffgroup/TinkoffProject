package com.tinkoff.education.fintech

import android.app.Application
import com.tinkoff.education.fintech.di.AppComponent
import com.tinkoff.education.fintech.di.DaggerAppComponent
import com.tinkoff.education.fintech.features.util.URL
import com.tinkoff.education.networking.datasource.api.di.NetworkModule

class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.factory().create(context = applicationContext, NetworkModule(URL))
    }
}