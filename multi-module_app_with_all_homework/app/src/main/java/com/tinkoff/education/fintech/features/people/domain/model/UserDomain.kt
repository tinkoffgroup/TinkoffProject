package com.tinkoff.education.fintech.features.people.domain.model

data class UserDomain(
    val id: Int,
    val userName: String,
    val userEmail: String,
    val userUrl: String?,
    val userStatus: String?
)
