package com.tinkoff.education.fintech.di

import android.content.Context
import com.tinkoff.education.database.di.DataBaseModule
import com.tinkoff.education.networking.datasource.api.di.NetworkModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, RepositoryModule::class, DataBaseModule::class])
interface AppComponent {

    @Component.Factory
    interface AppComponentFactory {
        fun create(@BindsInstance context: Context, networkModule: NetworkModule): AppComponent
    }

    fun createStreamComponent(): StreamComponent

    fun createMessageComponent(): MessageComponent

    fun createProfileComponent(): ProfileComponent

    fun createPeopleComponent(): PeopleComponent

    fun createBottomSheetComponent(): BottomSheetComponent
}