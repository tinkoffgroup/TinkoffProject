package com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream

import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class StreamDelegateItem(
    val id: Int,
    private val streamModelUi: StreamModelUi
) : DelegateItem {
    override fun content(): Any = streamModelUi
    override fun id(): Int = streamModelUi.id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as StreamDelegateItem).streamModelUi == this.streamModelUi
    }
}