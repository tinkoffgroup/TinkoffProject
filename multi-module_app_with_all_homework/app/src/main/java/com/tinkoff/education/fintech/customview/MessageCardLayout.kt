package com.tinkoff.education.fintech.customview

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.withStyledAttributes
import androidx.core.view.size
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.shape.CornerFamily
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.util.extensions.loadImage

class MessageCardLayout @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0,
    defTheme: Int = 0,
) : ViewGroup(context, attributeSet, defStyle, defTheme) {

    private val avatar: ShapeableImageView
    private val name: TextView
    private val message: TextView
    private val emojis: FlexBoxLayout
    private var isMyMessage = false

    private var textConstantForOccupiedSpace: Float = 2f/3f

    fun setAvatar(imageView: String) {
        avatar.loadImage(imageView)
    }
    fun setName(newName: String) {
        name.text = newName
    }
    fun setMessage(newMessage: String) {
        message.text = newMessage
    }

    fun addEmoji(emojiView: EmojiView) {
        emojis.addView(emojiView, emojis.size)
    }

    fun removeAllViewsFromEmojis() {
        emojis.removeAllViews()
    }

    fun getPlus(): ImageView {
        return emojis.plus
    }

    fun isMyMessage(isMy: Boolean) {
        isMyMessage = isMy
        if (isMy) {
            avatar.visibility = View.GONE
            name.visibility = View.GONE
        }
    }

    fun addPlus() {
        emojis.addPlus()
    }

    init {
        context.withStyledAttributes(attributeSet, R.styleable.MessageCardLayout) {
            textConstantForOccupiedSpace =
                getString(R.styleable.MessageCardLayout_textConstantForOccupiedSpace)?.getFloat()
                    ?: (2f/3f)
            isMyMessage = getBoolean(R.styleable.MessageCardLayout_isMyMessage, false)
        }
        avatar = ShapeableImageView(context).apply {
            setImageResource(R.mipmap.ic_launcher_round)
            layoutParams = LayoutParams(40.dpToPx(context).toInt(), 40.dpToPx(context).toInt())
            strokeColor = ContextCompat.getColorStateList(context, R.color.teal)
            strokeWidth = 2.dpToPx(context)
            shapeAppearanceModel = shapeAppearanceModel.toBuilder().setAllCorners(CornerFamily.ROUNDED, 20.dpToPx(context)).build()
            addView(this)
        }

        name = TextView(context).apply {
            text = "My Name:!"
            this.setTextColor(AppCompatResources.getColorStateList(context, R.color.green))
            this.setTypeface(this.typeface, Typeface.BOLD)
            layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            addView(this)
            setPadding(8, 0 , 0,0)
        }

        message = TextView(context).apply {
            text = "1My Message:!"
            this.setTextColor(AppCompatResources.getColorStateList(context, R.color.white))
            this.setTypeface(this.typeface, Typeface.BOLD)
            layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            addView(this)
            setPadding(8, 0 , 0,0)
        }

        emojis = FlexBoxLayout(context).apply {
            layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            this@MessageCardLayout.addView(this)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        measureChildren(widthMeasureSpec, heightMeasureSpec)

        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        if (!isMyMessage) {
            message.measure(
                MeasureSpec.makeMeasureSpec(
                    ((widthSize - avatar.measuredWidth) * textConstantForOccupiedSpace).toInt(),
                    MeasureSpec.AT_MOST
                ),
                MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.UNSPECIFIED)
            )

            val height = maxOf(
                avatar.measuredHeight,
                name.measuredHeight + message.measuredHeight + emojis.measuredHeight
            )
            val width = maxOf(
                avatar.measuredWidth + name.measuredWidth,
                avatar.measuredWidth + message.measuredWidth,
                avatar.measuredWidth + emojis.measuredWidth
            )

            setMeasuredDimension(
                resolveSize(paddingLeft + paddingRight + width, widthMeasureSpec),
                resolveSize(paddingTop + paddingBottom + height, heightMeasureSpec)
            )
        } else {
            message.measure(
                MeasureSpec.makeMeasureSpec(
                    ((widthSize) * textConstantForOccupiedSpace).toInt(),
                    MeasureSpec.AT_MOST
                ),
                MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.UNSPECIFIED)
            )

            val height = maxOf(
                message.measuredHeight + emojis.measuredHeight
            )
            val width = maxOf(
                message.measuredWidth,
                emojis.measuredWidth
            )
            setMeasuredDimension(
                resolveSize(paddingLeft + paddingRight + width, widthMeasureSpec),
                resolveSize(paddingTop + paddingBottom + height, heightMeasureSpec)
            )
        }
    }

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams {
        return MarginLayoutParams(context, attrs)
    }

    override fun generateDefaultLayoutParams(): LayoutParams {
        return MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
    }

    override fun checkLayoutParams(layoutParams: LayoutParams?): Boolean {
        return layoutParams is MarginLayoutParams
    }

    override fun onLayout(p0: Boolean, l: Int, t: Int, r: Int, b: Int) {
        var left = paddingLeft  // int left, int top, int right, int bottom
        var top = paddingTop
        var right = paddingLeft
        var bottom = paddingTop

        if (!isMyMessage) {
            avatar.layout(left, top, right + avatar.measuredWidth, bottom + avatar.measuredHeight)

            left += avatar.measuredWidth
            right += avatar.measuredWidth
            name.layout(left, top, right + name.measuredWidth, bottom + name.measuredHeight)

            top += name.measuredHeight
            bottom += name.measuredHeight
            message.layout(
                left,
                top,
                right + message.measuredWidth,
                bottom + message.measuredHeight
            )

            top += message.measuredHeight
            bottom += message.measuredHeight
            emojis.layout(left, top, right + emojis.measuredWidth, bottom + emojis.measuredHeight)
        } else {
            message.layout(
                r - message.measuredWidth,
                0,
                r,
                message.measuredHeight)
            emojis.layout(
                r - emojis.measuredWidth,
                message.measuredHeight,
                r,
                message.measuredHeight + emojis.measuredHeight)
        }
    }

    private fun Int.dpToPx(context: Context): Float = (this * context.resources.displayMetrics.density)
    private fun String.getFloat(): Float {
        val parts = this.split(":")
        val numerator = parts[0].toFloat()
        val denominator = parts[1].toFloat()
        return numerator / denominator
    }
}