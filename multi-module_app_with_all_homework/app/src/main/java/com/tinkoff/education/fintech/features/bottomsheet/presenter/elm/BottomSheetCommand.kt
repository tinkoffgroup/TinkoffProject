package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

sealed class BottomSheetCommand {

    data class EditMessage(val editedMessage: String, val messageId: Int) : BottomSheetCommand()

    data class ChangeTopicMessage(val newTopic: String, val messageId: Int) : BottomSheetCommand()

    data class DeleteMessage(val messageId: Int) : BottomSheetCommand()

    data class GetListOfTopics(val streamName: String, val topicName: String) : BottomSheetCommand()
}