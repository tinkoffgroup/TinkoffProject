package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.message.presentation.MessageFragment
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageStoreFactory
import dagger.Subcomponent

@Subcomponent
interface MessageComponent {

    fun inject(fragment: MessageFragment)
    fun getMessageStoreFactory(): MessageStoreFactory
}