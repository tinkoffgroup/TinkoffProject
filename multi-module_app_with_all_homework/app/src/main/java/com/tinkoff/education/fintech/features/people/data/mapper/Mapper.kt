package com.tinkoff.education.fintech.features.people.data.mapper

import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain
import com.tinkoff.education.networking.datasource.api.model.UserApi
import com.tinkoff.education.networking.datasource.api.model.UserPresenceApi
import com.tinkoff.education.networking.datasource.api.model.UsersPresenceApi

object UsersApiToDomainMapper {
    fun map(type: List<UserApi>): List<UserDomain> {
        return type.map {
            UserDomain(
                id = it.id,
                userName = it.userName,
                userEmail = it.userEmail,
                userUrl = it.userUrl,
                userStatus = null
            )
        }
    }
}

object UserPresenceApiToDomainMapper {
    fun map(type: UserPresenceApi): UserPresenceDomain {
        return UserPresenceDomain(
            userStatus = type.presence.aggregated.status,
        )
    }
}

object UsersPresenceApiToDomainMapper {
    fun map(type: UsersPresenceApi): List<UserPresenceDomain> {
        return type.presences.map {
            UserPresenceDomain(userEmail = it.key, userStatus = it.value.aggregated.status)
        }
    }
}

object UsersDomainToTableMapper {
    fun map(type: List<UserDomain>): List<UserTable> {
        return type.map {
            UserTable(
                id = it.id,
                userId = it.id,
                userName = it.userName,
                userEmail = it.userEmail,
                userUrl = it.userUrl ?: "unknown",
                userStatus = it.userStatus ?: "unknown",
            )
        }
    }
}

object UsersTableToDomainMapper {
    fun map(type: List<UserTable>): List<UserDomain> {
        return type.map {
            UserDomain(
                id = it.id,
                userName = it.userName,
                userEmail = it.userEmail,
                userUrl = it.userUrl,
                userStatus = it.userStatus,
            )
        }
    }
}

object UserTableToDomainMapper {
    fun map(type: UserTable): UserDomain {
        return UserDomain(
            id = type.userId,
            userName = type.userName,
            userEmail = type.userEmail,
            userUrl = type.userUrl,
            userStatus = type.userStatus,
        )
    }
}

object UserDomainToTableMapper {
    fun map(type: UserDomain): UserTable {
        return UserTable(
            id = type.id,
            userId = type.id,
            userName = type.userName,
            userEmail = type.userEmail,
            userUrl = type.userUrl ?: "",
            userStatus = type.userStatus ?: "unknown",
        )
    }
}
