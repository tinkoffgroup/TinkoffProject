package com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.databinding.TopicItemBinding
import com.tinkoff.education.fintech.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class TopicDelegate(
    private val topicDetailsListener: (
        topicModelUi: TopicModelUi
    ) -> Unit
) : AdapterDelegate {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            TopicItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            topicDetailsListener
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int,
        payloads: List<Any>
    ) {
        (holder as ViewHolder).bind(item.content() as TopicModelUi)
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is TopicDelegateItem

    class ViewHolder(
        private val binding: TopicItemBinding,
        private val topicDetailsListener: (
            topicModelUi: TopicModelUi
        ) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: TopicModelUi) {
            with(binding) {
                title.text = model.title
                if (model.mes > 0) {
                    msgCount.text = itemView.resources.getString(R.string.mes, model.mes.toString())
                }
                itemView.setOnClickListener {
                    topicDetailsListener.invoke(model)
                }
            }
        }
    }
}