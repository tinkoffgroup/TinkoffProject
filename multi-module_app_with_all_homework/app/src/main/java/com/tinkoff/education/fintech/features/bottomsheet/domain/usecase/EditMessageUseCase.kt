package com.tinkoff.education.fintech.features.bottomsheet.domain.usecase

import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import javax.inject.Inject

class EditMessageUseCase @Inject constructor(
    private val bottomSheetRepository: BottomSheetRepository
) {

    suspend fun execute(editedMessage: String, messageId: Int): Any {
        return bottomSheetRepository.editMessage(editedMessage, messageId)
    }
}