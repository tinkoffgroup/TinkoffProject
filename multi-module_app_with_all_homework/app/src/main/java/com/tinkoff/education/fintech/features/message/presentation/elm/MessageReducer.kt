package com.tinkoff.education.fintech.features.message.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.dsl.ScreenDslReducer

class MessageReducer : ScreenDslReducer<MessageEvent,
        MessageEvent.Ui,
        MessageEvent.Domain,
        MessageState,
        MessageEffect,
        MessageCommand>(
    MessageEvent.Ui::class, MessageEvent.Domain::class
) {

    override fun Result.internal(event: MessageEvent.Domain) = when (event) {
        is MessageEvent.Domain.DataLoaded -> {
            state {
                copy(listOfMessages = ResultState.Success(event.pagingData))
            }
        }
        is MessageEvent.Domain.Error -> {
            effects {
                +MessageEffect.ShowError(event.throwable)
            }
        }
    }

    override fun Result.ui(event: MessageEvent.Ui) = when (event) {

        is MessageEvent.Ui.Init -> {
            state {
                copy(listOfMessages = ResultState.Loading)
            }
            commands {
                +MessageCommand.GetMessages(event.streamName, event.topicName)
            }
        }

        is MessageEvent.Ui.ClickOnSendButton -> {
            commands {
                +MessageCommand.SendMessage(event.stream, event.topic, event.message)
            }
            effects {
                +MessageEffect.HandleClick
            }
        }

        is MessageEvent.Ui.ReloadPage -> {
            state {
                copy(listOfMessages = ResultState.Loading)
            }
            commands {
                +MessageCommand.GetMessages(event.streamName, event.topicName)
            }
        }

        is MessageEvent.Ui.ClickOnEmoji -> {
            state {
                copy(listOfMessages = ResultState.Loading)
            }
            effects {
                +MessageEffect.HandleClick
            }
            commands {
                +MessageCommand.HandleEmojiClick(event.emojiUi, event.messageModelUi, event.streamName, event.topicName)
            }
        }

        is MessageEvent.Ui.AddEmoji -> {
            state {
                copy(listOfMessages = ResultState.Loading)
            }
            effects {
                +MessageEffect.HandleClick
            }
            commands {
                +MessageCommand.AddEmoji(event.emojiUi, event.messageModelUi, event.streamName, event.topicName)
            }
        }

        is MessageEvent.Ui.ClickOnTopic -> {
            commands {
                +MessageCommand.GetMessages(event.streamName, event.topicName)
            }
        }

        is MessageEvent.Ui.LongClickOnSendButton -> {
            commands {
                +MessageCommand.SendFile(event.stream, event.topic, event.file)
            }
        }
    }
}