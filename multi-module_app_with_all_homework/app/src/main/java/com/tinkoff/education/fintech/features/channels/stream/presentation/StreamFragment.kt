package com.tinkoff.education.fintech.features.channels.stream.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.databinding.FragmentStreamBinding
import com.tinkoff.education.fintech.features.channels.ChannelsFragmentDirections
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamDelegate
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicDelegate
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamEffect
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamEvent
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamState
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamStoreFactory
import com.tinkoff.education.fintech.features.channels.vm.SharedViewModel
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.util.delegate.MainAdapter
import com.tinkoff.education.fintech.features.util.extensions.showErrorDialog
import kotlinx.coroutines.launch
import vivid.money.elmslie.android.renderer.ElmRendererDelegate
import vivid.money.elmslie.android.renderer.elmStoreWithRenderer
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class StreamFragment : Fragment(),
    ElmRendererDelegate<StreamEffect, StreamState> {

    private var _binding: FragmentStreamBinding? = null
    private val binding get() = _binding!!

    private val sharedViewModel: SharedViewModel by activityViewModels()

    private lateinit var mainAdapter: MainAdapter
    private lateinit var recyclerView: RecyclerView

    val store: Store<StreamEvent, StreamEffect, StreamState> by elmStoreWithRenderer(
        elmRenderer = this
    ) {
        streamStoreFactory.create()
    }

    private val init: () -> Unit = {
        store.accept(StreamEvent.Ui.Init(amISubscribed))
    }

    private val streamDetailsListener: (
        streamModelUi: StreamModelUi
    ) -> Unit = { streamModel ->
        store.accept(StreamEvent.Ui.ClickOnStream(amISubscribed, streamModel))
    }

    private var amISubscribed: Boolean = true

    private var topicDetailsListener: (
        topicModel: TopicModelUi
    ) -> Unit = { topicModel ->
        store.accept(StreamEvent.Ui.ClickOnTopic(topicModel))
    }

    private var longClickOnStream: (
        streamModel: StreamModelUi
    ) -> Unit = { streamModel ->
        store.accept(StreamEvent.Ui.LongClickOnStream(streamModel))
    }

    @Inject
    lateinit var streamStoreFactory: StreamStoreFactory
    override fun onCreate(savedInstanceState: Bundle?) {
        (requireActivity().applicationContext as App).appComponent.createStreamComponent().inject(this)
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            amISubscribed = it.getBoolean(AM_I_SUBSCRIBED)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStreamBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        observerFlow()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let {
            amISubscribed = it.getBoolean(AM_I_SUBSCRIBED)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(AM_I_SUBSCRIBED, amISubscribed)
    }

    private fun observerFlow() {
        viewLifecycleOwner.lifecycleScope.launch {
            sharedViewModel.sharedState.collect { sharedState ->
                if (sharedState.getAllStreamsAndTopics) {
                    store.accept(StreamEvent.Ui.Init(amISubscribed))
                } else {
                    store.accept(StreamEvent.Ui.SearchTopic(amISubscribed, sharedState.searchQuery))
                }
            }
        }
    }

    private fun setUpRecyclerView() {
        mainAdapter = MainAdapter()
        mainAdapter.addDelegate(StreamDelegate(streamDetailsListener, longClickOnStream))
        mainAdapter.addDelegate(TopicDelegate(topicDetailsListener))
        recyclerView = binding.recyclerView
        recyclerView.apply {
            adapter = mainAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
    }

    override fun render(state: StreamState) {
        when (val dataToRender = state.listOfStreams) {
            is ResultState.Success -> {
                val data = dataToRender.data
                binding.shimmerView.stopShimmer()
                binding.recyclerView.visibility = View.VISIBLE
                binding.shimmerView.visibility = View.INVISIBLE
                mainAdapter.submitList(data)
            }
            is ResultState.Loading -> {
                binding.recyclerView.visibility = View.INVISIBLE
                binding.shimmerView.visibility = View.VISIBLE
                binding.shimmerView.startShimmer()
            }
            is ResultState.Error -> Unit
        }
    }

    override fun handleEffect(effect: StreamEffect){
        when (effect) {
            is StreamEffect.ShowError -> {
                showErrorDialog(context, init)
                binding.recyclerView.visibility = View.INVISIBLE
                binding.shimmerView.visibility = View.INVISIBLE
            }

            is StreamEffect.OpenMessage -> {
                val action = ChannelsFragmentDirections.actionChannelsFragmentToMessageFragment(effect.topicName, effect.streamName, effect.topicName.isEmpty())
                findNavController().navigate(action)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val AM_I_SUBSCRIBED: String = "am_i_subscribed"
        fun createInstance(
            amISubscribed: Boolean
        ) = StreamFragment().apply {
            this.amISubscribed = amISubscribed
        }
    }
}