package com.tinkoff.education.fintech.features.message.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.tinkoff.education.database.DataBaseDataSource
import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.fintech.features.message.data.mapper.UserApiToDomainMapper
import com.tinkoff.education.fintech.features.message.data.paging.MessageRemoteMediator
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.networking.datasource.api.ChatNetworkDataSource
import kotlinx.coroutines.flow.Flow
import java.io.File
import javax.inject.Inject

class MessageRepositoryImpl @Inject constructor(
    private val chatNetworkDataSource: ChatNetworkDataSource,
    private val dataBaseDataSource: DataBaseDataSource
) : MessageRepository {

    @OptIn(ExperimentalPagingApi::class)
    override fun getMessages(stream: String, topic: String): Flow<PagingData<MessageWithReactions>> {
        val pagingSourceFactory = { dataBaseDataSource.getMessages((stream + topic).hashCode()) }
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                prefetchDistance = 5
            ),
            remoteMediator = MessageRemoteMediator(chatNetworkDataSource, dataBaseDataSource, stream, topic),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    override suspend fun uploadFile(file: File): String {
        return chatNetworkDataSource.uploadFile(file)
    }

    override suspend fun sendMessage(stream: String, topic: String, message: String): Any {
        return chatNetworkDataSource.sendMessage(stream, topic, message)
    }

    override suspend fun getUserMe(): UserDomain {
        return UserApiToDomainMapper.map(chatNetworkDataSource.getUserMe())
    }

    override suspend fun addMessageReaction(messageId: Int, reactionName: String): Any {
        return chatNetworkDataSource.addMessageReaction(messageId, reactionName)
    }

    override suspend fun removeMessageReaction(messageId: Int, reactionName: String): Any {
        return chatNetworkDataSource.removeMessageReaction(messageId, reactionName)
    }
}