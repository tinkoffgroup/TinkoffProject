package com.tinkoff.education.fintech.features.channels.stream.domain

import com.tinkoff.education.database.model.StreamEntity
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.UnreadMessagesByTopicDomain

interface StreamAndTopicRepository {

    suspend fun deleteStreams(amISubscribed: Boolean)

    suspend fun getStreamsFromDataBase(amISubscribed: Boolean): List<StreamDomain>

    suspend fun saveStreamsToDataBase(streams: List<StreamEntity>, amISubscribed: Boolean)

    suspend fun getAllStreams(): List<StreamDomain>

    suspend fun getSubscribedStreams(): List<StreamDomain>

    suspend fun getTopicsByStreamId(id: Int): List<TopicDomain>

    suspend fun getUnreadMessagesByTopicStreams(eventTypes: String): List<UnreadMessagesByTopicDomain>
}