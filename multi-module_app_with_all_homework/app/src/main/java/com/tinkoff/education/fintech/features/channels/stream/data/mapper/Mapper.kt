package com.tinkoff.education.fintech.features.channels.stream.data.mapper

import com.tinkoff.education.database.model.StreamEntity
import com.tinkoff.education.database.model.TopicTable
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.UnreadMessagesByTopicDomain
import com.tinkoff.education.networking.datasource.api.model.Stream
import com.tinkoff.education.networking.datasource.api.model.StreamApi
import com.tinkoff.education.networking.datasource.api.model.TopicApi

object StreamsDomainToEntityMapper {
    fun map(type: List<StreamDomain>, amISubscribed: Boolean): List<StreamEntity> {
        return type.map {
            StreamEntity(
                id = it.id,
                title = it.name,
                isOpened = it.isOpened,
                amISubscribedToStream = amISubscribed,
                topics = TopicsDomainToTableMapper.map(it.topics, it)
            )
        }
    }
}

object TopicsDomainToTableMapper {
    fun map(type: List<TopicDomain>, stream: StreamDomain): List<TopicTable> {
        return type.map {
            TopicTable(
                id = it.id,
                title = it.name,
                mes = it.numberOfUnreadMessages,
                streamName = stream.name,
                streamId = stream.id
            )
        }
    }
}

object StreamsEntityToDomainMapper {
    fun map(type: List<StreamEntity>): List<StreamDomain> {
        return type.map {
            StreamDomain(
                id = it.id,
                name = it.title,
                isOpened = it.isOpened,
                topics = TopicsTableToDomainMapper.map(it.topics)
            )
        }
    }
}

object TopicsTableToDomainMapper {
    fun map(type: List<TopicTable>): List<TopicDomain> {
        return type.map {
            TopicDomain(
                id = it.id,
                name = it.title,
                maxId = 0,
                numberOfUnreadMessages = it.mes
            )
        }
    }
}

object StreamsApiToDomainMapper {
    fun map(type: List<StreamApi>): List<StreamDomain> {
        return type.map {
            StreamDomain(
                id = it.id,
                name = it.name
            )
        }
    }
}

object TopicsApiToDomainMapper {
    fun map(type: List<TopicApi>): List<TopicDomain> {
        return type.map {
            TopicDomain(
                id = 0,
                name = it.name,
                maxId = it.maxId
            )
        }
    }
}

object UnreadMessagesByTopicApiToDomainMapper {
    fun map(type: List<Stream>): List<UnreadMessagesByTopicDomain> {
        return type.map {
            UnreadMessagesByTopicDomain(
                topicName = it.topic,
                numberOfUnreadMessages = it.unreadMessageIds.size
            )
        }
    }
}