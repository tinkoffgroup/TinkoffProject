package com.tinkoff.education.fintech.features.message.presentation.elm

import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.flatMap
import com.tinkoff.education.fintech.features.message.domain.model.MessageDomain
import com.tinkoff.education.fintech.features.message.domain.usecase.AddMessageReactionUseCase
import com.tinkoff.education.fintech.features.message.domain.usecase.GetMessagesUseCase
import com.tinkoff.education.fintech.features.message.domain.usecase.HandleEmojiClickUseCase
import com.tinkoff.education.fintech.features.message.domain.usecase.SendFileUseCase
import com.tinkoff.education.fintech.features.message.domain.usecase.SendMessageUseCase
import com.tinkoff.education.fintech.features.message.presentation.delegate.date.DateDelegateItem
import com.tinkoff.education.fintech.features.message.presentation.delegate.date.DateModelUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageDelegateItem
import com.tinkoff.education.fintech.features.message.presentation.delegate.subject.SubjectDelegateItem
import com.tinkoff.education.fintech.features.message.presentation.delegate.subject.SubjectModelUi
import com.tinkoff.education.fintech.features.message.presentation.mapper.MessageDomainToUiMapper
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem
import com.tinkoff.education.fintech.features.util.extensions.toDayAndMonth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import vivid.money.elmslie.core.store.Actor
import javax.inject.Inject

class MessageActor @Inject constructor(
    private val getMessagesUseCase: GetMessagesUseCase,
    private val sendFileUseCase: SendFileUseCase,
    private val sendMessageUseCase: SendMessageUseCase,
    private val handleEmojiClickUseCase: HandleEmojiClickUseCase,
    private val addMessageReactionUseCase: AddMessageReactionUseCase
) : Actor<MessageCommand, MessageEvent>(), CoroutineScope by CoroutineScope(Dispatchers.Main) {

    override fun execute(command: MessageCommand): Flow<MessageEvent> {
        return when (command) {
            is MessageCommand.GetMessages -> flow {
                runCatching {
                    val flows: Flow<PagingData<MessageDomain>> =
                        getMessagesUseCase.execute(command.streamName, command.topicName)
                            .cachedIn(CoroutineScope(Dispatchers.IO + currentCoroutineContext()))
                    flows
                }.fold(
                    onSuccess = { flow ->
                        flow.map { pagingData: PagingData<MessageDomain> ->
                            var currentItemDate = ""
                            var currentItemSubject = ""

                            val newPaging: PagingData<DelegateItem> = pagingData.flatMap { message ->

                                val messageUi = MessageDomainToUiMapper.map(message)
                                val messageDelegate: DelegateItem = MessageDelegateItem(messageUi.id, messageUi)
                                val messageDate = message.timestamp.toDayAndMonth()
                                val messageSubject = message.subject

                                val delegateWithDate:List<DelegateItem> = if (currentItemDate.isBlank()) {
                                    currentItemDate = messageDate
                                    listOf()
                                } else {
                                    if (currentItemDate != messageDate) {
                                        val dateDelegate: DelegateItem = DateDelegateItem(message.timestamp.hashCode(), DateModelUi(currentItemDate))
                                        currentItemDate = messageDate
                                        listOf(dateDelegate)
                                    } else {
                                        listOf()
                                    }
                                }

                                val mutableDelegateList = delegateWithDate.toMutableList()

                                if (currentItemSubject.isBlank()) {
                                    currentItemSubject = messageSubject
                                    mutableDelegateList.add(messageDelegate)
                                } else {
                                    if (currentItemSubject != messageSubject) {
                                        val subjectDelegate: DelegateItem = SubjectDelegateItem(message.message.hashCode(), SubjectModelUi(currentItemSubject))
                                        currentItemSubject = messageSubject
                                        mutableDelegateList.add(subjectDelegate)
                                        mutableDelegateList.add(messageDelegate)
                                    } else {
                                        mutableDelegateList.add(messageDelegate)
                                    }
                                }
                                mutableDelegateList
                            }
                            emit(MessageEvent.Domain.DataLoaded(newPaging))
                        }.collect()
                    },
                    onFailure = {
                        emit(MessageEvent.Domain.Error(it))
                    }
                )
            }

            is MessageCommand.SendMessage -> flow {
                runCatching {
                    sendMessageUseCase.execute(command.stream, command.topic, command.message)
                }.fold(
                    onSuccess = {
                        emit(MessageEvent.Ui.ReloadPage(command.stream, command.topic))
                    },
                    onFailure = {
                        emit(MessageEvent.Domain.Error(it))
                    }
                )
            }

            is MessageCommand.HandleEmojiClick -> flow {
                runCatching {
                    handleEmojiClickUseCase.execute(command.emojiUi, command.messageModelUi)
                }.fold(
                    onSuccess = {
                        emit(MessageEvent.Ui.ReloadPage(command.streamName, command.topicName))
                    },
                    onFailure = {
                        emit(MessageEvent.Domain.Error(it))
                    }
                )
            }

            is MessageCommand.AddEmoji -> flow {
                runCatching {
                    addMessageReactionUseCase.execute(command.messageModelUi, command.emojiUi)
                }.fold(
                    onSuccess = {
                        emit(MessageEvent.Ui.ReloadPage(command.streamName, command.topicName))
                    },
                    onFailure = {
                        emit(MessageEvent.Domain.Error(it))
                    }
                )
            }

            is MessageCommand.SendFile -> flow {
                runCatching {
                    sendFileUseCase.execute(command.stream, command.topic, command.file)
                }.fold(
                    onSuccess = {
                        emit(MessageEvent.Ui.ReloadPage(command.stream, command.topic))
                    },
                    onFailure = {
                        emit(MessageEvent.Domain.Error(it))
                    }
                )
            }
        }
    }
}