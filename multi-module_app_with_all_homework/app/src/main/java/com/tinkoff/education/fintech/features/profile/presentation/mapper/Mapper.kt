package com.tinkoff.education.fintech.features.profile.presentation.mapper

import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.presentation.model.UserStatus
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

object UserDomainToUiMapper {
    fun map(type: UserDomain): UserUi {
        return UserUi(
                id = type.id,
                userName = type.userName,
                userEmail = type.userEmail,
                userUrl = type.userUrl ?: "https://i.ytimg.com/vi/YCaGYUIfdy4/maxresdefault.jpg",
                userStatus = UserStatus.getUserStatus(type.userStatus ?: "unknown")
            )
    }
}