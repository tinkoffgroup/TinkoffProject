package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.dsl.ScreenDslReducer

class StreamReducer : ScreenDslReducer<StreamEvent,
        StreamEvent.Ui,
        StreamEvent.Domain,
        StreamState,
        StreamEffect,
        StreamCommand>(
    StreamEvent.Ui::class, StreamEvent.Domain::class
) {

    override fun Result.internal(event: StreamEvent.Domain) = when (event) {
        is StreamEvent.Domain.DataLoaded -> {
            state {
                copy(listOfStreams = ResultState.Success(event.listOfStreams))
            }
        }
        is StreamEvent.Domain.Error -> {
            effects {
                +StreamEffect.ShowError(event.throwable)
            }
        }
    }

    override fun Result.ui(event: StreamEvent.Ui) = when (event) {
        is StreamEvent.Ui.Init -> {
            state {
                copy(listOfStreams = ResultState.Loading)
            }
            commands {
                +StreamCommand.GetListOfStreams(event.amISubscribed)
            }
        }

        is StreamEvent.Ui.ClickOnStream -> {
            state {
                copy(listOfStreams = ResultState.Loading)
            }
            commands {
                +StreamCommand.AddTopicsByStream(event.amISubscribed, event.stream)
            }
        }

        is StreamEvent.Ui.ClickOnTopic -> {
            effects {
                +StreamEffect.OpenMessage(event.topic.title, event.topic.streamName)
            }
        }

        is StreamEvent.Ui.SearchTopic -> {
            state {
                copy(listOfStreams = ResultState.Loading)
            }
            commands {
                +StreamCommand.GetListOfTopicsBySearchQuery(event.amISubscribed, event.searchQuery)
            }
        }

        is StreamEvent.Ui.LongClickOnStream -> {
            effects {
                +StreamEffect.OpenMessage("", event.stream.title)
            }
        }
    }
}