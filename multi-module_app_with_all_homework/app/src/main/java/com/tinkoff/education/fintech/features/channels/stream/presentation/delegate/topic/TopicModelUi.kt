package com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TopicModelUi(
    val id: Int,
    val title: String,
    val mes: Int,
    val streamName: String
): Parcelable
