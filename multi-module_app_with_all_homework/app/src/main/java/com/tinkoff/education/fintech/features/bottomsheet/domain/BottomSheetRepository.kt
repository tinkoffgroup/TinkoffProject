package com.tinkoff.education.fintech.features.bottomsheet.domain

import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain

interface BottomSheetRepository {

    suspend fun editMessage(editedMessage: String, messageId: Int): Any

    suspend fun changeTopicMessage(newTopic: String, messageId: Int): Any

    suspend fun deleteMessage(messageId: Int): Any

    suspend fun getTopicsByStream(streamName: String): List<TopicDomain>
}