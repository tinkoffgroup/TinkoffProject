package com.tinkoff.education.fintech.features.people.presentation.model

sealed interface PeopleModelState {
    data object Initial : PeopleModelState
    data object Loading : PeopleModelState
    data object Error : PeopleModelState
    data object DataLoaded : PeopleModelState
}