package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.dsl.ScreenDslReducer

class BottomSheetReducer : ScreenDslReducer<BottomSheetEvent,
        BottomSheetEvent.Ui,
        BottomSheetEvent.Domain,
        BottomSheetState,
        BottomSheetEffect,
        BottomSheetCommand>(
    BottomSheetEvent.Ui::class, BottomSheetEvent.Domain::class
) {

    override fun Result.internal(event: BottomSheetEvent.Domain) = when (event) {
        is BottomSheetEvent.Domain.Success -> {
            state {
                copy(
                    bottomSheetState = ResultState.Success(Any()),
                    listOfTopic = emptyList()
                )
            }
        }
        is BottomSheetEvent.Domain.Error -> {
            effects {
                +BottomSheetEffect.ShowError(event.throwable)
            }
        }

        is BottomSheetEvent.Domain.DataLoaded -> {
            state {
                copy(listOfTopic = event.listOfTopic)
            }
        }
    }

    override fun Result.ui(event: BottomSheetEvent.Ui) = when (event) {
        is BottomSheetEvent.Ui.EditMessage -> {
            commands {
                +BottomSheetCommand.EditMessage(event.editedMessage, event.messageId)
            }
        }
        is BottomSheetEvent.Ui.DeleteMessage -> {
            commands {
                +BottomSheetCommand.DeleteMessage(event.messageId)
            }
        }

        is BottomSheetEvent.Ui.ClickOnChangeTopic -> {
            commands {
                +BottomSheetCommand.GetListOfTopics(event.streamName, event.topicName)
            }
        }

        is BottomSheetEvent.Ui.ChangeTopicMessage -> {
            commands {
                +BottomSheetCommand.ChangeTopicMessage(event.newTopic, event.messageId)
            }
        }
    }
}