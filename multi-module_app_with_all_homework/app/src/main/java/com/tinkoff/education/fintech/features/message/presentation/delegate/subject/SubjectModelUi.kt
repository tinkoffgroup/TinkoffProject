package com.tinkoff.education.fintech.features.message.presentation.delegate.subject

data class SubjectModelUi(
    val subject: String
)
