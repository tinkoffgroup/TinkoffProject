package com.tinkoff.education.fintech.features.anotherprofile

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.transition.TransitionInflater
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.tinkoff.education.fintech.databinding.FragmentAnotherProfileBinding
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi
import com.tinkoff.education.fintech.features.util.extensions.loadImage

class AnotherProfileFragment : Fragment() {

    private var _binding: FragmentAnotherProfileBinding? = null
    private val binding get() = _binding!!

    private val args: AnotherProfileFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAnotherProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerButton()
        fillUserLayout(args.userUi)
        setTransition()
    }

    private fun fillUserLayout(userUi: UserUi) {
        Log.d("AnotherProfile", "$userUi")
        binding.profile.avatar.loadImage(userUi.userUrl)
        binding.profile.userName.text = userUi.userName
        binding.profile.status.text = userUi.userStatus.status
        binding.profile.status.setTextColor(AppCompatResources.getColorStateList(requireContext(), userUi.userStatus.color))
    }

    private fun observerButton() {
        binding.backArrow.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setTransition() {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
        binding.profile.avatar.apply {
            transitionName = args.userUi.userUrl
            startEnterTransitionAfterLoadingImage(args.userUi.userUrl, this)
        }
        binding.profile.userName.apply {
            transitionName = args.userUi.userName
        }
    }

    private fun startEnterTransitionAfterLoadingImage(imageAddress: String, imageView: ImageView) {
        Glide.with(this)
            .load(imageAddress)
            .dontAnimate()
            .listener(object : RequestListener<Drawable> {

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }
            })
            .into(imageView)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}