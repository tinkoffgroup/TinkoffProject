package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi

sealed interface BottomSheetEvent {

    sealed interface Ui : BottomSheetEvent {
        data class EditMessage(val editedMessage: String, val messageId: Int) : Ui
        data class ChangeTopicMessage(val newTopic: String, val messageId: Int) : Ui
        data class DeleteMessage(val messageId: Int) : Ui
        data class ClickOnChangeTopic(val streamName: String, val topicName: String) : Ui
    }

    sealed interface Domain : BottomSheetEvent {
        data object Success : Domain
        data class Error(val throwable: Throwable) : Domain
        data class DataLoaded(val listOfTopic: List<TopicModelUi>) : Domain
    }
}