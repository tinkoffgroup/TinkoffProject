package com.tinkoff.education.fintech.features.message.domain.usecase

import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import java.io.File
import javax.inject.Inject

class SendFileUseCase @Inject constructor(
    private val messageRepository: MessageRepository
) {

    suspend fun execute(stream: String, topic: String, file: File): Any {
        val uri: String = messageRepository.uploadFile(file)

        val uploadedFileLink = "[${file.name}]($uri)"
        return messageRepository.sendMessage(stream, topic, uploadedFileLink)
    }
}