package com.tinkoff.education.fintech.customview

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.withStyledAttributes
import com.tinkoff.education.fintech.R

class FlexBoxLayout @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0,
    defTheme: Int = 0,
) : ViewGroup(context, attributeSet, defStyle, defTheme) {

    var plus: ImageView

    private var horizontalSpaceBetweenEmoji: Int = 0
    private var countOnOneLine: Int = 5

    init {
        context.withStyledAttributes(attributeSet, R.styleable.FlexBoxLayout) {
            horizontalSpaceBetweenEmoji = getInt(R.styleable.FlexBoxLayout_horizontalSpaceBetweenEmoji, 0).dpToPx(context).toInt()
            countOnOneLine = getInt(R.styleable.FlexBoxLayout_countOnOneLine, 5)
        }

        plus = ImageView(context).apply {
            setImageResource(R.drawable.add_icon)
            layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            addView(this)
        }
    }

    fun addPlus() {
        plus.apply {
            setImageResource(R.drawable.add_icon)
            layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            addView(this)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val count = childCount
        val countItemToGoNextRow = countOnOneLine

        var maxHeight = 0
        var widthOnRow = 0
        var maxWidth = 0

        for (i in 0 until count) {
            val child = getChildAt(i)

            measureChild(child, widthMeasureSpec, heightMeasureSpec)
            val childWidth = child.measuredWidth
            val childHeight = child.measuredHeight

            if (i % countItemToGoNextRow == 0) {
                widthOnRow = childWidth
                if(widthOnRow > maxWidth) {
                    maxWidth = widthOnRow
                }
                maxHeight += childHeight
            } else {
                widthOnRow += childWidth + horizontalSpaceBetweenEmoji
                if(widthOnRow > maxWidth) {
                    maxWidth = widthOnRow
                }
            }
        }
        setMeasuredDimension(
            resolveSize(paddingLeft + paddingRight + maxWidth, widthMeasureSpec)
            , resolveSize(paddingTop + paddingBottom + maxHeight, heightMeasureSpec))
    }

    override fun onLayout(p0: Boolean, p1: Int, p2: Int, p3: Int, p4: Int) {
        var left = 0
        var top = 0
        val countItemToGoNextRow = countOnOneLine

        val count = childCount
        for (index in 0 until count) {
            val child = getChildAt(index)
            if (index % countItemToGoNextRow == 0) {
                if(index != 0) {
                    top += child.measuredHeight
                } else {
                    top = paddingTop
                }
                left = paddingLeft
            } else {
                left += horizontalSpaceBetweenEmoji
            }
            child.layout(left, top, left + child.measuredWidth, top + child.measuredHeight)
            left += child.measuredWidth
        }
    }

    private fun Int.dpToPx(context: Context): Float = (this * context.resources.displayMetrics.density)
}