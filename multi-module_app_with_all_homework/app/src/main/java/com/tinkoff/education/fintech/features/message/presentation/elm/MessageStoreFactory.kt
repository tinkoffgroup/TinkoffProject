package com.tinkoff.education.fintech.features.message.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.ElmStore
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class MessageStoreFactory @Inject constructor(private val actor: MessageActor) {

    fun create(): Store<MessageEvent, MessageEffect, MessageState> {
        return ElmStore(
            initialState = MessageState(listOfMessages = ResultState.Loading),
            reducer = MessageReducer(),
            actor = actor
        )
    }
}