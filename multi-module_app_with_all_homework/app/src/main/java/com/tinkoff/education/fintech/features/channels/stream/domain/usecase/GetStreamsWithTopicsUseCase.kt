package com.tinkoff.education.fintech.features.channels.stream.domain.usecase

import com.tinkoff.education.fintech.features.channels.stream.data.mapper.StreamsDomainToEntityMapper
import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain
import com.tinkoff.education.fintech.features.util.extensions.createEventTypes
import com.tinkoff.education.fintech.features.util.extensions.runCatchingNonCancellation
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetStreamsWithTopicsUseCase @Inject constructor(
    private val streamAndTopicRepository: StreamAndTopicRepository
) {

    suspend fun execute(amISubscribed: Boolean): Flow<Result<List<StreamDomain>>> = flow {
        val streamFromDataBase: List<StreamDomain> =
            streamAndTopicRepository.getStreamsFromDataBase(amISubscribed)
        if (streamFromDataBase.isNotEmpty()) {
            emit(Result.success(streamFromDataBase))
        }
        runCatchingNonCancellation {
            val streams: List<StreamDomain> = if (amISubscribed) {
                streamAndTopicRepository.getSubscribedStreams()
            } else {
                streamAndTopicRepository.getAllStreams()
            }

            val eventTypes = createEventTypes(listOf("message", "update_message_flags"))
            val unreadMessagesByTopic =
                streamAndTopicRepository.getUnreadMessagesByTopicStreams(eventTypes)
            val listOfStreams = streams.map { stream ->
                val topics: List<TopicDomain> =
                    streamAndTopicRepository.getTopicsByStreamId(stream.id)
                val newTopics = mutableListOf<TopicDomain>()
                topics.forEach { topic: TopicDomain ->
                    val topicWithUnreadMessages =
                        unreadMessagesByTopic.find { it.topicName == topic.name }
                    val numberUnreadMessage = topicWithUnreadMessages?.numberOfUnreadMessages ?: 0
                    val newTopic = topic.copy(numberOfUnreadMessages = numberUnreadMessage)
                    newTopics.add(newTopic)
                }
                stream.copy(topics = newTopics)
            }
            listOfStreams
        }.fold(
            onSuccess = { listOfStreams ->
                emit(Result.success(listOfStreams))
                streamAndTopicRepository.deleteStreams(amISubscribed)
                streamAndTopicRepository.saveStreamsToDataBase(
                    StreamsDomainToEntityMapper.map(
                        listOfStreams,
                        amISubscribed
                    ), amISubscribed
                )
            },
            onFailure = {
                emit(Result.failure(it))
            }
        )

    }
}