package com.tinkoff.education.fintech.features.people.presentation

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.databinding.FragmentPeopleBinding
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleEffect
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleEvent
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleState
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleStoreFactory
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi
import com.tinkoff.education.fintech.features.people.presentation.recyclerview.UserAdapter
import com.tinkoff.education.fintech.features.util.extensions.showErrorDialog
import vivid.money.elmslie.android.renderer.ElmRendererDelegate
import vivid.money.elmslie.android.renderer.elmStoreWithRenderer
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class PeopleFragment : Fragment(), ElmRendererDelegate<PeopleEffect, PeopleState> {

    private var _binding: FragmentPeopleBinding? = null
    private val binding get() = _binding!!

    val store: Store<PeopleEvent, PeopleEffect, PeopleState> by elmStoreWithRenderer(
        elmRenderer = this
    ) {
        peopleStoreFactory.create()
    }

    private val init: () -> Unit = {
        store.accept(PeopleEvent.Ui.Init)
    }

    private val userDetailListener: (
        userUi: UserUi, imageView: AppCompatImageView, textView: TextView
    ) -> Unit = { userUi, imageView, textView ->
        store.accept(PeopleEvent.Ui.ClickOnUser(userUi, imageView, textView))
    }
    private lateinit var userAdapter: UserAdapter
    private lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var peopleStoreFactory: PeopleStoreFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        (requireActivity().applicationContext as App).appComponent.createPeopleComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPeopleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            store.accept(PeopleEvent.Ui.Init)
        }
        setUpRecyclerView()
        observerToolBar()
    }

    private fun setUpRecyclerView() {
        userAdapter = UserAdapter(userDetailListener)
        recyclerView = binding.recyclerView
        recyclerView.apply {
            postponeEnterTransition()
            viewTreeObserver.addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
            adapter = userAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun observerToolBar() {
        if (activity is AppCompatActivity){
            (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        }
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.tool_bar, menu)
            }
            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_search -> {
                        val searchView = menuItem.actionView as? SearchView
                        searchView?.queryHint = getText(R.string.users)

                        menuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
                            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                                return true
                            }
                            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                                store.accept(PeopleEvent.Ui.Init)
                                return true
                            }

                        })

                        val handler = Handler(Looper.getMainLooper())
                        var workRunnable: Runnable? = null
                        val delay: Long = 500
                        var lastQuery: String? = null

                        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                            override fun onQueryTextSubmit(p0: String?): Boolean {
                                searchView.clearFocus()
                                return true
                            }
                            override fun onQueryTextChange(newText: String): Boolean {
                                if (newText.isNotBlank()) {
                                    workRunnable?.let { handler.removeCallbacks(it) }

                                    workRunnable = Runnable {
                                        if (newText != lastQuery) {
                                            searchUsers(newText)
                                            lastQuery = newText
                                        }
                                    }
                                    workRunnable?.let { handler.postDelayed(it, delay) }
                                }
                                return true
                            }
                        })
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun searchUsers(userName: String) {
        store.accept(PeopleEvent.Ui.SearchUser(userName))
    }

    override fun render(state: PeopleState) {
        when (val dataToRender = state.listOfPeople) {
            is ResultState.Success -> {
                val data = dataToRender.data
                binding.shimmerView.stopShimmer()
                binding.recyclerView.visibility = View.VISIBLE
                binding.shimmerView.visibility = View.INVISIBLE
                userAdapter.submitList(data)
            }
            is ResultState.Loading -> {
                binding.recyclerView.visibility = View.INVISIBLE
                binding.shimmerView.visibility = View.VISIBLE
                binding.shimmerView.startShimmer()
            }
            is ResultState.Error -> Unit
        }
    }

    override fun handleEffect(effect: PeopleEffect){
        when (effect) {
            is PeopleEffect.ShowError -> {
                binding.recyclerView.visibility = View.INVISIBLE
                binding.shimmerView.visibility = View.INVISIBLE
                showErrorDialog(context, init)
            }
            is PeopleEffect.OpenUserProfile -> {

                val extras = FragmentNavigatorExtras(
                    effect.imageView to effect.user.userUrl,
                    effect.textView to effect.user.userName
                )
                val action =
                    PeopleFragmentDirections.actionPeopleFragmentToAnotherProfileFragment(
                        effect.user
                    )
                findNavController().navigate(action, extras)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}