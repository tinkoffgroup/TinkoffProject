package com.tinkoff.education.fintech.features.people.domain.model

data class UserPresenceDomain(
    val userStatus: String,
    val userEmail: String? = null
)
