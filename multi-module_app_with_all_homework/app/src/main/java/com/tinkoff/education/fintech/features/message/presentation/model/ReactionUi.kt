package com.tinkoff.education.fintech.features.message.presentation.model

import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi

data class ReactionUi(
    val emojiUi: EmojiUi,
    val userId: Int
)
