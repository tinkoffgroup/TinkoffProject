package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.people.presentation.PeopleFragment
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleStoreFactory
import dagger.Subcomponent

@Subcomponent
interface PeopleComponent {

    fun inject(fragment: PeopleFragment)
    fun getPeopleStoreFactory(): PeopleStoreFactory
}