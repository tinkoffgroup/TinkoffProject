package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.bottomsheet.data.BottomSheetRepositoryImpl
import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import com.tinkoff.education.fintech.features.channels.stream.data.StreamAndTopicRepositoryImpl
import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.message.data.MessageRepositoryImpl
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.people.data.UserRepositoryImpl
import com.tinkoff.education.fintech.features.people.domain.UserRepository
import com.tinkoff.education.fintech.features.profile.data.ProfileRepositoryImpl
import com.tinkoff.education.fintech.features.profile.domain.ProfileRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindMessageRepository(
        messageRepository: MessageRepositoryImpl
    ): MessageRepository


    @Binds
    @Singleton
    fun bindStreamAndTopicRepository(
        streamAndTopicRepository: StreamAndTopicRepositoryImpl
    ): StreamAndTopicRepository

    @Binds
    @Singleton
    fun bindUserRepository(
        userRepository: UserRepositoryImpl
    ): UserRepository

    @Binds
    @Singleton
    fun bindProfileRepository(
        profileRepository: ProfileRepositoryImpl
    ): ProfileRepository

    @Binds
    @Singleton
    fun bindBottomSheetRepository(
        bottomSheetRepository: BottomSheetRepositoryImpl
    ): BottomSheetRepository
}