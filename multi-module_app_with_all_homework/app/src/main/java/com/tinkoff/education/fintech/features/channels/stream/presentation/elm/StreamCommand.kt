package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi


sealed class StreamCommand {

    data class GetListOfStreams(val amISubscribed: Boolean) : StreamCommand()

    data class AddTopicsByStream(val amISubscribed: Boolean, val stream: StreamModelUi) : StreamCommand()

    data class GetListOfTopicsBySearchQuery(val amISubscribed: Boolean, val query: String) : StreamCommand()
}