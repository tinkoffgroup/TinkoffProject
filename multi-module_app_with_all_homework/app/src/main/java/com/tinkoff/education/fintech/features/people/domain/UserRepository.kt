package com.tinkoff.education.fintech.features.people.domain

import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain

interface UserRepository {

    suspend fun saveUsersToDataBase(users: List<UserTable>): List<Long>

    suspend fun getUsersFromDataBase(): List<UserDomain>

    suspend fun getListOfUsers(): List<UserDomain>

    suspend fun getUsersPresence(): List<UserPresenceDomain>
}