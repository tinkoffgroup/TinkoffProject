package com.tinkoff.education.fintech.features.message.domain.usecase

import androidx.paging.PagingData
import androidx.paging.map
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.message.domain.model.MessageDomain
import com.tinkoff.education.fintech.features.message.presentation.mapper.MessageWithReactionsTableToDomainMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetMessagesUseCase @Inject constructor(
    private val messageRepository: MessageRepository
) {

    fun execute(stream: String, topic: String): Flow<PagingData<MessageDomain>> {
        return messageRepository.getMessages(stream, topic).map {
            it.map { message ->
                MessageWithReactionsTableToDomainMapper.map(message)
            }
        }
    }
}