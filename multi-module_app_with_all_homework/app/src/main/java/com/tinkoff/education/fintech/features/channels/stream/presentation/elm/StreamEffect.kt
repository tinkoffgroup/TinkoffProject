package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

sealed interface StreamEffect {

    data class ShowError(val throwable: Throwable) : StreamEffect

    data class OpenMessage(val topicName: String, val streamName: String) : StreamEffect
}