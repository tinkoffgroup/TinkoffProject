package com.tinkoff.education.fintech.features.message.presentation.delegate.date

import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class DateDelegateItem(
    val id: Int,
    private val value: DateModelUi,
) : DelegateItem {
    override fun content(): Any = value
    override fun id(): Int = id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as DateDelegateItem).value == content()
    }
}