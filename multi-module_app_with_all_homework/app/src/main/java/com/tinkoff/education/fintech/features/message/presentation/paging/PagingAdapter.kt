package com.tinkoff.education.fintech.features.message.presentation.paging

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fintech.features.util.delegate.DelegateAdapterItemCallback
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class PagingAdapter : PagingDataAdapter<DelegateItem, RecyclerView.ViewHolder>(DelegateAdapterItemCallback()) {

    private val delegates: MutableList<AdapterDelegate> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        delegates[viewType].onCreateViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegates[getItemViewType(position)].onBindViewHolder(holder, getItem(position)!!, position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {
        if (payloads.isNotEmpty()) {
            delegates[getItemViewType(position)].onBindViewHolder(
                holder,
                getItem(position)!!,
                position,
                payloads.toList()
            )
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    fun addDelegate(delegate: AdapterDelegate) {
        delegates.add(delegate)
    }

    override fun getItemViewType(position: Int): Int {
        return delegates.indexOfFirst { it.isOfViewType(snapshot()[position]!!) }
    }
}