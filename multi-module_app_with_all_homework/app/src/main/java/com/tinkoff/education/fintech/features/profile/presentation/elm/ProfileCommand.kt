package com.tinkoff.education.fintech.features.profile.presentation.elm

sealed class ProfileCommand {

    data object GetInformationAboutMe : ProfileCommand()
}