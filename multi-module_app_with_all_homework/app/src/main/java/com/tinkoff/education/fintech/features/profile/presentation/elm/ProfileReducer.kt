package com.tinkoff.education.fintech.features.profile.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.dsl.ScreenDslReducer

class ProfileReducer : ScreenDslReducer<ProfileEvent,
        ProfileEvent.Ui,
        ProfileEvent.Domain,
        ProfileState,
        ProfileEffect,
        ProfileCommand>(
    ProfileEvent.Ui::class, ProfileEvent.Domain::class
) {
    override fun Result.internal(event: ProfileEvent.Domain) = when (event) {
        is ProfileEvent.Domain.DataLoaded -> {
            state {
                copy(user = ResultState.Success(event.user))
            }
        }
        is ProfileEvent.Domain.Error -> {
            effects {
                +ProfileEffect.ShowError(event.throwable)
            }
        }
    }

    override fun Result.ui(event: ProfileEvent.Ui) = when (event) {
        ProfileEvent.Ui.Init -> {
            state {
                copy(user = ResultState.Loading)
            }
            commands {
                +ProfileCommand.GetInformationAboutMe
            }
        }
    }

}