package com.tinkoff.education.fintech.features.people.presentation.elm

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

sealed interface PeopleEvent {

    sealed interface Ui : PeopleEvent {
        data object Init : Ui
        data class SearchUser(val searchQuery: String) : Ui
        data class ClickOnUser(val user: UserUi, val imageView: AppCompatImageView, val textView: TextView) : Ui
    }

    sealed interface Domain : PeopleEvent {
        data class DataLoaded(val listOfPeople: List<UserUi>) : Domain
        data class Error(val throwable: Throwable) : Domain
    }
}