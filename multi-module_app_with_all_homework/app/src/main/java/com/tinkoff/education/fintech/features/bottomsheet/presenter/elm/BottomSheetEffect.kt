package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

sealed interface BottomSheetEffect {

    data class ShowError(val throwable: Throwable) : BottomSheetEffect
}