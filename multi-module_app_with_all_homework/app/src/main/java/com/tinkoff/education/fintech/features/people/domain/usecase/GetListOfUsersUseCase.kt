package com.tinkoff.education.fintech.features.people.domain.usecase

import com.tinkoff.education.fintech.features.people.data.mapper.UsersDomainToTableMapper
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.UserRepository
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain
import com.tinkoff.education.fintech.features.util.extensions.MY_ID_IN_DATABASE
import com.tinkoff.education.fintech.features.util.extensions.runCatchingNonCancellation
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetListOfUsersUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend fun execute(): Flow<Result<List<UserDomain>>> = flow {
        val localData: List<UserDomain>  = userRepository.getUsersFromDataBase()
        if (localData.isNotEmpty()) {
            val listOfUsersDataBase = localData.toMutableList()
            listOfUsersDataBase.removeAll { it.id == MY_ID_IN_DATABASE } // exclude my id
            emit(Result.success(listOfUsersDataBase))
        }

        runCatchingNonCancellation {
            val listOfUsers: List<UserDomain> = userRepository.getListOfUsers()
            val listOfUsersPresence: List<UserPresenceDomain> = userRepository.getUsersPresence()
            val updateListOfUsers = listOfUsers.map {  user ->
                val userPresenceDomain = listOfUsersPresence.find { it.userEmail.equals(user.userEmail) }
                user.copy(userStatus = userPresenceDomain?.userStatus ?: "unknown")
            }
            updateListOfUsers
        }.fold(
            onSuccess = { updateListOfUsers ->
                userRepository.saveUsersToDataBase(UsersDomainToTableMapper.map(updateListOfUsers))
                emit(Result.success(updateListOfUsers))
            },
            onFailure = {
                emit(Result.failure(it))
            }
        )
    }
}