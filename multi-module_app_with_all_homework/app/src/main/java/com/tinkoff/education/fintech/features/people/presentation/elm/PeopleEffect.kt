package com.tinkoff.education.fintech.features.people.presentation.elm

import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

sealed interface PeopleEffect {

    data class ShowError(val throwable: Throwable) : PeopleEffect

    data class OpenUserProfile(val user: UserUi, val imageView: AppCompatImageView, val textView: TextView) : PeopleEffect
}