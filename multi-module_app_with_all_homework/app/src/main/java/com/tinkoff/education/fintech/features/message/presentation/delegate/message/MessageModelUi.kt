package com.tinkoff.education.fintech.features.message.presentation.delegate.message

import com.tinkoff.education.fintech.features.message.presentation.model.ReactionUi

data class MessageModelUi(
    val id: Int,
    val userId: Int,
    val myId: Int,
    val userName: String,
    val userMessage: String,
    val isMyMessage: Boolean,
    val timestamp: Long,
    val subject: String,
    val avatarUrl: String,
    val listOfReactions: List<ReactionUi> = emptyList()
)
