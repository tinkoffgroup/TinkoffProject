package com.tinkoff.education.fintech.features.people.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

data class PeopleState(
    val listOfPeople: ResultState<List<UserUi>>
)
