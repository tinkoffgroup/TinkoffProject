package com.tinkoff.education.fintech.features.message.domain.model

data class MessageDomain(
    val id: Int,
    val message: String,
    val avatarUrl: String,
    val timestamp: Long,
    val subject: String,
    val isMeMessage: Boolean,
    val senderId: Int,
    val myId: Int = 0,
    val senderFullName: String,
    val listOfReactions: List<ReactionDomain> = emptyList()
)

data class ReactionDomain(
    val emojiName: String,
    val emojiCode: String,
    val userId: Int
)
