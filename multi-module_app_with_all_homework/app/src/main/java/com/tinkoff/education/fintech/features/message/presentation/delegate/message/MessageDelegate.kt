package com.tinkoff.education.fintech.features.message.presentation.delegate.message

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.customview.EmojiView
import com.tinkoff.education.fintech.databinding.MessageItemBinding
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class MessageDelegate(
    private val onMessageListener: (
        messageModelUi: MessageModelUi
    ) -> Unit,
    private val onEmojiListener: (
        emojiUi: EmojiUi, message: MessageModelUi
    ) -> Unit
) : AdapterDelegate {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            MessageItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int,
        payloads: List<Any>
    ) {
        when (payloads.firstOrNull() as? MessageDelegateItem.ChangePayload) {
            is MessageDelegateItem.ChangePayload.ListOfReactionChanged ->
                (holder as ViewHolder).bindListOfReaction(item.content() as MessageModelUi, onMessageListener, onEmojiListener)

            else -> (holder as ViewHolder).bind(item.content() as MessageModelUi, onMessageListener, onEmojiListener)
        }
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is MessageDelegateItem

    class ViewHolder(
        private val binding: MessageItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindListOfReaction(
            model: MessageModelUi,
            onMessageListener: (
                messageModelUi: MessageModelUi
            ) -> Unit,
            onEmojiListener: (
                emojiUi: EmojiUi, message: MessageModelUi
            ) -> Unit
        ) {
            binding.message.removeAllViewsFromEmojis()
            val emojiUsage = model.listOfReactions
                .groupBy { it.emojiUi }
                .mapValues { it.value.map { reaction -> reaction.userId }.distinct().count() }
            emojiUsage.forEach { (emojiUi, count) ->
                val emojiView = EmojiView(itemView.context, defTheme = R.style.EmojiStyle)
                emojiView.emoji = emojiUi.getCodeString()
                emojiView.emojiCounter = count

                val reaction = model.listOfReactions.find { it.userId == model.myId && it.emojiUi.emojiCode == emojiUi.emojiCode }
                if (reaction != null) {
                    emojiView.isSelected = true
                }

                emojiView.setOnClickListener {
                    onEmojiListener.invoke(emojiUi, model)
                }

                binding.message.addEmoji(emojiView)
            }

            if (model.listOfReactions.isNotEmpty()) {
                binding.message.addPlus()
                binding.message.getPlus().setOnClickListener {
                    onMessageListener(model)
                }
            }

            binding.message.setOnLongClickListener {
                onMessageListener(model)
                true
            }
        }

        fun bind(
            model: MessageModelUi,
            onMessageListener: (
                messageModelUi: MessageModelUi
            ) -> Unit,
            onEmojiListener: (
                emojiUi: EmojiUi, message: MessageModelUi
            ) -> Unit
        ) {
            with(binding.message) {
                setName(model.userName)
                setMessage(model.userMessage)
                isMyMessage(model.isMyMessage)
                removeAllViewsFromEmojis()
                setAvatar(model.avatarUrl)
            }
            bindListOfReaction(model, onMessageListener, onEmojiListener)
        }
    }
}