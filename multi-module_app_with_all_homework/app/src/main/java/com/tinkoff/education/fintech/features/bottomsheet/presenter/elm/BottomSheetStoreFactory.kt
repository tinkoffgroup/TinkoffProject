package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.ElmStore
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class BottomSheetStoreFactory @Inject constructor(private val actor: BottomSheetActor) {

    fun create(): Store<BottomSheetEvent, BottomSheetEffect, BottomSheetState> {
        return ElmStore(
            initialState = BottomSheetState(bottomSheetState = ResultState.Loading),
            reducer = BottomSheetReducer(),
            actor = actor
        )
    }
}