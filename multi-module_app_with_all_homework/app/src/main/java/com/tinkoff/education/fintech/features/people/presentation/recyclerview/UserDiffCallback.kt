package com.tinkoff.education.fintech.features.people.presentation.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

class UserDiffCallback : DiffUtil.ItemCallback<UserUi>() {

    override fun areItemsTheSame(oldItem: UserUi, newItem: UserUi): Boolean {
        return oldItem === newItem
    }

    override fun areContentsTheSame(oldItem: UserUi, newItem: UserUi): Boolean {
        return oldItem == newItem
    }
}