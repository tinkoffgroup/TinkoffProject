package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

sealed interface StreamEvent {

    sealed interface Ui : StreamEvent {
        data class Init(val amISubscribed: Boolean) : Ui

        data class ClickOnStream(val amISubscribed: Boolean, val stream: StreamModelUi) : Ui

        data class ClickOnTopic(val topic: TopicModelUi) : Ui

        data class LongClickOnStream(val stream: StreamModelUi) : Ui

        data class SearchTopic(val amISubscribed: Boolean, val searchQuery: String) : Ui
    }

    sealed interface Domain : StreamEvent {
        data class DataLoaded(val listOfStreams: List<DelegateItem>) : Domain
        data class Error(val throwable: Throwable) : Domain
    }
}