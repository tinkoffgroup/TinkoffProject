package com.tinkoff.education.fintech.features.profile.data

import com.tinkoff.education.database.DataBaseDataSource
import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.message.data.mapper.UserApiToDomainMapper
import com.tinkoff.education.fintech.features.people.data.mapper.UserPresenceApiToDomainMapper
import com.tinkoff.education.fintech.features.people.data.mapper.UserTableToDomainMapper
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain
import com.tinkoff.education.fintech.features.profile.domain.ProfileRepository
import com.tinkoff.education.networking.datasource.api.ChatNetworkDataSource
import javax.inject.Inject

class ProfileRepositoryImpl @Inject constructor(
    private val chatNetworkDataSource: ChatNetworkDataSource,
    private val dataBaseDataSource: DataBaseDataSource
) : ProfileRepository {

    override suspend fun getUserMe(): UserDomain {
        return UserApiToDomainMapper.map(chatNetworkDataSource.getUserMe())
    }

    override suspend fun getUserPresence(id: Int): UserPresenceDomain {
        return UserPresenceApiToDomainMapper.map(chatNetworkDataSource.getUserPresence(id))
    }

    override suspend fun getUserByIdDataBase(id: Int): UserDomain? {
        val user: UserTable? = dataBaseDataSource.getUserById(id)
        return if (user == null) {
            null
        } else {
            UserTableToDomainMapper.map(user)
        }
    }

    override suspend fun saveUser(user: UserTable): Long {
        return dataBaseDataSource.saveUser(user)
    }
}