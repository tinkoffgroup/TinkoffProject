package com.tinkoff.education.fintech.features.people.presentation.elm

sealed class PeopleCommand {

    data object GetListOfUsers : PeopleCommand()

    data class GetListOfUsersBySearchQuery(val query: String) : PeopleCommand()
}