package com.tinkoff.education.fintech.features.channels.stream.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import vivid.money.elmslie.core.store.ElmStore
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class StreamStoreFactory @Inject constructor(private val actor: StreamActor) {

    fun create(): Store<StreamEvent, StreamEffect, StreamState> {
        return ElmStore(
            initialState = StreamState(listOfStreams = ResultState.Loading),
            reducer = StreamReducer(),
            actor = actor
        )
    }
}