package com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream

import android.os.Parcelable
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import kotlinx.parcelize.Parcelize

@Parcelize
data class StreamModelUi(
    val id: Int,
    val title: String,
    val isOpened: Boolean,
    val topics : List<TopicModelUi>
): Parcelable
