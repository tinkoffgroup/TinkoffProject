package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState

data class BottomSheetState(
    val bottomSheetState: ResultState<Any>,
    val listOfTopic: List<TopicModelUi> = emptyList()
)
