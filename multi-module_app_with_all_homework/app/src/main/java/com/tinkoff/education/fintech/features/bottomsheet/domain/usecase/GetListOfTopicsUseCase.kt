package com.tinkoff.education.fintech.features.bottomsheet.domain.usecase

import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.mapper.TopicsDomainToUiMapper
import javax.inject.Inject

class GetListOfTopicsUseCase @Inject constructor(
    private val bottomSheetRepository: BottomSheetRepository
) {

    suspend fun execute(streamName: String, topicName: String): List<TopicModelUi> {
        val listOfTopics = bottomSheetRepository.getTopicsByStream(streamName).filter {
            (it.name == topicName).not()
        }
        return TopicsDomainToUiMapper.map(listOfTopics, streamName)
    }
}