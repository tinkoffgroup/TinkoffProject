package com.tinkoff.education.fintech.features.channels.stream.data

import com.tinkoff.education.database.DataBaseDataSource
import com.tinkoff.education.database.model.StreamEntity
import com.tinkoff.education.fintech.features.channels.stream.data.mapper.StreamsApiToDomainMapper
import com.tinkoff.education.fintech.features.channels.stream.data.mapper.StreamsEntityToDomainMapper
import com.tinkoff.education.fintech.features.channels.stream.data.mapper.TopicsApiToDomainMapper
import com.tinkoff.education.fintech.features.channels.stream.data.mapper.UnreadMessagesByTopicApiToDomainMapper
import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.UnreadMessagesByTopicDomain
import com.tinkoff.education.networking.datasource.api.ChatNetworkDataSource
import javax.inject.Inject

class StreamAndTopicRepositoryImpl @Inject constructor(
    private val chatNetworkDataSource: ChatNetworkDataSource,
    private val dataBaseDataSource: DataBaseDataSource
) : StreamAndTopicRepository {

    override suspend fun deleteStreams(amISubscribed: Boolean) {
        return dataBaseDataSource.deleteStreams(amISubscribed)
    }

    override suspend fun getStreamsFromDataBase(amISubscribed: Boolean): List<StreamDomain> {
        return StreamsEntityToDomainMapper.map(dataBaseDataSource.getStreams(amISubscribed))
    }

    override suspend fun saveStreamsToDataBase(streams: List<StreamEntity>, amISubscribed: Boolean) {
        return dataBaseDataSource.saveStreams(streams, amISubscribed)
    }

    override suspend fun getAllStreams(): List<StreamDomain> {
        return StreamsApiToDomainMapper.map(chatNetworkDataSource.getAllStreams().streams)
    }

    override suspend fun getSubscribedStreams(): List<StreamDomain> {
        return StreamsApiToDomainMapper.map(chatNetworkDataSource.getSubscribedStreams().streams)
    }

    override suspend fun getTopicsByStreamId(id: Int): List<TopicDomain> {
        return TopicsApiToDomainMapper.map(chatNetworkDataSource.getTopicsByStreamId(id).topics)
    }

    override suspend fun getUnreadMessagesByTopicStreams(eventTypes: String): List<UnreadMessagesByTopicDomain> {
        return UnreadMessagesByTopicApiToDomainMapper.map(chatNetworkDataSource.getUnreadMessagesByTopicStreams(eventTypes).unreadMsgs.streams)
    }
}