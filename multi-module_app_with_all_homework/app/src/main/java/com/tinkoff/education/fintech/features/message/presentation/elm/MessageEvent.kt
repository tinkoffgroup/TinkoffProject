package com.tinkoff.education.fintech.features.message.presentation.elm

import androidx.paging.PagingData
import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem
import java.io.File

interface MessageEvent {

    sealed interface Ui : MessageEvent {
        data class Init(val streamName: String, val topicName: String) : Ui

        data class ClickOnSendButton(val stream: String, val topic: String, val message: String) : Ui

        data class LongClickOnSendButton(val stream: String, val topic: String, val file: File) : Ui

        data class ReloadPage(val streamName: String, val topicName: String) : Ui

        data class ClickOnEmoji(val emojiUi: EmojiUi, val messageModelUi: MessageModelUi, val streamName: String, val topicName: String) : Ui

        data class AddEmoji(val emojiUi: EmojiUi, val messageModelUi: MessageModelUi, val streamName: String, val topicName: String) : Ui

        data class ClickOnTopic(val streamName: String, val topicName: String) : Ui
    }

    sealed interface Domain : MessageEvent {
        data class DataLoaded(val pagingData: PagingData<DelegateItem>) : Domain
        data class Error(val throwable: Throwable) : Domain
    }
}