package com.tinkoff.education.fintech.features

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tinkoff.education.fintech.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}