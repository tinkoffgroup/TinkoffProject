package com.tinkoff.education.fintech.features.profile.domain

import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain

interface ProfileRepository {

    suspend fun getUserMe(): UserDomain

    suspend fun getUserPresence(id: Int): UserPresenceDomain

    suspend fun getUserByIdDataBase(id: Int): UserDomain?

    suspend fun saveUser(user: UserTable): Long
}