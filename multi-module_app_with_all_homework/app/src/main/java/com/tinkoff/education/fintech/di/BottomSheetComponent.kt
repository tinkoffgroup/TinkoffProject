package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.bottomsheet.BottomSheetFragment
import com.tinkoff.education.fintech.features.bottomsheet.presenter.elm.BottomSheetStoreFactory
import dagger.Subcomponent

@Subcomponent
interface BottomSheetComponent {

    fun inject(fragment: BottomSheetFragment)
    fun getBottomSheetStoreFactory(): BottomSheetStoreFactory
}