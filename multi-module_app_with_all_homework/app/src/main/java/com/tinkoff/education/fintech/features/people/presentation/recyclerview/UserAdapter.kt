package com.tinkoff.education.fintech.features.people.presentation.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.databinding.UserItemBinding
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

class UserAdapter(
    private val userDetailListener: (
        userUi: UserUi, imageView: AppCompatImageView, textView: TextView
    ) -> Unit
) : ListAdapter<UserUi, ViewHolderUser>(
    UserDiffCallback()
)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUser {
        val itemViewHolder = UserItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        val viewHolder = ViewHolderUser(itemViewHolder)
        setItemListener(viewHolder)
        return viewHolder
    }
    override fun onBindViewHolder(holder: ViewHolderUser, position: Int) {
        holder.apply {
            val current: UserUi = getItem(position)
            bind(current)
        }
    }

    private fun setItemListener(viewHolderUser: ViewHolderUser) {
        viewHolderUser.itemView.setOnClickListener {
            val position = viewHolderUser.bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                getItem(position)?.let {
                    userDetailListener.invoke(
                        it,
                        viewHolderUser.binding.userAvatar,
                        viewHolderUser.binding.userName)
                }
            }
        }
    }
}