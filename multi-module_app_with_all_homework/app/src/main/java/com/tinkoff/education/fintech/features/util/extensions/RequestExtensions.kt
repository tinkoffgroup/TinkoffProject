package com.tinkoff.education.fintech.features.util.extensions

import org.json.JSONArray
import org.json.JSONObject

fun createNarrow(stream: String, topic: String): String {
    val narrow = mutableListOf<JSONObject>()

    val narrowOfStream = JSONObject()
    narrowOfStream.put("operator", "stream")
    narrowOfStream.put("operand", stream)
    narrow.add(narrowOfStream)

    if (topic.isNotEmpty()) {
        val narrowOfTopic = JSONObject()
        narrowOfTopic.put("operator", "topic")
        narrowOfTopic.put("operand", topic)
        narrow.add(narrowOfTopic)
    }
    return narrow.toString()
}

fun createEventTypes(list: List<String>): String {
    val jsonArray = JSONArray()
    list.forEach {
        jsonArray.put(it)
    }
    return jsonArray.toString()
}