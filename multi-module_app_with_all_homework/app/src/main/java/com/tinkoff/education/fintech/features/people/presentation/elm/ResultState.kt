package com.tinkoff.education.fintech.features.people.presentation.elm

interface ResultState<out R> {

    data class Success<out T>(val data: T) : ResultState<T>
    data object Loading : ResultState<Nothing>
    data class Error(val throwable: Throwable) : ResultState<Nothing>
}