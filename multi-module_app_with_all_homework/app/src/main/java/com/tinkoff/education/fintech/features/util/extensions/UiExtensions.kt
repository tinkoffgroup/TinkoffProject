package com.tinkoff.education.fintech.features.util.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewPropertyAnimator
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.core.graphics.drawable.DrawableCompat
import com.bumptech.glide.Glide
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import com.tinkoff.education.fintech.features.people.presentation.model.UserStatus

fun ImageView.loadImage(imageUri: String?) {

    val image = imageUri ?: R.drawable.ic_launcher_foreground

    Glide.with(context)
        .load(image)
        .placeholder(R.drawable.ic_launcher_foreground)
        .error(R.drawable.ic_launcher_foreground)
        .into(this)
}

fun AppCompatImageView.setStatusColor(userStatus: UserStatus) {
    val drawable = AppCompatResources.getDrawable(context, R.drawable.ic_rectangle_status)
    val wrappedDrawable = drawable?.let { DrawableCompat.wrap(it) }
    wrappedDrawable?.let { DrawableCompat.setTint(it, ContextCompat.getColor(context, userStatus.color)) }
    this.setImageDrawable(wrappedDrawable)
}

fun showErrorDialog(context: Context?, init: () -> Unit) {
    val dialog = context?.let { Dialog(it) }
    dialog?.apply {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setContentView(R.layout.error_dialog)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val reloadButton: Button = findViewById(R.id.button)
        reloadButton.setOnClickListener {
            init.invoke()
            dismiss()
        }
    }?.show()
}

fun showDeleteRequestDialog(context: Context?, delete: () -> Unit) {
    val dialog = context?.let { Dialog(it) }
    dialog?.apply {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setContentView(R.layout.delete_request_dialog)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val cancelButton: Button = findViewById(R.id.cancel_button)
        cancelButton.setOnClickListener {
            dismiss()
        }
        val deleteButton: Button = findViewById(R.id.delete_button)
        deleteButton.setOnClickListener {
            delete.invoke()
            dismiss()
        }
    }?.show()
}

fun showEditRequestDialog(context: Context?, messageModelUi: MessageModelUi, edit: (String) -> Unit) {
    val dialog = context?.let { Dialog(it) }
    dialog?.apply {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setContentView(R.layout.edit_request_dialog)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val editMessage: EditText = findViewById(R.id.edit_message)
        editMessage.setText(messageModelUi.userMessage)
        val cancelButton: Button = findViewById(R.id.cancel_button)
        cancelButton.setOnClickListener {
            dismiss()
        }
        val submitButton: Button = findViewById(R.id.submit_button)
        submitButton.setOnClickListener {
            edit.invoke(editMessage.text.toString())
            dismiss()
        }
    }?.show()
}

fun changeTopicMessage(context: Context?, listOfTopics: List<String>, changeTopic: (String) -> Unit) {
    val dialog = context?.let { Dialog(it) }
    dialog?.apply {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setContentView(R.layout.change_topic_dialog)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val spinner: Spinner = findViewById(R.id.spinner_topics)
        val adapter = ArrayAdapter(context,
            android.R.layout.simple_spinner_item, listOfTopics)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        val cancelButton: Button = findViewById(R.id.cancel_button)
        cancelButton.setOnClickListener {
            dismiss()
        }
        val submitButton: Button = findViewById(R.id.submit_button)
        submitButton.setOnClickListener {
            changeTopic.invoke(spinner.selectedItem.toString())
            dismiss()
        }
    }?.show()
}

fun shareMessage(context: Context?, messageModelUi: MessageModelUi) {
    val intent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        type = "text/plain"
        putExtra(Intent.EXTRA_SUBJECT, "Quiz results")
        putExtra(Intent.EXTRA_TEXT, createShareMessageByTemplate(messageModelUi))
    }
    context?.let { startActivity(context, intent, null) }
}

fun createShareMessageByTemplate(messageModelUi: MessageModelUi): String {
    return "User: ${messageModelUi.userName}\nSend message: ${messageModelUi.userMessage}\ntime: ${messageModelUi.timestamp}"
}

fun animateView(view: AppCompatImageView, resId: Int, lambda: () -> Unit) {
    val viewPropertyAnimator = view.animate()
    viewPropertyAnimator
        .scaleX(0.5f)
        .scaleY(0.5f)
        .setDuration(250)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                populateFavoriteIconView(view, resId)
                restartFavoriteIconSize(viewPropertyAnimator, view, lambda)
            }

            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
                view.isClickable = false
            }
        })
}

private fun populateFavoriteIconView(view: AppCompatImageView, resId: Int) {
    view.setImageResource(resId)
}

private fun restartFavoriteIconSize(viewPropertyAnimator: ViewPropertyAnimator, view: View, lambda: () -> Unit) {
    viewPropertyAnimator
        .scaleX(1f)
        .scaleY(1f)
        .setDuration(250)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                viewPropertyAnimator.cancel()
                lambda.invoke()
                view.isClickable = true
            }
        })
}
