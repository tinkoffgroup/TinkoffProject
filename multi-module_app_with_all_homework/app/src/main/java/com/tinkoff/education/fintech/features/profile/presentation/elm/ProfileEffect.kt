package com.tinkoff.education.fintech.features.profile.presentation.elm

sealed interface ProfileEffect {

    data class ShowError(val throwable: Throwable) : ProfileEffect
}