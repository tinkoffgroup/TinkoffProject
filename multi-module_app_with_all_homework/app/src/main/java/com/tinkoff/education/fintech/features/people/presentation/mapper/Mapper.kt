package com.tinkoff.education.fintech.features.people.presentation.mapper

import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.presentation.model.UserStatus
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

object UsersDomainToUiMapper {
    fun map(type: List<UserDomain>): List<UserUi> {
        return type.map {
            UserUi(
                id = it.id,
                userName = it.userName,
                userEmail = it.userEmail,
                userUrl = it.userUrl ?: "https://i.ytimg.com/vi/YCaGYUIfdy4/maxresdefault.jpg",
                userStatus = UserStatus.getUserStatus(it.userStatus ?: "unknown")
            )
        }
    }
}