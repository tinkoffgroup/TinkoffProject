package com.tinkoff.education.fintech.features.message.presentation.delegate.date

data class DateModelUi(
    val date: String
)
