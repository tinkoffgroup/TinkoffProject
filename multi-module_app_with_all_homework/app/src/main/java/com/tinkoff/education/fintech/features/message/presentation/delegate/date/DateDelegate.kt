package com.tinkoff.education.fintech.features.message.presentation.delegate.date

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.databinding.DateItemBinding
import com.tinkoff.education.fintech.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class DateDelegate : AdapterDelegate {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            DateItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int,
        payloads: List<Any>
    ) {
        (holder as ViewHolder).bind(item.content() as DateModelUi)
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is DateDelegateItem

    class ViewHolder(
        private val binding: DateItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: DateModelUi) {
            binding.date.text = model.date
        }
    }
}