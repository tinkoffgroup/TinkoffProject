package com.tinkoff.education.fintech.features.channels.stream.domain.usecase

import com.tinkoff.education.fintech.features.channels.stream.data.mapper.StreamsDomainToEntityMapper
import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi
import javax.inject.Inject

class GetTopicsByStreamUseCase @Inject constructor(
    private val streamAndTopicRepository: StreamAndTopicRepository
) {

    suspend fun execute(amISubscribed: Boolean, stream: StreamModelUi): List<StreamDomain> {
        val listOfStreams: List<StreamDomain> = streamAndTopicRepository.getStreamsFromDataBase(amISubscribed)
        val updatedListOfStreams = listOfStreams.map {
            if (it.id == stream.id) {
                it.copy(isOpened = !it.isOpened)
            } else {
                it
            }
        }
        streamAndTopicRepository.saveStreamsToDataBase(
            StreamsDomainToEntityMapper.map(updatedListOfStreams, amISubscribed), amISubscribed
        )
        return updatedListOfStreams
    }
}