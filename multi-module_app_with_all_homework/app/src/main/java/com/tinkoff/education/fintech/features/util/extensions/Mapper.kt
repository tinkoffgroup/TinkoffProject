package com.tinkoff.education.fintech.features.util.extensions

import android.icu.text.SimpleDateFormat
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamDelegateItem
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicDelegateItem
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem
import java.util.Date
import java.util.Locale

const val MY_ID_IN_DATABASE = 5

fun Long.toDayAndMonth(): String {
    val simpleDateFormat = SimpleDateFormat("dd MMM", Locale.ENGLISH)
    val date = Date(this * 1000)
    return simpleDateFormat.format(date)
}

object TopicUiToDelegateMapper {
    fun map(topics: List<TopicModelUi>): List<DelegateItem> {
        val result = mutableListOf<DelegateItem>()
        topics.forEach { topic ->
            result.add(TopicDelegateItem(topic.id, topic))
        }
        return result
    }
}

object StreamUiAndTopicUiToDelegateMapper {
    fun map(streams: List<StreamModelUi>): List<DelegateItem> {
        val result = mutableListOf<DelegateItem>()
        streams.forEach { stream ->
            result.add(StreamDelegateItem(stream.id, stream))
            if (stream.isOpened) {
                stream.topics.forEach { topic ->
                    result.add(TopicDelegateItem(stream.id, topic))
                }
            }
        }
        return result
    }
}