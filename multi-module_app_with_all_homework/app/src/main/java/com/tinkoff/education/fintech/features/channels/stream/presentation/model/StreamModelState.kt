package com.tinkoff.education.fintech.features.channels.stream.presentation.model

sealed interface StreamModelState {
    data object Initial : StreamModelState
    data object Loading : StreamModelState
    data object Error : StreamModelState
    data object DataLoaded : StreamModelState
}