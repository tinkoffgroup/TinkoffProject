package com.tinkoff.education.fintech.features.profile.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

sealed interface ProfileEvent {

    sealed interface Ui : ProfileEvent {
        data object Init : Ui
    }

    sealed interface Domain : ProfileEvent {
        data class DataLoaded(val user: UserUi) : Domain
        data class Error(val throwable: Throwable) : Domain
    }
}