package com.tinkoff.education.fintech.features.message.domain.usecase

import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import javax.inject.Inject

class SendMessageUseCase @Inject constructor(
    private val messageRepository: MessageRepository
) {

    suspend fun execute(stream: String, topic: String, message: String): Any {
        return messageRepository.sendMessage(stream, topic, message)
    }
}