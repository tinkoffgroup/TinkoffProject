package com.tinkoff.education.fintech.features.people.presentation.model

import android.os.Parcelable
import com.tinkoff.education.fintech.R
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserUi(
    val id: Int,
    val userName: String,
    val userEmail: String,
    val userUrl: String,
    val userStatus: UserStatus
): Parcelable

enum class UserStatus(val status: String, val color: Int) {
    ACTIVE("active", R.color.green),
    IDLE("idle", R.color.orange),
    OFFLINE("offline", R.color.red),
    UNKNOWN("unknown", R.color.white);

    companion object {
        fun getUserStatus(value: String): UserStatus = entries.find { it.status == value } ?: UNKNOWN
    }
}