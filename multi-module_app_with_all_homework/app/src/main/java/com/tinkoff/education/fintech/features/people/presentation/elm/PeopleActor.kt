package com.tinkoff.education.fintech.features.people.presentation.elm

import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.usecase.GetListOfUsersBySearchQueryUseCase
import com.tinkoff.education.fintech.features.people.domain.usecase.GetListOfUsersUseCase
import com.tinkoff.education.fintech.features.people.presentation.mapper.UsersDomainToUiMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import vivid.money.elmslie.core.store.Actor
import javax.inject.Inject

class PeopleActor @Inject constructor(
    private val getListOfUsersUseCase: GetListOfUsersUseCase,
    private val getListOfUsersBySearchQueryUseCase: GetListOfUsersBySearchQueryUseCase
) : Actor<PeopleCommand, PeopleEvent>() {

    override fun execute(command: PeopleCommand): Flow<PeopleEvent> {
        return when (command) {
            is PeopleCommand.GetListOfUsers -> flow {
                val flow: Flow<Result<List<UserDomain>>> = getListOfUsersUseCase.execute()
                flow.map { result ->
                    result.fold(
                        onSuccess = { users: List<UserDomain> ->
                            emit(PeopleEvent.Domain.DataLoaded(UsersDomainToUiMapper.map(users)))
                        },
                        onFailure = {
                            emit(PeopleEvent.Domain.Error(it))
                        }
                    )
                }.collect()
            }

            is PeopleCommand.GetListOfUsersBySearchQuery -> flow {
                runCatching {
                    getListOfUsersBySearchQueryUseCase.execute(command.query)
                }.fold(
                    onSuccess = {
                        emit(PeopleEvent.Domain.DataLoaded(UsersDomainToUiMapper.map(it)))
                    },
                    onFailure = {
                        emit(PeopleEvent.Domain.Error(it))
                    }
                )
            }
        }
    }
}