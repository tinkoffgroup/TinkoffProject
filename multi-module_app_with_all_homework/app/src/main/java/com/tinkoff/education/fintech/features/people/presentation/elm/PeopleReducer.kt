package com.tinkoff.education.fintech.features.people.presentation.elm

import vivid.money.elmslie.core.store.dsl.ScreenDslReducer

class PeopleReducer : ScreenDslReducer<PeopleEvent,
        PeopleEvent.Ui,
        PeopleEvent.Domain,
        PeopleState,
        PeopleEffect,
        PeopleCommand>(
    PeopleEvent.Ui::class, PeopleEvent.Domain::class
) {

    override fun Result.internal(event: PeopleEvent.Domain) = when (event) {
        is PeopleEvent.Domain.DataLoaded -> {
            state {
                copy(listOfPeople = ResultState.Success(event.listOfPeople))
            }
        }
        is PeopleEvent.Domain.Error -> {
            effects {
                +PeopleEffect.ShowError(event.throwable)
            }
        }
    }

    override fun Result.ui(event: PeopleEvent.Ui) = when (event) {
        PeopleEvent.Ui.Init -> {
            state {
                copy(listOfPeople = ResultState.Loading)
            }
            commands {
                +PeopleCommand.GetListOfUsers
            }
        }

        is PeopleEvent.Ui.SearchUser -> {
            state {
                copy(listOfPeople = ResultState.Loading)
            }
            commands {
                +PeopleCommand.GetListOfUsersBySearchQuery(event.searchQuery)
            }
        }

        is PeopleEvent.Ui.ClickOnUser -> {
            effects {
                +PeopleEffect.OpenUserProfile(event.user, event.imageView, event.textView)
            }
        }
    }
}