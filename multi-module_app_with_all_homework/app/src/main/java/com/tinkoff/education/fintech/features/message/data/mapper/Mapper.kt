package com.tinkoff.education.fintech.features.message.data.mapper

import android.text.Html
import com.tinkoff.education.database.model.MessageTable
import com.tinkoff.education.database.model.ReactionTable
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.networking.datasource.api.model.MessageApi
import com.tinkoff.education.networking.datasource.api.model.UserApi

object MessagesApiToTableMapper {
    fun map(type: List<MessageApi>, userMe: UserApi, hashCodeOfStreamsAndTopics: Int): List<MessageTable> {
        return type.map {
            MessageTable(
                messageId = it.id,
                message = Html.fromHtml(it.content, Html.FROM_HTML_MODE_COMPACT).toString(),
                avatarUrl = it.avatarUrl,
                timestamp = it.timestamp,
                subject = it.subject,
                isMeMessage = it.senderId == userMe.id,
                senderId = it.senderId,
                myId = userMe.id,
                senderFullName = it.senderFullName,
                hashCodeOfStreamsAndTopics = hashCodeOfStreamsAndTopics
            )
        }
    }
}

object MessagesApiGetReactionTableMapper {
    fun map(type: List<MessageApi>, hashCodeOfStreamsAndTopics: Int): List<ReactionTable> {
        return type.flatMap { message ->
            message.reactions.map { reaction ->
                ReactionTable(
                    emojiName = reaction.emojiName,
                    emojiCode = reaction.emojiCode,
                    userId = reaction.userId,
                    messageId = message.id,
                    hashCodeOfStreamsAndTopics = hashCodeOfStreamsAndTopics
                )
            }
        }
    }
}

object UserApiToDomainMapper {
    fun map(type: UserApi): UserDomain {
        return UserDomain(
                id = type.id,
                userName = type.userName,
                userEmail = type.userEmail,
                userUrl = type.userUrl,
                userStatus = null
            )
    }
}