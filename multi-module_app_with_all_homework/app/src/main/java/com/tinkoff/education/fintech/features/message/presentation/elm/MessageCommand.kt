package com.tinkoff.education.fintech.features.message.presentation.elm

import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import java.io.File

sealed class MessageCommand {

    data class GetMessages(val streamName: String, val topicName: String) : MessageCommand()

    data class SendFile(val stream: String, val topic: String, val file: File) : MessageCommand()

    data class SendMessage(val stream: String, val topic: String, val message: String) : MessageCommand()

    data class HandleEmojiClick(val emojiUi: EmojiUi, val messageModelUi: MessageModelUi, val streamName: String, val topicName: String) : MessageCommand()

    data class AddEmoji(val emojiUi: EmojiUi, val messageModelUi: MessageModelUi, val streamName: String, val topicName: String) : MessageCommand()
}