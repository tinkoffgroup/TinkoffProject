package com.tinkoff.education.fintech.features.profile.presentation.elm

import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

data class ProfileState(
    val user: ResultState<UserUi>
)