package com.tinkoff.education.fintech.features.bottomsheet.domain.usecase

import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import javax.inject.Inject

class DeleteMessageUseCase @Inject constructor(
    private val bottomSheetRepository: BottomSheetRepository
) {

    suspend fun execute(messageId: Int): Any {
        return bottomSheetRepository.deleteMessage(messageId)
    }
}