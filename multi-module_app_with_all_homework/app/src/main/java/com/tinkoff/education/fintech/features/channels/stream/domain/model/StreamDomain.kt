package com.tinkoff.education.fintech.features.channels.stream.domain.model

data class StreamDomain(
    val id: Int,
    val name: String,
    val isOpened: Boolean = false,
    val topics : List<TopicDomain> = emptyList()
)
