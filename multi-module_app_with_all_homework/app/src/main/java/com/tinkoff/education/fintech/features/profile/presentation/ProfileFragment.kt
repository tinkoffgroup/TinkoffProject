package com.tinkoff.education.fintech.features.profile.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.databinding.FragmentProfileBinding
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import com.tinkoff.education.fintech.features.profile.presentation.elm.ProfileEffect
import com.tinkoff.education.fintech.features.profile.presentation.elm.ProfileEvent
import com.tinkoff.education.fintech.features.profile.presentation.elm.ProfileState
import com.tinkoff.education.fintech.features.profile.presentation.elm.ProfileStoreFactory
import com.tinkoff.education.fintech.features.util.extensions.loadImage
import com.tinkoff.education.fintech.features.util.extensions.showErrorDialog
import vivid.money.elmslie.android.renderer.ElmRendererDelegate
import vivid.money.elmslie.android.renderer.elmStoreWithRenderer
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class ProfileFragment : Fragment(), ElmRendererDelegate<ProfileEffect, ProfileState> {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    val store: Store<ProfileEvent, ProfileEffect, ProfileState> by elmStoreWithRenderer(
        elmRenderer = this
    ) {
        profileStoreFactory.create()
    }

    private val init: () -> Unit = {
        store.accept(ProfileEvent.Ui.Init)
    }

    @Inject
    lateinit var profileStoreFactory: ProfileStoreFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        (requireActivity().applicationContext as App).appComponent.createProfileComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            store.accept(ProfileEvent.Ui.Init)
        }
    }

    override fun handleEffect(effect: ProfileEffect){
        when (effect) {
            is ProfileEffect.ShowError -> {
                binding.skeleton.root.visibility = View.INVISIBLE
                binding.profile.root.visibility = View.INVISIBLE
                showErrorDialog(context, init)
            }
        }
    }

    override fun render(state: ProfileState) {
        when (val dataToRender = state.user) {
            is ResultState.Success -> {
                val user = dataToRender.data
                with(binding) {
                    skeleton.root.visibility = View.INVISIBLE
                    profile.root.visibility = View.VISIBLE
                    skeleton.shimmerView.stopShimmer()
                    profile.avatar.loadImage(user.userUrl)
                    profile.userName.text = user.userName
                    profile.status.text = user.userStatus.status
                    profile.status.setTextColor(AppCompatResources.getColorStateList(requireContext(), user.userStatus.color))
                }
            }
            is ResultState.Loading -> {
                with(binding) {
                    skeleton.root.visibility = View.VISIBLE
                    profile.root.visibility = View.INVISIBLE
                    skeleton.shimmerView.startShimmer()
                }
            }
            is ResultState.Error -> Unit
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}