package com.tinkoff.education.fintech.features.profile.domain.usecase

import com.tinkoff.education.fintech.features.people.data.mapper.UserDomainToTableMapper
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.profile.domain.ProfileRepository
import com.tinkoff.education.fintech.features.util.extensions.MY_ID_IN_DATABASE
import com.tinkoff.education.fintech.features.util.extensions.runCatchingNonCancellation
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetUserMeUseCase @Inject constructor(
    private val profileRepository: ProfileRepository
) {

    suspend fun execute(): Flow<Result<UserDomain>> = flow {
        val localData: UserDomain?  = profileRepository.getUserByIdDataBase(MY_ID_IN_DATABASE)
        if (localData != null) {
            emit(Result.success(localData))
        }
        runCatchingNonCancellation {
            val userMe = profileRepository.getUserMe()
            val presenceResult = profileRepository.getUserPresence(userMe.id)
            userMe.copy(userStatus = presenceResult.userStatus)
        }.fold(
            onSuccess = { userMe ->
                emit(Result.success(userMe))
                profileRepository.saveUser(UserDomainToTableMapper.map(userMe).copy(id = MY_ID_IN_DATABASE))
            },
            onFailure = {
                emit(Result.failure(it))
            }
        )
    }
}