package com.tinkoff.education.fintech.features.channels.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tinkoff.education.fintech.features.channels.vm.model.SharedModelState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SharedViewModel : ViewModel() {

    private val _sharedState = MutableStateFlow<SharedModelState>(SharedModelState("", true))
    val sharedState: StateFlow<SharedModelState> = _sharedState

    fun searchTopic(query: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val newSharedState =
                _sharedState.value.copy(searchQuery = query, getAllStreamsAndTopics = false)

            withContext(Dispatchers.Main) {
                _sharedState.value = newSharedState
            }
        }
    }

    fun getAllStreamsAndTopics() {
        viewModelScope.launch(Dispatchers.IO) {
            val newSharedState =
                _sharedState.value.copy(searchQuery = "", getAllStreamsAndTopics = true)

            withContext(Dispatchers.Main) {
                _sharedState.value = newSharedState
            }
        }
    }
}