package com.tinkoff.education.fintech.features.channels.stream.domain.usecase

import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import javax.inject.Inject

class GetListOfTopicsBySearchQueryUseCase @Inject constructor(
    private val streamAndTopicRepository: StreamAndTopicRepository
) {

    suspend fun execute(amISubscribed: Boolean, query: String): List<StreamDomain> {

        val streams: List<StreamDomain> = streamAndTopicRepository.getStreamsFromDataBase(amISubscribed)
        return streams.map { stream ->
            StreamDomain(
                id = stream.id,
                name = stream.name,
                isOpened = stream.isOpened,
                topics = stream.topics.filter {
                    it.name.startsWith(query, ignoreCase = true)
                }
            )
        }.filter { it.topics.isNotEmpty() }
    }
}