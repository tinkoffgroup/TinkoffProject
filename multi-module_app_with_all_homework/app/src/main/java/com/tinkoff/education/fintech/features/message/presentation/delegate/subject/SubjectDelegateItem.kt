package com.tinkoff.education.fintech.features.message.presentation.delegate.subject

import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class SubjectDelegateItem(
    val id: Int,
    private val value: SubjectModelUi,
) : DelegateItem {
    override fun content(): Any = value
    override fun id(): Int = id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as SubjectDelegateItem).value == content()
    }
}