package com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.databinding.StreamItemBinding
import com.tinkoff.education.fintech.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fintech.features.util.delegate.DelegateItem

class StreamDelegate(
    private val streamDetailsListener: (
        streamModelUi: StreamModelUi
    ) -> Unit,
    private val longClickOnStream: (
        streamModelUi: StreamModelUi
    ) -> Unit
) : AdapterDelegate {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            StreamItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            streamDetailsListener,
            longClickOnStream
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int,
        payloads: List<Any>
    ) {
        (holder as ViewHolder).bind(item.content() as StreamModelUi)
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is StreamDelegateItem

    class ViewHolder(
        private val binding: StreamItemBinding,
        private val streamDetailsListener: (
            streamModelUi: StreamModelUi
        ) -> Unit,
        private val longClickOnStream: (
            streamModelUi: StreamModelUi
        ) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: StreamModelUi) {
            with(binding) {
                title.text = model.title

                if (model.isOpened) {
                    binding.button.setImageDrawable(AppCompatResources.getDrawable(itemView.context, R.drawable.ic_arrow_opened))
                } else {
                    binding.button.setImageDrawable(AppCompatResources.getDrawable(itemView.context, R.drawable.ic_arrow_closed))
                }

                itemView.setOnLongClickListener {
                    longClickOnStream.invoke(model)
                    true
                }

                itemView.setOnClickListener {
                    streamDetailsListener.invoke(model)
                }
            }
        }
    }
}