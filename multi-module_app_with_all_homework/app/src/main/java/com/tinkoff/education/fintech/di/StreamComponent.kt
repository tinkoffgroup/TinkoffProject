package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.channels.stream.presentation.StreamFragment
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamStoreFactory
import dagger.Subcomponent

@Subcomponent
interface StreamComponent {

    fun inject(fragment: StreamFragment)
    fun getStreamStoreFactory(): StreamStoreFactory
}