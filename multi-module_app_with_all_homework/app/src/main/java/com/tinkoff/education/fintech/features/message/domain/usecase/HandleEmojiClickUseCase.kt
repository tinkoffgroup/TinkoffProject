package com.tinkoff.education.fintech.features.message.domain.usecase

import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi
import javax.inject.Inject

class HandleEmojiClickUseCase @Inject constructor(
    private val messageRepository: MessageRepository
) {

    suspend fun execute(emojiUi: EmojiUi, messageModelUi: MessageModelUi): Any {
        val reactions = messageModelUi.listOfReactions.filter { it.emojiUi == emojiUi }
        var isMyEmoji = false
        reactions.forEach { reaction ->
            if (reaction.userId == messageModelUi.myId) {
                isMyEmoji = true
            }
        }
        return if (isMyEmoji) {
            messageRepository.removeMessageReaction(messageModelUi.id, emojiUi.emojiName)
        } else {
            messageRepository.addMessageReaction(messageModelUi.id, emojiUi.emojiName)
        }
    }
}