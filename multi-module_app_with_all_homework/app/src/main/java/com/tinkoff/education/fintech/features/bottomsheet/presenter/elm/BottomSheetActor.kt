package com.tinkoff.education.fintech.features.bottomsheet.presenter.elm

import com.tinkoff.education.fintech.features.bottomsheet.domain.usecase.ChangeTopicMessageUseCase
import com.tinkoff.education.fintech.features.bottomsheet.domain.usecase.DeleteMessageUseCase
import com.tinkoff.education.fintech.features.bottomsheet.domain.usecase.EditMessageUseCase
import com.tinkoff.education.fintech.features.bottomsheet.domain.usecase.GetListOfTopicsUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import vivid.money.elmslie.core.store.Actor
import javax.inject.Inject

class BottomSheetActor @Inject constructor(
    private val editMessageUseCase: EditMessageUseCase,
    private val deleteMessageUseCase: DeleteMessageUseCase,
    private val getListOfTopicsUseCase: GetListOfTopicsUseCase,
    private val changeTopicMessageUseCase: ChangeTopicMessageUseCase
) : Actor<BottomSheetCommand, BottomSheetEvent>() {

    override fun execute(command: BottomSheetCommand): Flow<BottomSheetEvent> {
        return when (command) {
            is BottomSheetCommand.EditMessage -> flow {
                runCatching {
                    editMessageUseCase.execute(command.editedMessage, command.messageId)
                }.fold(
                    onSuccess = {
                        emit(BottomSheetEvent.Domain.Success)
                    },
                    onFailure = {
                        emit(BottomSheetEvent.Domain.Error(it))
                    }
                )
            }

            is BottomSheetCommand.DeleteMessage -> flow {
                runCatching {
                    deleteMessageUseCase.execute(command.messageId)
                }.fold(
                    onSuccess = {
                        emit(BottomSheetEvent.Domain.Success)
                    },
                    onFailure = {
                        emit(BottomSheetEvent.Domain.Error(it))
                    }
                )
            }

            is BottomSheetCommand.GetListOfTopics -> flow {
                runCatching {
                    getListOfTopicsUseCase.execute(command.streamName, command.topicName)
                }.fold(
                    onSuccess = {
                        emit(BottomSheetEvent.Domain.DataLoaded(it))
                    },
                    onFailure = {
                        emit(BottomSheetEvent.Domain.Error(it))
                    }
                )
            }

            is BottomSheetCommand.ChangeTopicMessage -> flow {
                runCatching {
                    changeTopicMessageUseCase.execute(command.newTopic, command.messageId)
                }.fold(
                    onSuccess = {
                        emit(BottomSheetEvent.Domain.Success)
                    },
                    onFailure = {
                        emit(BottomSheetEvent.Domain.Error(it))
                    }
                )
            }
        }
    }
}