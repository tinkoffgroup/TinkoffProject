package com.tinkoff.education.fintech.features.people.presentation.elm

import vivid.money.elmslie.core.store.ElmStore
import vivid.money.elmslie.core.store.Store
import javax.inject.Inject

class PeopleStoreFactory @Inject constructor(private val actor: PeopleActor) {

    fun create(): Store<PeopleEvent, PeopleEffect, PeopleState> {
        return ElmStore(
            initialState = PeopleState(listOfPeople = ResultState.Loading),
            reducer = PeopleReducer(),
            actor = actor
        )
    }
}