package com.tinkoff.education.fintech.data

import com.tinkoff.education.fintech.features.people.presentation.model.UserStatus
import com.tinkoff.education.fintech.features.people.presentation.model.UserUi

class PeopleTestData {
    val searchQuery = "testQuery"
    val user = UserUi(1, "userName", "userEmail", "userUrl", userStatus = UserStatus.getUserStatus("unknown"))
}