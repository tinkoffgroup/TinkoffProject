package com.tinkoff.education.fintech.data

import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.stream.StreamModelUi

class StreamTestData {
    val amISubscribed = true
    val stream = StreamModelUi(1, "Title", true, emptyList())
    val searchQuery = "testQuery"
}