package com.tinkoff.education.fintech

import com.tinkoff.education.fintech.data.StreamTestData
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamCommand
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamEvent
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamReducer
import com.tinkoff.education.fintech.features.channels.stream.presentation.elm.StreamState
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldContainOnly
import io.kotest.matchers.shouldBe
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class StreamTest : BehaviorSpec({
    StreamTestData().apply {
        Given("StreamReducer") {
            val reducer = StreamReducer()
            When("reduce") {
                And("state is initial") {
                    And("event is Init") {
                        val actual = reducer.reduce(event = StreamEvent.Ui.Init(amISubscribed), state = StreamState(
                            listOfStreams = ResultState.Loading)
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = StreamCommand.GetListOfStreams(amISubscribed)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfStreams shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                    And("event is user click on stream") {
                        val actual = reducer.reduce(event = StreamEvent.Ui.ClickOnStream(amISubscribed, stream), state = StreamState(
                            listOfStreams = ResultState.Loading)
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = StreamCommand.AddTopicsByStream(amISubscribed, stream)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfStreams shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                    And("event is user click on topic") {
                        val topic = TopicModelUi(1, "Title", 0, "StreamTitle")
                        val actual = reducer.reduce(event = StreamEvent.Ui.ClickOnTopic(topic), state = StreamState(
                            listOfStreams = ResultState.Loading)
                        )
                        val expectedStatus = ResultState.Loading
                        Then("should return result with status is $expectedStatus") {
                            actual.state.listOfStreams shouldBe expectedStatus
                        }
                    }
                    And("event is user search topic") {
                        val actual = reducer.reduce(event = StreamEvent.Ui.SearchTopic(amISubscribed, searchQuery), state = StreamState(
                            listOfStreams = ResultState.Loading)
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = StreamCommand.GetListOfTopicsBySearchQuery(amISubscribed, searchQuery)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfStreams shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                }
            }
        }
    }
})