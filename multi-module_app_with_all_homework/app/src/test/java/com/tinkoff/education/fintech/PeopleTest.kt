package com.tinkoff.education.fintech

import com.tinkoff.education.fintech.data.PeopleTestData
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleCommand
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleEffect
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleEvent
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleState
import com.tinkoff.education.fintech.features.people.presentation.elm.PeopleReducer
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldContainOnly
import io.kotest.matchers.shouldBe
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PeopleTest : BehaviorSpec({
    PeopleTestData().apply {
        Given("PeopleReducer") {
            val reducer = PeopleReducer()
            When("reduce") {
                And("state is initial") {
                    And("event is Init") {
                        val actual = reducer.reduce(event = PeopleEvent.Ui.Init, state = PeopleState(
                            listOfPeople = ResultState.Success(emptyList())))
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = PeopleCommand.GetListOfUsers
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfPeople shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                    And("event is user search another users") {
                        val actual = reducer.reduce(event = PeopleEvent.Ui.SearchUser(searchQuery), state = PeopleState(
                            listOfPeople = ResultState.Success(emptyList())))
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = PeopleCommand.GetListOfUsersBySearchQuery(searchQuery)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfPeople shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                    And("event is user click on user") {
                        val actual = reducer.reduce(event = PeopleEvent.Ui.ClickOnUser(user),
                            state = PeopleState(listOfPeople = ResultState.Success(data= listOf(user))))
                        val expectedStatus = ResultState.Success(data = listOf(user))
                        val expectedEffect = listOf(PeopleEffect.OpenUserProfile(user))
                        Then("should return result with status is $expectedStatus and command $expectedEffect") {
                            actual.state.listOfPeople shouldBe expectedStatus
                            actual.effects shouldBe expectedEffect
                        }
                    }
                }
            }
        }
    }
})