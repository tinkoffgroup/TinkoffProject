package com.tinkoff.education.fintech

import androidx.paging.PagingData
import com.tinkoff.education.fintech.data.MessageTestData
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageCommand
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageEffect
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageEvent
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageReducer
import com.tinkoff.education.fintech.features.message.presentation.elm.MessageState
import com.tinkoff.education.fintech.features.people.presentation.elm.ResultState
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldContainOnly
import io.kotest.matchers.shouldBe
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MessageTest : BehaviorSpec({
    MessageTestData().apply {
        Given("MessageReducer") {
            val reducer = MessageReducer()
            When("reduce") {
                And("state is initial") {
                    And("event is Init") {
                        val actual = reducer.reduce(event = MessageEvent.Ui.Init(streamName, topicName), state = MessageState(
                            listOfMessages = ResultState.Success(PagingData.empty()))
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = MessageCommand.GetMessages(streamName, topicName)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfMessages shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                    And("event is user send message") {
                        val actual = reducer.reduce(event = MessageEvent.Ui.ClickOnSendButton(streamName, topicName, message),
                            state = MessageState(listOfMessages = ResultState.Success(PagingData.empty())))
                            val expectedEffect = listOf(MessageEffect.HandleClick)
                        val expectedCommand = MessageCommand.SendMessage(streamName, topicName, message)
                        Then("should return result with status is $ and command $expectedCommand") {
                            actual.commands.shouldContainOnly(expectedCommand)
                            actual.effects shouldBe expectedEffect
                        }
                    }
                    And("event is user reload the page") {
                        val actual = reducer.reduce(event = MessageEvent.Ui.ReloadPage(streamName, topicName), state = MessageState(
                            listOfMessages = ResultState.Success(PagingData.empty()))
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = MessageCommand.GetMessages(streamName, topicName)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfMessages shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                        }
                    }
                    And("event is user click on emoji") {
                        val actual = reducer.reduce(event = MessageEvent.Ui.ClickOnEmoji(emoji, messageModelUi, streamName, topicName), state = MessageState(
                            listOfMessages = ResultState.Success(PagingData.empty()))
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = MessageCommand.HandleEmojiClick(emoji, messageModelUi, streamName, topicName)
                        val expectedEffect = listOf(MessageEffect.HandleClick)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfMessages shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                            actual.effects shouldBe expectedEffect
                        }
                    }
                    And("event is user add emoji") {
                        val actual = reducer.reduce(event = MessageEvent.Ui.AddEmoji(emoji, messageModelUi, streamName, topicName), state = MessageState(
                            listOfMessages = ResultState.Success(PagingData.empty()))
                        )
                        val expectedStatus = ResultState.Loading
                        val expectedCommand = MessageCommand.AddEmoji(emoji, messageModelUi, streamName, topicName)
                        val expectedEffect = listOf(MessageEffect.HandleClick)
                        Then("should return result with status is $expectedStatus and command $expectedCommand") {
                            actual.state.listOfMessages shouldBe expectedStatus
                            actual.commands.shouldContainOnly(expectedCommand)
                            actual.effects shouldBe expectedEffect
                        }
                    }
                }
            }
        }
    }
})