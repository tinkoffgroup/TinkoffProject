package com.tinkoff.education.fintech.data

import com.tinkoff.education.fintech.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fintech.features.message.presentation.delegate.message.MessageModelUi

class MessageTestData {
    val streamName = "streamName"
    val topicName = "topicName"
    val message = "message"
    val emoji = EmojiUi("grinning", "1f600")
    val messageModelUi = MessageModelUi(1, 2, 3, "userName", "userMessage", true, 111, "", emptyList())
}