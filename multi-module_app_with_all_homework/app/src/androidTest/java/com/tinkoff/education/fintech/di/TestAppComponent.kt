package com.tinkoff.education.fintech.di

import android.content.Context
import com.tinkoff.education.networking.datasource.api.di.NetworkModule
import dagger.BindsInstance
import dagger.Component

@Component(modules = [NetworkModule::class, TestRepositoryModule::class])
interface TestAppComponent : AppComponent {

    @Component.Factory
    interface AppComponentFactory {
        fun create(@BindsInstance context: Context, networkModule: NetworkModule): AppComponent
    }
}