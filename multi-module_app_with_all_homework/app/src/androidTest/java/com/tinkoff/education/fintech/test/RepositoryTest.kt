package com.tinkoff.education.fintech.test

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.transition.R
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.di.DaggerTestAppComponent
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.message.presentation.MessageFragment
import com.tinkoff.education.fintech.features.util.URL_LOCAL_HOST
import com.tinkoff.education.fintech.screen.MessageFragmentScreen
import com.tinkoff.education.networking.datasource.api.di.NetworkModule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RepositoryTest : TestCase() {

    @get:Rule
    val wiremockRule = WireMockRule()

    lateinit var app: App

    @Before
    fun setup() {
        app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as App
        app.appComponent = DaggerTestAppComponent.factory().create(app, NetworkModule(URL_LOCAL_HOST))
    }

    @Test
    fun messageDisplayedTest() = run {
        val fragmentArgs = Bundle().apply {
            putParcelable("topicModel", TopicModelUi(1, "General", 10, "Testing"))
        }
        launchFragmentInContainer<MessageFragment>(fragmentArgs, themeResId = R.style.Theme_AppCompat)
        MessageFragmentScreen {
            recycler.hasSize(1)
            recycler.childAt<MessageFragmentScreen.KCardItem>(position = 0) {
                step("Проверяем отображение карточки") {
                    message.isDisplayed()
                }
            }
        }
    }
}