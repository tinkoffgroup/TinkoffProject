package com.tinkoff.education.fintech.screen

import com.kaspersky.kaspresso.screens.KScreen
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.anotherprofile.AnotherProfileFragment
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.text.KTextView

object AnotherProfileFragmentScreen : KScreen<AnotherProfileFragmentScreen>() {

    override val layoutId: Int
        get() = R.layout.fragment_another_profile
    override val viewClass: Class<*>
        get() = AnotherProfileFragment::class.java

    val avatar = KImageView { withId(R.id.avatar) }
    val userName = KTextView { withId(R.id.user_name) }
    val status = KTextView { withId(R.id.status) }
}