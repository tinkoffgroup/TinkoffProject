package com.tinkoff.education.fintech.utils

import androidx.paging.PagingData
import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import kotlinx.coroutines.flow.Flow
import java.io.File

class FakeMessageRepositoryImpl : MessageRepository {

    override fun getMessages(
        stream: String,
        topic: String
    ): Flow<PagingData<MessageWithReactions>> {
        return fakeFlow
    }

    override suspend fun uploadFile(file: File): String {
        TODO("Not yet implemented")
    }

    override suspend fun sendMessage(stream: String, topic: String, message: String): Any {
        TODO("Not yet implemented")
    }

    override suspend fun getUserMe(): UserDomain {
        TODO("Not yet implemented")
    }

    override suspend fun addMessageReaction(messageId: Int, reactionName: String): Any {
        TODO("Not yet implemented")
    }

    override suspend fun removeMessageReaction(messageId: Int, reactionName: String): Any {
        TODO("Not yet implemented")
    }
}