package com.tinkoff.education.fintech.screen

import com.kaspersky.kaspresso.screens.KScreen
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.mainscreen.MainScreenFragment
import io.github.kakaocup.kakao.bottomnav.KBottomNavigationView
import io.github.kakaocup.kakao.text.KButton

object MainFragmentScreen : KScreen<MainFragmentScreen>() {

    override val layoutId: Int
        get() = R.layout.fragment_main_screen
    override val viewClass: Class<*>
        get() = MainScreenFragment::class.java

    val bottomNavigation = KBottomNavigationView { withId(R.id.bottom_navigation_view) }

    val channelsButton = KButton { withId(R.id.channels_nav_graph) }
    val peopleButton = KButton { withId(R.id.people_nav_graph) }
    val profileButton = KButton { withId(R.id.profileFragment) }
}