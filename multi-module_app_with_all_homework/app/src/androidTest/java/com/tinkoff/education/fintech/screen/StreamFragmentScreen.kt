package com.tinkoff.education.fintech.screen

import android.view.View
import com.kaspersky.kaspresso.screens.KScreen
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.channels.stream.presentation.StreamFragment
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matcher

object StreamFragmentScreen : KScreen<StreamFragmentScreen>() {

    override val layoutId: Int
        get() = R.layout.fragment_stream
    override val viewClass: Class<*>
        get() = StreamFragment::class.java

    val recycler = KRecyclerView(
        builder = { withId(R.id.recycler_view) },
        itemTypeBuilder = {
            itemType(::KCardItem)
        }
    )

    class KCardItem(parent: Matcher<View>) : KRecyclerItem<KCardItem>(parent) {
        val title = KTextView { withId(R.id.title) }
        val button = KButton { withId(R.id.button) }
    }
}