package com.tinkoff.education.fintech.utils

import com.tinkoff.education.database.model.StreamEntity
import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.model.StreamDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain
import com.tinkoff.education.fintech.features.channels.stream.domain.model.UnreadMessagesByTopicDomain

class FakeStreamAndTopicRepositoryImpl : StreamAndTopicRepository {

    override suspend fun deleteStreams(amISubscribed: Boolean) {}

    override suspend fun getStreamsFromDataBase(amISubscribed: Boolean): List<StreamDomain> {
        TODO("Not yet implemented")
    }

    override suspend fun saveStreamsToDataBase(
        streams: List<StreamEntity>,
        amISubscribed: Boolean
    ) {
        TODO("Not yet implemented")
    }

    override suspend fun getAllStreams(): List<StreamDomain> {
        TODO("Not yet implemented")
    }

    override suspend fun getSubscribedStreams(): List<StreamDomain> {
        TODO("Not yet implemented")
    }

    override suspend fun getTopicsByStreamId(id: Int): List<TopicDomain> {
        TODO("Not yet implemented")
    }

    override suspend fun getUnreadMessagesByTopicStreams(eventTypes: String): List<UnreadMessagesByTopicDomain> {
        TODO("Not yet implemented")
    }
}