package com.tinkoff.education.fintech.di

import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.StreamAndTopicRepository
import com.tinkoff.education.fintech.features.message.domain.MessageRepository
import com.tinkoff.education.fintech.features.people.domain.UserRepository
import com.tinkoff.education.fintech.features.profile.domain.ProfileRepository
import com.tinkoff.education.fintech.utils.FakeBottomSheetRepositoryImpl
import com.tinkoff.education.fintech.utils.FakeMessageRepositoryImpl
import com.tinkoff.education.fintech.utils.FakeProfileRepositoryImpl
import com.tinkoff.education.fintech.utils.FakeStreamAndTopicRepositoryImpl
import com.tinkoff.education.fintech.utils.FakeUserRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class TestRepositoryModule {

    @Provides
    fun provideMessageRepository(): MessageRepository = FakeMessageRepositoryImpl()

    @Provides
    fun provideStreamAndTopicRepository(): StreamAndTopicRepository = FakeStreamAndTopicRepositoryImpl()

    @Provides
    fun provideUserRepository(): UserRepository = FakeUserRepositoryImpl()

    @Provides
    fun provideProfileRepository(): ProfileRepository = FakeProfileRepositoryImpl()

    @Provides
    fun provideBottomSheetRepository(): BottomSheetRepository = FakeBottomSheetRepositoryImpl()
}