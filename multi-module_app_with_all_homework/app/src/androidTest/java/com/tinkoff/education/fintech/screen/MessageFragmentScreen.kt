package com.tinkoff.education.fintech.screen

import android.view.View
import com.kaspersky.kaspresso.screens.KScreen
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.message.presentation.MessageFragment
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matcher

object MessageFragmentScreen : KScreen<MessageFragmentScreen>() {

    override val layoutId: Int
        get() = R.layout.fragment_message
    override val viewClass: Class<*>
        get() = MessageFragment::class.java

    val recycler = KRecyclerView(
        builder = { withId(R.id.recyclerView) },
        itemTypeBuilder = {
            itemType(::KCardItem)
        }
    )

    val inputField = KEditText { withId(R.id.inputField) }
    val sendButtton = KImageView { withId(R.id.sendButtton) }
    val topic = KTextView { withId(R.id.topic) }
    val stream = KTextView { withId(R.id.stream) }

    class KCardItem(parent: Matcher<View>) : KRecyclerItem<KCardItem>(parent) {
        val message = KView { withId(R.id.message) }
    }
}