package com.tinkoff.education.fintech.utils

import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.people.domain.UserRepository
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain

class FakeUserRepositoryImpl : UserRepository {
    override suspend fun saveUsersToDataBase(users: List<UserTable>): List<Long> {
        TODO("Not yet implemented")
    }

    override suspend fun getUsersFromDataBase(): List<UserDomain> {
        TODO("Not yet implemented")
    }

    override suspend fun getListOfUsers(): List<UserDomain> {
        TODO("Not yet implemented")
    }

    override suspend fun getUsersPresence(): List<UserPresenceDomain> {
        TODO("Not yet implemented")
    }
}