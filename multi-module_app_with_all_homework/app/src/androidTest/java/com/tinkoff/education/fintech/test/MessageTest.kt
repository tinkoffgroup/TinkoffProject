package com.tinkoff.education.fintech.test

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.transition.R
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.di.DaggerAppComponent
import com.tinkoff.education.fintech.features.channels.stream.presentation.delegate.topic.TopicModelUi
import com.tinkoff.education.fintech.features.message.presentation.MessageFragment
import com.tinkoff.education.fintech.features.util.URL_LOCAL_HOST
import com.tinkoff.education.fintech.mock.MockMessengerApi
import com.tinkoff.education.fintech.mock.MockMessengerApi.Companion.messenger
import com.tinkoff.education.fintech.screen.MessageFragmentScreen
import com.tinkoff.education.networking.datasource.api.di.NetworkModule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MessageTest : TestCase() {

    @get:Rule
    val wiremockRule = WireMockRule()

    @Before
    fun setup() {
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as App
        app.appComponent = DaggerAppComponent.factory()
            .create(app, NetworkModule(URL_LOCAL_HOST))
    }

    @Test
    fun sendMessageTest() = run {
        wiremockRule.messenger {
            messages()
            usersMe()
            sendMessage()
        }
        val fragmentArgs = Bundle().apply {
            putParcelable("topicModel", TopicModelUi(1, "General", 10, "Testing"))
        }
        launchFragmentInContainer<MessageFragment>(fragmentArgs, themeResId = R.style.Theme_AppCompat)
        MessageFragmentScreen {
            step("Заполняем сообщение перед отправкой") {
                recycler.hasSize(0)
                inputField.typeText("Test test ..")
                step("Отправляем сообщение") {
                    sendButtton.click()
                }
            }
            step("Проверяем, что вызвался метод sendMessage") {
                WireMock.verify(WireMock.getRequestedFor(MockMessengerApi.sendMessageUrl))
            }
        }
    }
}