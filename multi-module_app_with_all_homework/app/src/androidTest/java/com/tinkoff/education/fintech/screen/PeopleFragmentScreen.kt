package com.tinkoff.education.fintech.screen

import android.view.View
import com.kaspersky.kaspresso.screens.KScreen
import com.tinkoff.education.fintech.R
import com.tinkoff.education.fintech.features.people.presentation.PeopleFragment
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.searchview.KSearchView
import io.github.kakaocup.kakao.text.KTextView
import io.github.kakaocup.kakao.toolbar.KToolbar
import org.hamcrest.Matcher

object PeopleFragmentScreen : KScreen<PeopleFragmentScreen>() {

    override val layoutId: Int
        get() = R.layout.fragment_people
    override val viewClass: Class<*>
        get() = PeopleFragment::class.java

    val toolbar = KToolbar { withId(R.id.toolbar) }

    val searchView = KSearchView { withId(R.id.action_search) }

    val recycler = KRecyclerView(
        builder = { withId(R.id.recycler_view) },
        itemTypeBuilder = {
            itemType(::KCardItem)
        }
    )

    class KCardItem(parent: Matcher<View>) : KRecyclerItem<KCardItem>(parent) {
        val userAvatar = KImageView { withId(R.id.userAvatar) }
        val userStatus = KImageView { withId(R.id.dot) }
        val userEmail = KTextView { withId(R.id.userEmail) }
        val userName = KTextView { withId(R.id.userName) }
    }
}