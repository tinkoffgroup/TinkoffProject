package com.tinkoff.education.fintech.utils

import com.tinkoff.education.fintech.features.bottomsheet.domain.BottomSheetRepository
import com.tinkoff.education.fintech.features.channels.stream.domain.model.TopicDomain

class FakeBottomSheetRepositoryImpl : BottomSheetRepository {

    override suspend fun editMessage(editedMessage: String, messageId: Int): Any {
        TODO("Not yet implemented")
    }

    override suspend fun changeTopicMessage(newTopic: String, messageId: Int): Any {
        TODO("Not yet implemented")
    }

    override suspend fun deleteMessage(messageId: Int): Any {
        TODO("Not yet implemented")
    }

    override suspend fun getTopicsByStream(streamName: String): List<TopicDomain> {
        TODO("Not yet implemented")
    }
}