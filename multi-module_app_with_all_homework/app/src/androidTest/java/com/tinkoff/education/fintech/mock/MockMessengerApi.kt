package com.tinkoff.education.fintech.mock

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.matching.UrlPathPattern
import com.tinkoff.education.fintech.utils.AssetsUtils

class MockMessengerApi(private val wireMockServer: WireMockServer) {

    private val usersMatcher = WireMock.get(usersUrl)
    private val presenceMatcher = WireMock.get(presenceUrl)
    private val usersMeSubscriptionsMatcher = WireMock.get(usersMeSubscriptionsUrl)
    private val registerMatcher = WireMock.post(registerUrl)
    private val generalTopicsMatcher = WireMock.get(generalTopicsUrl)
    private val messagesMatcher = WireMock.get(messagesUrl)
    private val usersMeMatcher = WireMock.get(usersMeUrl)
    private val sendMessageMatcher = WireMock.post(sendMessageUrl)

    fun users() {
        wireMockServer.stubFor(usersMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/users.json"))))
    }

    fun presence() {
        wireMockServer.stubFor(presenceMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/presence.json"))))
    }

    fun usersMeSubscriptions() {
        wireMockServer.stubFor(usersMeSubscriptionsMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/usersMeSubscriptions.json"))))
    }

    fun register() {
        wireMockServer.stubFor(registerMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/register.json"))))
    }

    fun generalTopics() {
        wireMockServer.stubFor(generalTopicsMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/generalTopics.json"))))
    }

    fun messages() {
        wireMockServer.stubFor(messagesMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/messages.json"))))
    }

    fun usersMe() {
        wireMockServer.stubFor(usersMeMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/usersMe.json"))))
    }

    fun sendMessage() {
        wireMockServer.stubFor(sendMessageMatcher.willReturn(WireMock.ok(AssetsUtils.fromAssets("people/sendMessage.json"))))
    }

    companion object {

        val usersUrl: UrlPathPattern? = WireMock.urlPathMatching("/users")
        val presenceUrl: UrlPathPattern? = WireMock.urlPathMatching("/realm/presence")
        val usersMeSubscriptionsUrl: UrlPathPattern? = WireMock.urlPathMatching("/users/me/subscriptions")
        val registerUrl: UrlPathPattern? = WireMock.urlPathMatching("/register?.+")
        val generalTopicsUrl: UrlPathPattern? = WireMock.urlPathMatching("/users/me/432915/topics")
        val messagesUrl: UrlPathPattern? = WireMock.urlPathMatching("/messages?.*")
        val usersMeUrl: UrlPathPattern? = WireMock.urlPathMatching("/users/me")
        val sendMessageUrl: UrlPathPattern? = WireMock.urlPathMatching("/messages")

        fun WireMockServer.messenger(block: MockMessengerApi.() -> Unit) {
            MockMessengerApi(this).apply(block)
        }
    }
}