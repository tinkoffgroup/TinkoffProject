package com.tinkoff.education.fintech.utils

import androidx.paging.PagingData
import com.tinkoff.education.database.model.MessageTable
import com.tinkoff.education.database.model.MessageWithReactions
import com.tinkoff.education.database.model.ReactionTable
import kotlinx.coroutines.flow.flowOf

val fakeMessageTable = MessageTable(messageId = 1, message = "Fake message", avatarUrl = "Fake avatarUrl",
    timestamp = 1L, subject = "subject", isMeMessage = false, senderId = 1, myId = 1, senderFullName = "Fake Name", hashCodeOfStreamsAndTopics = 123456)

val fakeReactionTable = ReactionTable(emojiName = "sleepy", emojiCode = "1f62a", userId = 1, messageId = 1, hashCodeOfStreamsAndTopics = 123456)

val fakeMessageWithReactions = MessageWithReactions(fakeMessageTable, listOf(fakeReactionTable))

val fakeFlow = flowOf(PagingData.from(listOf(fakeMessageWithReactions)))