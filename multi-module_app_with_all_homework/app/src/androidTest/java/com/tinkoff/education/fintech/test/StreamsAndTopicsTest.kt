package com.tinkoff.education.fintech.test

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.transition.R
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.di.DaggerAppComponent
import com.tinkoff.education.fintech.features.mainscreen.MainScreenFragment
import com.tinkoff.education.fintech.features.util.URL_LOCAL_HOST
import com.tinkoff.education.fintech.mock.MockMessengerApi.Companion.messenger
import com.tinkoff.education.fintech.screen.MainFragmentScreen
import com.tinkoff.education.fintech.screen.MessageFragmentScreen
import com.tinkoff.education.fintech.screen.StreamFragmentScreen
import com.tinkoff.education.networking.datasource.api.di.NetworkModule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class StreamsAndTopicsTest : TestCase() {

    @get:Rule
    val wiremockRule = WireMockRule()

    @Before
    fun setup() {
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as App
        app.appComponent = DaggerAppComponent.factory()
            .create(app, NetworkModule(URL_LOCAL_HOST))
    }

    @Test
    fun openAndCloseStreamTest() = run {
        wiremockRule.messenger {
            usersMeSubscriptions()
            register()
            generalTopics()
        }
        launchFragmentInContainer<MainScreenFragment>(themeResId = R.style.Theme_AppCompat)
        MainFragmentScreen {
            step("Проверяем bottomNavigation") {
                bottomNavigation.isDisplayed()
                peopleButton.isDisplayed()
                profileButton.isDisplayed()
            }
        }
        StreamFragmentScreen {
            flakySafely {
                recycler.hasSize(1)
                recycler.childAt<StreamFragmentScreen.KCardItem>(position = 0) {
                    step("Проверяем отображение карточки") {
                        title.hasText("general")
                        button.isVisible()
                    }
                    step("Нажимаем на карточку") {
                        click()
                    }
                }
            }
            flakySafely {
                step("Проверяем что число айтемов изменилось и топики добавились") {
                    recycler.hasSize(14)
                }
                recycler.childAt<StreamFragmentScreen.KCardItem>(position = 0) {
                    step("Нажимаем на первый пйтем для закрытия стримов") {
                        click()
                    }
                }
            }
            flakySafely {
                step("Проверяем что число айтемов изменилось и топики убрались") {
                    recycler.hasSize(1)
                }
            }
        }
    }

    @Test
    fun openTopicAndCheckTitleTest() = run {
        wiremockRule.messenger {
            usersMeSubscriptions()
            register()
            generalTopics()
            messages()
            usersMe()
        }
        launchFragmentInContainer<MainScreenFragment>(themeResId = R.style.Theme_AppCompat)
        MainFragmentScreen {
            step("Проверяем bottomNavigation") {
                bottomNavigation.isDisplayed()
                peopleButton.isDisplayed()
                profileButton.isDisplayed()
            }
        }
        StreamFragmentScreen {
            flakySafely {
                recycler.hasSize(1)
                recycler.childAt<StreamFragmentScreen.KCardItem>(position = 0) {
                    step("Нажимаем на карточку") {
                        click()
                    }
                }
            }
            flakySafely {
                step("Проверяем что число айтемов изменилось и топики добавились") {
                    recycler.hasSize(14)
                }
                recycler.childAt<StreamFragmentScreen.KCardItem>(position = 1) {
                    step("Нажимаем на первый топик для проваливания в него") {
                        click()
                    }
                }
            }
            MessageFragmentScreen {
                step("Проверяет название стрима и топика") {
                    stream.hasText("general")
                    topic.hasText("Topic: testing")
                }
            }
        }
    }
}