package com.tinkoff.education.fintech.utils

import com.tinkoff.education.database.model.UserTable
import com.tinkoff.education.fintech.features.people.domain.model.UserDomain
import com.tinkoff.education.fintech.features.people.domain.model.UserPresenceDomain
import com.tinkoff.education.fintech.features.profile.domain.ProfileRepository

class FakeProfileRepositoryImpl : ProfileRepository {
    override suspend fun getUserMe(): UserDomain {
        TODO("Not yet implemented")
    }

    override suspend fun getUserPresence(id: Int): UserPresenceDomain {
        TODO("Not yet implemented")
    }

    override suspend fun getUserByIdDataBase(id: Int): UserDomain? {
        TODO("Not yet implemented")
    }

    override suspend fun saveUser(user: UserTable): Long {
        TODO("Not yet implemented")
    }
}