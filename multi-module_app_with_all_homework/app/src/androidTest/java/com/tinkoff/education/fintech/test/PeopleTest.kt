package com.tinkoff.education.fintech.test

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.transition.R
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.tinkoff.education.fintech.App
import com.tinkoff.education.fintech.di.DaggerAppComponent
import com.tinkoff.education.fintech.features.mainscreen.MainScreenFragment
import com.tinkoff.education.fintech.features.util.URL_LOCAL_HOST
import com.tinkoff.education.fintech.mock.MockMessengerApi.Companion.messenger
import com.tinkoff.education.fintech.screen.AnotherProfileFragmentScreen
import com.tinkoff.education.fintech.screen.MainFragmentScreen
import com.tinkoff.education.fintech.screen.PeopleFragmentScreen
import com.tinkoff.education.networking.datasource.api.di.NetworkModule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PeopleTest : TestCase() {

    @get:Rule
    val wiremockRule = WireMockRule()

    @Before
    fun setup() {
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as App
        app.appComponent = DaggerAppComponent.factory()
            .create(app, NetworkModule(URL_LOCAL_HOST))
    }

    @Test
    fun openingFirstUserTest() = run {
        wiremockRule.messenger {
            generalTopics()
            usersMeSubscriptions()
            users()
            presence()
            register()
        }
        launchFragmentInContainer<MainScreenFragment>(themeResId = R.style.Theme_AppCompat)
        MainFragmentScreen {
            step("Переходим в PeopleFragmentScreen") {
                bottomNavigation.isDisplayed()
                peopleButton.isDisplayed()
                profileButton.isDisplayed()
                peopleButton.click()
            }
        }
        PeopleFragmentScreen {
            flakySafely {
                recycler.hasSize(1)
                recycler.childAt<PeopleFragmentScreen.KCardItem>(position = 0) {
                    step("Проверяем отображение карточки") {
                        userAvatar.isVisible()
                        userStatus.isVisible()
                        userEmail.hasText("test@tinkoff-android.com")
                        userName.hasText("Test Name")
                    }
                    step("Нажимаем на первую карточку") {
                        click()
                    }
                }
            }
        }
        AnotherProfileFragmentScreen {
            step("Переходим в PeopleFragmentScreen") {
                avatar.isDisplayed()
                userName.hasText("Test Name")
                status.hasText("unknown")
            }
        }
    }
}