package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AllStreamsApi(
    @Json(name = "streams") val streams: List<StreamApi>
)

@JsonClass(generateAdapter = true)
data class SubscribedStreamsApi(
    @Json(name = "subscriptions") val streams: List<StreamApi>
)

@JsonClass(generateAdapter = true)
data class StreamApi(
    @Json(name = "stream_id") val id: Int,
    @Json(name = "name") val name: String
)
