package com.tinkoff.education.networking.datasource.api

import com.tinkoff.education.networking.datasource.api.model.AllStreamsApi
import com.tinkoff.education.networking.datasource.api.model.MessagesApi
import com.tinkoff.education.networking.datasource.api.model.ResponseApi
import com.tinkoff.education.networking.datasource.api.model.SubscribedStreamsApi
import com.tinkoff.education.networking.datasource.api.model.TopicsApi
import com.tinkoff.education.networking.datasource.api.model.UnreadMessagesApi
import com.tinkoff.education.networking.datasource.api.model.UserApi
import com.tinkoff.education.networking.datasource.api.model.UserPresenceApi
import com.tinkoff.education.networking.datasource.api.model.UsersApi
import com.tinkoff.education.networking.datasource.api.model.UsersPresenceApi
import com.tinkoff.education.networking.datasource.api.retrofit.ChatApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class ChatNetworkDataSource @Inject constructor(
    private val chatApiService: ChatApiService
)  {

    suspend fun editMessage(editedMessage: String, messageId: Int): Any {
        return chatApiService.editMessage(messageId, editedMessage)
    }

    suspend fun changeTopicMessage(newTopic: String, messageId: Int): Any {
        return chatApiService.changeTopicMessage(messageId, newTopic)
    }

    suspend fun deleteMessage(messageId: Int): Any {
        return chatApiService.deleteMessage(messageId)
    }

    suspend fun getUserMe(): UserApi {
        return chatApiService.getUserMe()
    }

    suspend fun getMessages(anchor: String, narrow: String): MessagesApi {
        return chatApiService.getMessages(anchor = anchor, narrow = narrow)
    }

    suspend fun uploadFile(file: File): String {
        val multipartBody: MultipartBody.Part = MultipartBody.Part
            .createFormData(
                "file",
                file.name,
                file.asRequestBody()
            )
        return chatApiService.uploadFile(multipartBody).uri
    }

    suspend fun sendMessage(stream: String, topic: String, message: String): ResponseApi {
        return chatApiService.sendMessage(stream, topic, message)
    }

    suspend fun addMessageReaction(messageId: Int, reactionName: String): Any {
        return chatApiService.addMessageReaction(messageId, reactionName)
    }

    suspend fun removeMessageReaction(messageId: Int, reactionName: String): Any {
        return chatApiService.removeMessageReaction(messageId, reactionName)
    }

    suspend fun getUserPresence(id: Int): UserPresenceApi {
        return chatApiService.getUserPresence(id)
    }

    suspend fun getListOfUsers(): UsersApi {
        return chatApiService.getListOfUsers()
    }

    suspend fun getUsersPresence(): UsersPresenceApi {
        return chatApiService.getUsersPresence()
    }

    suspend fun getAllStreams(): AllStreamsApi {
        return chatApiService.getAllStreams()
    }

    suspend fun getUnreadMessagesByTopicStreams(eventTypes: String): UnreadMessagesApi {
        return chatApiService.getUnreadMessages(eventTypes)
    }

    suspend fun getSubscribedStreams(): SubscribedStreamsApi {
        return chatApiService.getSubscribedStreams()
    }

    suspend fun getTopicsByStreamId(id: Int): TopicsApi {
        return chatApiService.getTopicsByStreamId(id)
    }
}