package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TopicsApi(
    @Json(name = "topics") val topics: List<TopicApi>
)

@JsonClass(generateAdapter = true)
data class TopicApi(
    @Json(name = "name") val name: String,
    @Json(name = "max_id") val maxId: Int
)