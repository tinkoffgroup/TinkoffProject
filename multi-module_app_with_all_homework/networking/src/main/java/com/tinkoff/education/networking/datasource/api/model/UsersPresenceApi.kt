package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UsersPresenceApi(
    @Json(name = "presences") val presences: Map<String, PresenceApi>
)

@JsonClass(generateAdapter = true)
data class UserPresenceApi(
    @Json(name = "presence") val presence: PresenceApi
)

@JsonClass(generateAdapter = true)
data class PresenceApi(
    @Json(name = "aggregated") val aggregated: AggregatedApi
)

@JsonClass(generateAdapter = true)
data class AggregatedApi(
    @Json(name = "status") val status: String
)
