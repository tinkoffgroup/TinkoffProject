package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UsersApi(
    @Json(name = "members") val users: List<UserApi>
)

@JsonClass(generateAdapter = true)
data class UserApi(
    @Json(name = "user_id") val id: Int,
    @Json(name = "full_name") val userName: String,
    @Json(name = "email") val userEmail: String,
    @Json(name = "avatar_url") val userUrl: String?,
)