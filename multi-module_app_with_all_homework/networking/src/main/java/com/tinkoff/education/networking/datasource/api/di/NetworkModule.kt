package com.tinkoff.education.networking.datasource.api.di

import com.tinkoff.education.networking.datasource.api.retrofit.ChatApiService
import dagger.Module
import dagger.Provides
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule(baseUrl: String) {

    private val authClient: OkHttpClient = OkHttpClient
        .Builder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .addInterceptor(AuthHeaderInterceptor())
        .addInterceptor(
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }
        )
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create())
        .client(authClient)
        .build()

    @Provides
    @Singleton
    fun provideChatApiService(): ChatApiService =
        retrofit.create(ChatApiService::class.java)
}

private const val EMAIL = "kreyeraliaksandr@gmail.com"
private const val API_KEY = "PXFnKqx8ul4yQjEzR2xEtrF0a7rQuoZW"
class AuthHeaderInterceptor : Interceptor {

    private val credential: String = Credentials.basic(EMAIL, API_KEY)

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestWithHeader =
            chain.request().newBuilder().addHeader("Authorization", credential).build()
        return chain.proceed(requestWithHeader)
    }
}