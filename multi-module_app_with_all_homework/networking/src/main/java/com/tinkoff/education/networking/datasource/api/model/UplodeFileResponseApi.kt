package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UploadFileResponseApi(
    @Json(name = "result") val result: String,
    @Json(name = "uri") val uri: String
)
