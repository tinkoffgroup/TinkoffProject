package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MessagesApi(
    @Json(name = "messages") val messages: List<MessageApi>
)

@JsonClass(generateAdapter = true)
data class MessageApi(
    @Json(name = "id") val id: Int,
    @Json(name = "content") val content: String,
    @Json(name = "avatar_url") val avatarUrl: String,
    @Json(name = "timestamp") val timestamp: Long,
    @Json(name = "subject") val subject: String,
    @Json(name = "is_me_message") val isMeMessage: Boolean,
    @Json(name = "sender_id") val senderId: Int,
    @Json(name = "sender_full_name") val senderFullName: String,
    @Json(name = "reactions") val reactions: List<ReactionApi>
)

@JsonClass(generateAdapter = true)
data class ReactionApi(
    @Json(name = "emoji_name") val emojiName: String,
    @Json(name = "emoji_code") val emojiCode: String,
    @Json(name = "user_id") val userId: Int
)