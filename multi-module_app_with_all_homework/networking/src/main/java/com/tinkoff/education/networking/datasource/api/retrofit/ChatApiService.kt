package com.tinkoff.education.networking.datasource.api.retrofit

import com.tinkoff.education.networking.datasource.api.model.AllStreamsApi
import com.tinkoff.education.networking.datasource.api.model.MessagesApi
import com.tinkoff.education.networking.datasource.api.model.ResponseApi
import com.tinkoff.education.networking.datasource.api.model.SubscribedStreamsApi
import com.tinkoff.education.networking.datasource.api.model.TopicsApi
import com.tinkoff.education.networking.datasource.api.model.UnreadMessagesApi
import com.tinkoff.education.networking.datasource.api.model.UploadFileResponseApi
import com.tinkoff.education.networking.datasource.api.model.UserApi
import com.tinkoff.education.networking.datasource.api.model.UserPresenceApi
import com.tinkoff.education.networking.datasource.api.model.UsersApi
import com.tinkoff.education.networking.datasource.api.model.UsersPresenceApi
import okhttp3.MultipartBody
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ChatApiService {

    @PATCH("messages/{message_id}")
    suspend fun editMessage(
        @Path("message_id") messageId: Int,
        @Query("content") content: String
    ): Any

    @PATCH("messages/{message_id}")
    suspend fun changeTopicMessage(
        @Path("message_id") messageId: Int,
        @Query("topic") topic: String
    ): Any

    @DELETE("messages/{message_id}")
    suspend fun deleteMessage(
        @Path("message_id") messageId: Int
    ): ResponseApi

    @GET("messages")
    suspend fun getMessages(
        @Query("anchor") anchor: String = "newest",
        @Query("num_before") numBefore: Int = 20,
        @Query("num_after") numAfter: Int = 0,
        @Query("narrow") narrow: String,
        @Query("include_anchor") includeAnchor: Boolean = false
    ): MessagesApi

    @Multipart
    @POST("user_uploads")
    suspend fun uploadFile(
        @Part file: MultipartBody.Part
    ): UploadFileResponseApi

    @POST("messages")
    suspend fun sendMessage(
        @Query("to") stream: String,
        @Query("topic") topic: String,
        @Query("content") content: String,
        @Query("type") type: String = "stream"
    ): ResponseApi

    @POST("register")
    suspend fun getUnreadMessages(
        @Query("event_types") eventTypes: String
    ): UnreadMessagesApi

    @POST("messages/{message_id}/reactions")
    suspend fun addMessageReaction(
        @Path("message_id") messageId: Int,
        @Query("emoji_name") reactionName: String
    ): ResponseApi

    @DELETE("messages/{message_id}/reactions")
    suspend fun removeMessageReaction(
        @Path("message_id") messageId: Int, @Query("emoji_name") emojiName: String
    ): ResponseApi

    @GET("users")
    suspend fun getListOfUsers(
    ): UsersApi

    @GET("users/{user_id}/presence")
    suspend fun getUserPresence(
        @Path("user_id") id: Int
    ): UserPresenceApi

    @GET("realm/presence")
    suspend fun getUsersPresence(
    ): UsersPresenceApi

    @GET("users/me")
    suspend fun getUserMe(
    ): UserApi

    @GET("streams")
    suspend fun getAllStreams(
    ): AllStreamsApi

    @GET("users/me/subscriptions")
    suspend fun getSubscribedStreams(
    ): SubscribedStreamsApi

    @GET("users/me/{stream_id}/topics")
    suspend fun getTopicsByStreamId(
        @Path("stream_id") id: Int
    ): TopicsApi
}