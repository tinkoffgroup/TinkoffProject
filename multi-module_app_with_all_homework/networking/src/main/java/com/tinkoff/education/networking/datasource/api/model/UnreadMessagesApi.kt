package com.tinkoff.education.networking.datasource.api.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UnreadMessagesApi(
    @Json(name = "unread_msgs") val unreadMsgs: UnreadMsgs
)

@JsonClass(generateAdapter = true)
data class UnreadMsgs(
    @Json(name = "streams") val streams: List<Stream>
)

@JsonClass(generateAdapter = true)
data class Stream(
    @Json(name = "stream_id") val streamId: Int,
    @Json(name = "topic") val topic: String,
    @Json(name = "unread_message_ids") val unreadMessageIds: List<Int> = emptyList()
)