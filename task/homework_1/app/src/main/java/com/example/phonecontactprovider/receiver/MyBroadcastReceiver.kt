package com.example.phonecontactprovider.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.phonecontactprovider.SecondActivity
import com.example.phonecontactprovider.model.Contact

class MyBroadcastReceiver(private val sendListOfContacts: (list: ArrayList<Contact>) -> Unit) : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val listOfContacts: ArrayList<Contact>? = intent?.getParcelableArrayListExtra(SecondActivity.EXTRA_LIST_OF_CONTACTS)
        listOfContacts?.let { sendListOfContacts.invoke(it) }
    }
}