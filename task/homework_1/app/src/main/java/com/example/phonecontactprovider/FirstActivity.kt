package com.example.phonecontactprovider

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.example.phonecontactprovider.databinding.ActivityFirstBinding
import com.example.phonecontactprovider.model.Contact

class FirstActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFirstBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirstBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        observerButton()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.run {
            putString(STATE_LIST_OF_PHONE_CONTACTS, binding.listOfPhoneContacts.text.toString())
            putBoolean(STATE_BUTTON_VISIBILITY, binding.getListOfPhoneContactsButton.isVisible)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.run {
            binding.listOfPhoneContacts.text = getString(STATE_LIST_OF_PHONE_CONTACTS)
            binding.getListOfPhoneContactsButton.isVisible = getBoolean(STATE_BUTTON_VISIBILITY)
        }
    }

    private val secondActivityResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val listItemCount: ArrayList<Contact>? = result.data?.getParcelableArrayListExtra(SecondActivity.EXTRA_LIST_OF_PHONE_CONTACTS)
            binding.listOfPhoneContacts.text = listItemCount.toString()
            binding.getListOfPhoneContactsButton.isVisible = false
        } else {
            Toast.makeText(this, R.string.sad, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startSecondActivity()
                } else {
                    Toast.makeText(this, R.string.sad, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun observerButton() {
        binding.getListOfPhoneContactsButton.setOnClickListener {
            checkPermission()
        }
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSIONS_REQUEST_CODE
            )
        } else {
            startSecondActivity()
        }
    }

    private fun startSecondActivity() {
        secondActivityResult.launch(Intent(this, SecondActivity::class.java))
    }

    companion object {
        const val STATE_LIST_OF_PHONE_CONTACTS = "state_list_of_phone_contacts"
        const val STATE_BUTTON_VISIBILITY = "state_button_visibility"
        const val PERMISSIONS_REQUEST_CODE = 1
    }
}