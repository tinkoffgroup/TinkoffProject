package com.example.phonecontactprovider

import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.phonecontactprovider.model.Contact
import com.example.phonecontactprovider.receiver.MyBroadcastReceiver
import com.example.phonecontactprovider.service.MyService

class SecondActivity : AppCompatActivity() {
    private val sendListOfContacts: (list: ArrayList<Contact>) -> Unit = { list ->
        val intent = Intent().apply {
            putParcelableArrayListExtra(EXTRA_LIST_OF_PHONE_CONTACTS, ArrayList(list))
        }
        setResult(RESULT_OK, intent)
        finish()
    }
    private val receiver = MyBroadcastReceiver(sendListOfContacts)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        registerReceiver()
        startMyService()
    }

    private fun registerReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, IntentFilter(ACTION_RECEIVE_CONTACTS))
    }

    private fun startMyService() {
        val intent = Intent(this, MyService::class.java)
        startService(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
    }

    companion object {
        const val ACTION_RECEIVE_CONTACTS = "action_receive_contacts"
        const val EXTRA_LIST_OF_CONTACTS = "extra_list_of_contacts"
        const val EXTRA_LIST_OF_PHONE_CONTACTS = "extra_list_of_phone_contacts"
    }
}