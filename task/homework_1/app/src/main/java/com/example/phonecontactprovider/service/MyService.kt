package com.example.phonecontactprovider.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.provider.ContactsContract
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.phonecontactprovider.SecondActivity
import com.example.phonecontactprovider.model.Contact

class MyService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val listOfContacts: List<Contact> = getListOfContacts()
        val sendContactIntent = Intent(SecondActivity.ACTION_RECEIVE_CONTACTS).apply {
            putParcelableArrayListExtra(SecondActivity.EXTRA_LIST_OF_CONTACTS, ArrayList(listOfContacts))
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(sendContactIntent)

        return START_NOT_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? = null

    private fun getListOfContacts(): List<Contact> {
        val listOfContacts = mutableListOf<Contact>()

        val projection = arrayOf(ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER)
        val contactCursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projection, null, null, null)

        while (contactCursor?.moveToNext() == true) {

            val idColumnIndex = contactCursor.getColumnIndex(ContactsContract.Contacts._ID)
            val nameColumnIndex = contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
            val hasPhoneColumnIndex = contactCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)

            val id = contactCursor.getString(idColumnIndex)
            val name = contactCursor.getString(nameColumnIndex)
            val hasPhone = contactCursor.getInt(hasPhoneColumnIndex)
            var phoneNumber = ""

            if (hasPhone > 0) {
                val phoneCursor = contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = $id",
                    null,
                    null
                )
                phoneCursor?.moveToFirst()
                phoneCursor?.let {
                    val phoneNumberColumnIndex = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    phoneNumber = phoneCursor.getString(phoneNumberColumnIndex)
                }
                phoneCursor?.close()
            }
            listOfContacts.add(Contact(id, name, hasPhone, phoneNumber))
        }
        contactCursor?.close()
        return listOfContacts
    }
}