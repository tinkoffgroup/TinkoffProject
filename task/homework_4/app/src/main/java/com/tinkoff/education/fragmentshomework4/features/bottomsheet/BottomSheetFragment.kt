package com.tinkoff.education.fragmentshomework4.features.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tinkoff.education.fragmentshomework4.databinding.FragmentBottomSheetBinding
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.emojiList
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.recyclerview.EmojiAdapter

class BottomSheetFragment(
    private val onEmojiClickListener: (
        emojiUi: EmojiUi
    ) -> Unit
)  : BottomSheetDialogFragment() {

    private var _binding: FragmentBottomSheetBinding? = null
    private val binding get() = _binding!!

    private lateinit var emojiAdapter: EmojiAdapter
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        fillRecyclerView()
    }

    private val dismiss: (
    ) -> Unit = {
        dismiss()
    }

    private fun setUpRecyclerView() {
        emojiAdapter = EmojiAdapter(onEmojiClickListener, dismiss)
        recyclerView = binding.recyclerView
        recyclerView.apply {
            adapter = emojiAdapter
            layoutManager = GridLayoutManager(requireContext(), 7)
        }
    }

    private fun fillRecyclerView() {
        emojiAdapter.submitList(emojiList)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}