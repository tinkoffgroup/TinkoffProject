package com.tinkoff.education.fragmentshomework4.features.message.model

import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.EmojiUi

data class ReactionUi(
    val emojiUi: EmojiUi,
    val userId: String
)
