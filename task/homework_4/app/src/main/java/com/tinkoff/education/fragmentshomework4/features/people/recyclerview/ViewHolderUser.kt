package com.tinkoff.education.fragmentshomework4.features.people.recyclerview

import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tinkoff.education.fragmentshomework4.R
import com.tinkoff.education.fragmentshomework4.databinding.UserItemBinding
import com.tinkoff.education.fragmentshomework4.features.people.model.UserUi
import com.tinkoff.education.fragmentshomework4.features.util.extensions.loadImage

class ViewHolderUser(private val binding: UserItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(userUi: UserUi) {
        binding.userName.text = userUi.userName
        binding.userEmail.text = userUi.userEmail
        binding.avatar.loadImage(userUi.userUrl)
    }
}