package com.tinkoff.education.fragmentshomework4.features.util.extensions

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.tinkoff.education.fragmentshomework4.R

fun AppCompatImageView.loadImage(imageUri: String) {

    Glide.with(context)
        .load(imageUri)
        .placeholder(R.drawable.ic_launcher_foreground)
        .error(R.drawable.ic_launcher_foreground)
        .into(this)
}