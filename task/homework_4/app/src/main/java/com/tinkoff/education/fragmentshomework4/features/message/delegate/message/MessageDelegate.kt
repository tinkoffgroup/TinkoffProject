package com.tinkoff.education.fragmentshomework4.features.message.delegate.message

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fragmentshomework4.R
import com.tinkoff.education.fragmentshomework4.customview.EmojiView
import com.tinkoff.education.fragmentshomework4.databinding.MessageItemBinding
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fragmentshomework4.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem

class MessageDelegate(
    private val onMessageListener: (
        messageModelUi: MessageModelUi
    ) -> Unit,
    private val onEmojiListener: (
        emojiUi: EmojiUi, message: MessageModelUi
    ) -> Unit
) : AdapterDelegate {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            MessageItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int
    ) {
        (holder as ViewHolder).bind(item.content() as MessageModelUi, onMessageListener, onEmojiListener)
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is MessageDelegateItem

    class ViewHolder(
        private val binding: MessageItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            model: MessageModelUi,
            onMessageListener: (
                messageModelUi: MessageModelUi
            ) -> Unit,
            onEmojiListener: (
                emojiUi: EmojiUi, message: MessageModelUi
            ) -> Unit
        ) {

            binding.message.setName(model.userName)
            binding.message.setMessage(model.userMessage)
            binding.message.isMyMessage(model.isMyMessage)
            binding.message.removeAllViewsFromEmojis()

            val emojiUsage = model.listOfReactions
                .groupBy { it.emojiUi }
                .mapValues { it.value.map { reaction -> reaction.userId }.distinct().count() }
            emojiUsage.forEach { (emojiUi, count) ->
                val emojiView = EmojiView(itemView.context, defTheme = R.style.EmojiStyle)
                emojiView.emoji = emojiUi.getCodeString()
                emojiView.emojiCounter = count

                val reaction = model.listOfReactions.find { it.userId == model.myId && it.emojiUi.emojiCode == emojiUi.emojiCode }
                if (reaction != null) {
                    emojiView.isSelected = true
                }

                emojiView.setOnClickListener {
                    onEmojiListener.invoke(emojiUi, model)
                }

                binding.message.addEmoji(emojiView)
            }

            if (model.listOfReactions.isNotEmpty()) {
                binding.message.addPlus()
                binding.message.getPlus().setOnClickListener {
                    onMessageListener(model)
                }
            }

            binding.message.setOnLongClickListener {
                onMessageListener(model)
                true
            }
        }
    }
}