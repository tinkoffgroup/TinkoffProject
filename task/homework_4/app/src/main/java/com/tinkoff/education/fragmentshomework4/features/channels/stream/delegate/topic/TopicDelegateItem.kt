package com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic

import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem

class TopicDelegateItem(
    val id: Int,
    private val topicModelUi: TopicModelUi
) : DelegateItem {
    override fun content(): Any = topicModelUi
    override fun id(): Int = topicModelUi.id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as TopicDelegateItem).topicModelUi == this.topicModelUi
    }
}