package com.tinkoff.education.fragmentshomework4.features.message.delegate.message

import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem

class MessageDelegateItem(
    val id: Int,
    private val messageModelUi: MessageModelUi
) : DelegateItem {
    override fun content(): Any = messageModelUi
    override fun id(): Int = messageModelUi.id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as MessageDelegateItem).messageModelUi == this.messageModelUi
    }
}