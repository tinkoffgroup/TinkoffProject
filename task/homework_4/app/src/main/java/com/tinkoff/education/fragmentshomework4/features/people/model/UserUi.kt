package com.tinkoff.education.fragmentshomework4.features.people.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserUi(
    val id: Int,
    val userName: String,
    val userEmail: String,
    val userUrl: String,
): Parcelable
