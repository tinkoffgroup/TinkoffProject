package com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TopicModelUi(
    val id: Int,
    val title: String,
    val mes: Int
): Parcelable
