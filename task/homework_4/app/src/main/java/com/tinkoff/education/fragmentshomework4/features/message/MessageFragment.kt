package com.tinkoff.education.fragmentshomework4.features.message

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.content.res.AppCompatResources
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fragmentshomework4.R
import com.tinkoff.education.fragmentshomework4.databinding.FragmentMessageBinding
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.BottomSheetFragment
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.EmojiUi
import com.tinkoff.education.fragmentshomework4.features.message.delegate.message.MessageDelegate
import com.tinkoff.education.fragmentshomework4.features.message.delegate.message.MessageDelegateItem
import com.tinkoff.education.fragmentshomework4.features.message.delegate.message.MessageModelUi
import com.tinkoff.education.fragmentshomework4.features.message.model.ReactionUi
import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem
import com.tinkoff.education.fragmentshomework4.features.util.delegate.MainAdapter

class MessageFragment : Fragment() {

    private var _binding: FragmentMessageBinding? = null
    private val binding get() = _binding!!

    private val args: MessageFragmentArgs by navArgs()

    private lateinit var mainAdapter: MainAdapter
    private lateinit var recyclerView: RecyclerView

    private val observerRecyclerView = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            recyclerView.layoutManager?.scrollToPosition(positionStart + itemCount - 1) // TODO срабатывае только первый раз
        }
        override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {}
        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {}
        override fun onChanged() {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMessageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillUserLayout()
        observerButton()
        setUpRecyclerView()
        fillRecyclerView()
        observeRecyclerView()
    }

    private fun observeRecyclerView() {
        mainAdapter.registerAdapterDataObserver(observerRecyclerView)
    }

    private fun fillRecyclerView() {
        mainAdapter.submitList(listOfMessagesStub)
    }

    private val onMessageListener: (
        messageModelUi: MessageModelUi
    ) -> Unit = { messageModelUi ->
        Log.d("onMessageListener", "messageModelUi: $messageModelUi")
        val onEmojiClickListener: (
            emojiUi: EmojiUi
        ) -> Unit = { emojiUi ->
            Log.d("BottomSheetFragment", "emoji: $emojiUi messageModelUi: $messageModelUi")
            addReactionToMessage(messageModelUi, emojiUi)
        }
        val bottom = BottomSheetFragment(onEmojiClickListener)
        bottom.show(parentFragmentManager, bottom.tag)
    }

    private val onEmojiListener: (
        emojiUi: EmojiUi, message: MessageModelUi
    ) -> Unit = { emojiUi, message ->
        Log.d("onEmojiListener", "emojiUi: $emojiUi message: $message")
        handleEmojiClick(emojiUi, message)
    }

    private fun handleEmojiClick(emojiUi: EmojiUi, message: MessageModelUi) {
        val memoryStubList: List<DelegateItem> = listOfMessagesStub
        val newStubList: MutableList<DelegateItem> = mutableListOf<DelegateItem>()

        val newStubOfReactions: MutableList<ReactionUi> = mutableListOf()
        memoryStubList.forEach { delegate ->
            val delegateContent = delegate.content()
            if (delegateContent is MessageModelUi) {
                if (delegate.id() == message.id) {
                    val reaction = message.listOfReactions.find { it.userId == message.myId && it.emojiUi.emojiCode == emojiUi.emojiCode }
                    if (reaction != null) { // emojiUi need remove
                        message.listOfReactions.forEach {        // ReactionUi (val emojiUi: EmojiUi, val userId: String)
                            if (it.userId == message.myId && it.emojiUi.emojiCode == emojiUi.emojiCode) {
                                return@forEach
                            }
                            newStubOfReactions.add(it)
                        }
                    } else { // emojiUi need add
                        message.listOfReactions.forEach {
                            newStubOfReactions.add(it)
                        }
                        newStubOfReactions.add(ReactionUi(emojiUi, userId = message.myId))
                    }
                    newStubList.add(MessageDelegateItem(id = message.id, message.copy(listOfReactions = newStubOfReactions.toList())))
                } else {
                    newStubList.add(delegate)
                }
            } else {
                // TODO
            }
        }

        mainAdapter.submitList(newStubList.toList())
        updateMemoryStubByNewList(newStubList)
    }

    private fun addReactionToMessage(messageModelUi: MessageModelUi, emojiUi: EmojiUi) {
        val memoryStubList: List<DelegateItem> = listOfMessagesStub
        val newStubList: MutableList<DelegateItem> = mutableListOf<DelegateItem>()

        val newStubOfReactions: MutableList<ReactionUi> = mutableListOf()
        memoryStubList.forEach { delegate ->
            val delegateContent = delegate.content()
            if (delegateContent is MessageModelUi) {
                if (delegate.id() == messageModelUi.id) {
                    messageModelUi.listOfReactions.forEach { reaction ->
                        newStubOfReactions.add(reaction)
                    }
                    val reaction = messageModelUi.listOfReactions.find { it.userId == messageModelUi.myId && it.emojiUi.emojiCode == emojiUi.emojiCode }
                    if (reaction != null) {
                        Log.d("addReactionToMessage", "$reaction") // содержит выбранную Reaction, skipp
                    } else {
                        newStubOfReactions.add(ReactionUi(emojiUi = emojiUi, userId = messageModelUi.myId))
                    }
                    newStubList.add(MessageDelegateItem(id = messageModelUi.id, messageModelUi.copy(listOfReactions = newStubOfReactions.toList())))
                } else {
                    newStubList.add(delegate)
                }
            } else {
                // TODO
            }
        }
        mainAdapter.submitList(newStubList.toList())
        updateMemoryStubByNewList(newStubList)
    }

    private fun setUpRecyclerView() {
        mainAdapter = MainAdapter()
        mainAdapter.addDelegate(MessageDelegate(onMessageListener, onEmojiListener))
        recyclerView = binding.recyclerView
        recyclerView.apply {
            adapter = mainAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
    }

    private fun fillUserLayout() {
        binding.stream.text = args.streamModel.title
        binding.topic.text = resources.getString(R.string.topic, args.topicModel.title)
        Log.d("MessageFragment", "${args.streamModel} ${args.topicModel}")
    }

    private fun observerButton() {
        binding.backArrow.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.inputField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (binding.inputField.text.toString().isEmpty()) {
                    binding.sendButtton.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.cross_circle))
                } else {
                    binding.sendButtton.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.paper_plane))
                }
            }
            override fun afterTextChanged(superfBatie: Editable?) {}
        })
        binding.sendButtton.setOnClickListener {
            if(binding.inputField.text.toString().trim().isNotEmpty()) {
                addMessageToMemoryMessageStub(binding.inputField.text.toString())
            }
            hideKeyboard()
            binding.inputField.setText("")
        }
    }

    private fun addMessageToMemoryMessageStub(message: String) {
        val memoryStubList: List<DelegateItem> = listOfMessagesStub
        val newStubList = mutableListOf<DelegateItem>()
        memoryStubList.forEach {
            newStubList.add(it)
        }
        newStubList.add(MessageDelegateItem(id = memoryStubList.size, MessageModelUi(memoryStubList.size, myIdStub, myIdStub, myNameStub, message, true, emptyList())))
        mainAdapter.submitList(newStubList.toList())
        updateMemoryStubByNewList(newStubList)
    }

    private fun updateMemoryStubByNewList(newList: List<DelegateItem>) {
        Log.d("updateMemoryStubByNewList", "${(newList[newList.size - 1] as MessageDelegateItem).id}")
        listOfMessagesStub = newList
    }

    private fun hideKeyboard() {
        val inputManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
            binding.inputField.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mainAdapter.unregisterAdapterDataObserver(observerRecyclerView)
        _binding = null
    }

    private val myIdStub: String = "3775"
    private val myNameStub: String = "Alex"

    private val anotherUserIdStub: String = "9674"
    private val anotherUserNameStub: String = "Alena"

    private val listOfReactionStub1 = listOf<ReactionUi>(
        ReactionUi(EmojiUi("grinning", "1f600"), myIdStub),
        ReactionUi(EmojiUi("grinning", "1f600"), anotherUserIdStub),
        ReactionUi(EmojiUi("big_smile", "1f604"), myIdStub),
        ReactionUi(EmojiUi("grinning_face_with_smiling_eyes", "1f601"), anotherUserIdStub),
        ReactionUi(EmojiUi("laughing", "1f606"), anotherUserIdStub),
        ReactionUi(EmojiUi("wink", "1f609"), anotherUserIdStub))

    private val listOfReactionStub2 = listOf<ReactionUi>(
        ReactionUi(EmojiUi("grinning", "1f600"), myIdStub),
        ReactionUi(EmojiUi("grinning", "1f600"), anotherUserIdStub),
        ReactionUi(EmojiUi("big_smile", "1f604"), myIdStub),
        ReactionUi(EmojiUi("grinning_face_with_smiling_eyes", "1f601"), anotherUserIdStub),
        ReactionUi(EmojiUi("laughing", "1f606"), anotherUserIdStub),
        ReactionUi(EmojiUi("wink", "1f609"), anotherUserIdStub),
        ReactionUi(EmojiUi("upside_down", "1f643"), anotherUserIdStub))

    private var listOfMessagesStub = listOf<DelegateItem>(
        MessageDelegateItem(id = 0, MessageModelUi(0, myIdStub, myId = myIdStub, myNameStub, "Hello World:!", true, listOfReactionStub1)),
        MessageDelegateItem(id = 1, MessageModelUi(1, myIdStub, myId = myIdStub, myNameStub, "Hello World:! Hello World:! Hello World:!Hello World:! Hello World:! Hello World:! Hello World:!", true, emptyList<ReactionUi>())),
        MessageDelegateItem(id = 2, MessageModelUi(2, anotherUserIdStub, myId = myIdStub, anotherUserNameStub, "Hello World:!", false, listOfReactionStub2)),
        MessageDelegateItem(id = 3, MessageModelUi(3, myIdStub, myId = myIdStub, myNameStub, "Hello World:! Hello World:! Hello World:!Hello World:! Hello World:! Hello World:! Hello World:!", true, emptyList<ReactionUi>())),
        MessageDelegateItem(id = 4, MessageModelUi(4, anotherUserIdStub, myId = myIdStub, anotherUserNameStub, "Hello World:! Hello World:! Hello World:!Hello World:! Hello World:! Hello World:! Hello World:!", false, emptyList<ReactionUi>()))
    )
}