package com.tinkoff.education.fragmentshomework4.features

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tinkoff.education.fragmentshomework4.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}