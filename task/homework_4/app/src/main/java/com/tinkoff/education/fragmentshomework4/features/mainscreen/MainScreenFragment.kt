package com.tinkoff.education.fragmentshomework4.features.mainscreen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.tinkoff.education.fragmentshomework4.R
import com.tinkoff.education.fragmentshomework4.databinding.FragmentMainScreenBinding

class MainScreenFragment : Fragment() {

    private var _binding: FragmentMainScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBottomNavigationView()
    }

    private fun setUpBottomNavigationView() {
        val hostFragment = childFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = hostFragment.navController
        binding.bottomNavigationView.setupWithNavController(navController)
        setUpVisibilityNavigationElements(navController)
    }

    private fun setUpVisibilityNavigationElements(navController: NavController) {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.messageFragment -> {
                    binding.bottomNavigationView.visibility = View.GONE
                    requireActivity().window.statusBarColor =  ContextCompat.getColor(requireContext(), R.color.teal)
                }
                R.id.anotherProfileFragment -> {
                    binding.bottomNavigationView.visibility = View.GONE
                    requireActivity().window.statusBarColor =  ContextCompat.getColor(requireContext(), R.color.grey)
                }
                else -> {
                    binding.bottomNavigationView.visibility = View.VISIBLE
                    requireActivity().window.statusBarColor =  ContextCompat.getColor(requireContext(), R.color.grey)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}