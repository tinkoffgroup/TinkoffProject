package com.tinkoff.education.fragmentshomework4.features.message.delegate.message

import com.tinkoff.education.fragmentshomework4.features.message.model.ReactionUi

data class MessageModelUi(
    val id: Int,
    val userId: String,
    val myId: String,
    val userName: String,
    val userMessage: String,
    val isMyMessage: Boolean,
    val listOfReactions: List<ReactionUi> = emptyList()
)
