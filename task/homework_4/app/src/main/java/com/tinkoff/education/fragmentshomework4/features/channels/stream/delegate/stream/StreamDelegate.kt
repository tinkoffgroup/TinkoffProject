package com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fragmentshomework4.R
import com.tinkoff.education.fragmentshomework4.databinding.StreamItemBinding
import com.tinkoff.education.fragmentshomework4.features.util.delegate.AdapterDelegate
import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem

class StreamDelegate(
    private val streamDetailsDListener: (
        streamModelUi: StreamModelUi
    ) -> Unit
) : AdapterDelegate {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(
            StreamItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            streamDetailsDListener
        )

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: DelegateItem,
        position: Int
    ) {
        (holder as ViewHolder).bind(item.content() as StreamModelUi)
    }

    override fun isOfViewType(item: DelegateItem): Boolean =
        item is StreamDelegateItem

    class ViewHolder(
        private val binding: StreamItemBinding,
        private val streamDetailsDListener: (
            streamModelUi: StreamModelUi
        ) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: StreamModelUi) {
            with(binding) {
                title.text = model.title

                if (model.isOpened) {
                    binding.button.setImageDrawable(AppCompatResources.getDrawable(itemView.context, R.drawable.ic_arrow_opened))
                } else {
                    binding.button.setImageDrawable(AppCompatResources.getDrawable(itemView.context, R.drawable.ic_arrow_closed))
                }

                itemView.setOnClickListener {
                    streamDetailsDListener.invoke(model)
                }
            }
        }
    }
}