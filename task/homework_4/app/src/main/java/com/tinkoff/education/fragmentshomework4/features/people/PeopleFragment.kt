package com.tinkoff.education.fragmentshomework4.features.people

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fragmentshomework4.R
import com.tinkoff.education.fragmentshomework4.databinding.FragmentPeopleBinding
import com.tinkoff.education.fragmentshomework4.features.people.model.UserUi
import com.tinkoff.education.fragmentshomework4.features.people.recyclerview.UserAdapter

class PeopleFragment : Fragment() {

    private var _binding: FragmentPeopleBinding? = null
    private val binding get() = _binding!!

    private val userDetailListener: (
        userUi: UserUi
    ) -> Unit = { userUi ->
        val action = PeopleFragmentDirections.actionPeopleFragmentToAnotherProfileFragment(userUi)
        findNavController().navigate(action)
    }
    private lateinit var userAdapter: UserAdapter
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPeopleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        observerToolBar()
        fillRecyclerView()
    }

    private fun fillRecyclerView() {
        userAdapter.submitList(listOfUsers)
    }

    private val listOfUsers = listOf<UserUi>(
        UserUi(1, "User 1", "user1@gmail.com", "https://i.ytimg.com/vi/YCaGYUIfdy4/maxresdefault.jpg"),
        UserUi(2, "User 2", "user2@gmail.com", "https://i.ytimg.com/vi/YCaGYUIfdy4/maxresdefault.jpg"),
        UserUi(3, "User 3", "user3@gmail.com", "https://i.ytimg.com/vi/YCaGYUIfdy4/maxresdefault.jpg"),
        )

    private fun setUpRecyclerView() {
        userAdapter = UserAdapter(userDetailListener)
        recyclerView = binding.recyclerView
        recyclerView.apply {
            adapter = userAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observerToolBar() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.tool_bar, menu)
            }
            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_search -> {
                        val searchView = menuItem.actionView as? SearchView
                        searchView?.queryHint = getText(R.string.users)
                        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                            override fun onQueryTextSubmit(p0: String?): Boolean {
                                searchView.clearFocus()
                                return true
                            }
                            override fun onQueryTextChange(newText: String?): Boolean {
                                if (newText != null) {
                                    searchUsers(newText)
                                }
                                return true
                            }
                        })
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun searchUsers(userName: String) {
        // TODO
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}