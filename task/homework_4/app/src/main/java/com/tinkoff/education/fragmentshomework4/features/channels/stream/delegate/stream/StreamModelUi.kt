package com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream

import android.os.Parcelable
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic.TopicModelUi
import kotlinx.parcelize.Parcelize

@Parcelize
data class StreamModelUi(
    val id: Int,
    val title: String,
    val isOpened: Boolean,
    val topics : List<TopicModelUi>
): Parcelable
