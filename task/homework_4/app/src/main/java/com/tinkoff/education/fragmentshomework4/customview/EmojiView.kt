package com.tinkoff.education.fragmentshomework4.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.text.TextPaint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.core.content.withStyledAttributes
import com.tinkoff.education.fragmentshomework4.R

class EmojiView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0,
    defTheme: Int = 0,
) : View(context, attributeSet, defStyle, defTheme) {

    var emoji: String = "\uD83D\uDE0E"
        set(value) {
            if (field != value) { // перерисовать если изменилось
                field = value
                requestLayout()
            }
        }

    var emojiCounter: Int = 0
        set(value) {
            if (field != value) { // перерисовать если изменилось
                field = value
                requestLayout()
            }
        }

    init {
        context.withStyledAttributes(attributeSet, R.styleable.EmojiView) {
            emoji = getString(R.styleable.EmojiView_emoji) ?: "\uD83D\uDE0E"
            emojiCounter = getInt(R.styleable.EmojiView_emojiCounter, 0)
        }
    }

    private val textToDraw: String
        get() {
            if (emojiCounter == 1) {
                return emoji
            }
            return if (emojiCounter != 0) "$emoji $emojiCounter"
            else emoji
        }

    private val textPaint = TextPaint().apply {
        color = Color.BLACK
        textSize = 14f.toSp(context) // пиксили в sp
    }

    private val textRect = Rect()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        textPaint.getTextBounds(textToDraw, 0, textToDraw.length, textRect)

        val actualWidth = resolveSize(paddingLeft + paddingRight + textRect.width(), widthMeasureSpec)
        val actualHeight = resolveSize(paddingTop + paddingBottom + textRect.height(), heightMeasureSpec)
        setMeasuredDimension(actualWidth, actualHeight)
    }

    override fun onDraw(canvas: Canvas) {
        val topOffset = (paddingTop - paddingBottom + height) / 2 - textRect.exactCenterY()
        canvas.drawText(textToDraw, paddingLeft.toFloat(), topOffset, textPaint)
    }

    // пиксили в sp
    private fun Float.toSp(context: Context) = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_SP, this, context.resources.displayMetrics
    )
}