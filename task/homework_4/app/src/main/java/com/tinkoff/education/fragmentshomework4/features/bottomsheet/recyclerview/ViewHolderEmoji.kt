package com.tinkoff.education.fragmentshomework4.features.bottomsheet.recyclerview

import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fragmentshomework4.databinding.EmojiItemBinding
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.EmojiUi

class ViewHolderEmoji(private val binding: EmojiItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(emoji: EmojiUi) {
        binding.emoji.text = emoji.getCodeString()
    }
}