package com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream

import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem

class StreamDelegateItem(
    val id: Int,
    private val streamModelUi: StreamModelUi
) : DelegateItem {
    override fun content(): Any = streamModelUi
    override fun id(): Int = streamModelUi.id

    override fun compareToOther(other: DelegateItem): Boolean {
        return (other as StreamDelegateItem).streamModelUi == this.streamModelUi
    }
}