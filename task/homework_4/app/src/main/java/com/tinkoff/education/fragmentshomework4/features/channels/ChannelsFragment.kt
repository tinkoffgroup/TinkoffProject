package com.tinkoff.education.fragmentshomework4.features.channels

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import com.tinkoff.education.fragmentshomework4.databinding.FragmentChannelsBinding
import com.tinkoff.education.fragmentshomework4.features.channels.stream.StreamFragment
import com.tinkoff.education.fragmentshomework4.features.channels.pager.PagerAdapter
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream.StreamModelUi
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic.TopicModelUi

class ChannelsFragment : Fragment() {

    private var _binding: FragmentChannelsBinding? = null
    private val binding get() = _binding!!

    private lateinit var pagerAdapter: PagerAdapter

    private val openMessage: (
        streamModelUi: StreamModelUi,
        topicModelUi: TopicModelUi
    ) -> Unit = { streamModel, topicModel ->
        val action = ChannelsFragmentDirections.actionChannelsFragmentToMessageFragment(streamModel, topicModel)
        findNavController().navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChannelsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpPagerAdapter()
    }

    private fun setUpPagerAdapter() {
        pagerAdapter = PagerAdapter(requireActivity().supportFragmentManager, lifecycle)
        binding.streamPager.adapter = pagerAdapter
        pagerAdapter.update(
            listOf(
                StreamFragment.createInstance(amISubscribed = true, openMessage),
                StreamFragment.createInstance(amISubscribed = false, openMessage)
            )
        )

        val tabs = listOf("Subscribed", "All Streams")

        TabLayoutMediator(binding.tabs, binding.streamPager) {tab, position ->
            tab.text = tabs[position]
        }.attach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}