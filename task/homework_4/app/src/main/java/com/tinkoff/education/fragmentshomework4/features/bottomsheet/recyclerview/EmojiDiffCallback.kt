package com.tinkoff.education.fragmentshomework4.features.bottomsheet.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.tinkoff.education.fragmentshomework4.features.bottomsheet.model.EmojiUi

class EmojiDiffCallback : DiffUtil.ItemCallback<EmojiUi>() {

    override fun areItemsTheSame(oldItem: EmojiUi, newItem: EmojiUi): Boolean {
        return oldItem === newItem
    }

    override fun areContentsTheSame(oldItem: EmojiUi, newItem: EmojiUi): Boolean {
        return oldItem == newItem
    }
}