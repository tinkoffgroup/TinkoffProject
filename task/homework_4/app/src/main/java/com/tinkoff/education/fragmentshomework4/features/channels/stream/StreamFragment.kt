package com.tinkoff.education.fragmentshomework4.features.channels.stream

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tinkoff.education.fragmentshomework4.databinding.FragmentStreamBinding
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream.StreamDelegate
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream.StreamDelegateItem
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.stream.StreamModelUi
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic.TopicDelegate
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic.TopicDelegateItem
import com.tinkoff.education.fragmentshomework4.features.channels.stream.delegate.topic.TopicModelUi
import com.tinkoff.education.fragmentshomework4.features.util.delegate.DelegateItem
import com.tinkoff.education.fragmentshomework4.features.util.delegate.MainAdapter

class StreamFragment : Fragment() {

    private var _binding: FragmentStreamBinding? = null
    private val binding get() = _binding!!

    private lateinit var mainAdapter: MainAdapter
    private lateinit var recyclerView: RecyclerView

    private val streamDetailsListener: (
        streamModelUi: StreamModelUi
    ) -> Unit = {
        streamModel ->
        replaceListDelegateById(streamModel)
    }

    private var amISubscribed: Boolean = true
    private var topicDetailsListener: (
        topicModel: TopicModelUi
    ) -> Unit = { topicModel ->
        openMessage(getStreamFromMemoryByTopic(topicModel), topicModel)
    }

    private fun getStreamFromMemoryByTopic(topicModel: TopicModelUi): StreamModelUi {
        val memoryStubList: List<DelegateItem> = getMemoryStubList()
        memoryStubList.forEach {
            val content = it.content()
            if (content is StreamModelUi) {
                if (content.topics.contains(topicModel)) {
                    return content
                }
            }
        }
        throw UnsupportedOperationException()
    }

    private lateinit var openMessage: (
        streamModelUi: StreamModelUi,
        topicModelUi: TopicModelUi
    ) -> Unit

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStreamBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        fillRecyclerView(amISubscribed)
    }

    private fun setUpRecyclerView() {
        mainAdapter = MainAdapter()
        mainAdapter.addDelegate(StreamDelegate(streamDetailsListener))
        mainAdapter.addDelegate(TopicDelegate(topicDetailsListener))
        recyclerView = binding.recyclerView
        recyclerView.apply {
            adapter = mainAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
    }

    private fun fillRecyclerView(amISubscribed: Boolean) {
        if (amISubscribed) {
            mainAdapter.submitList(listOfStreamIfIAmSubscribed)
        } else {
            mainAdapter.submitList(listOfStreamIfIAmNotSubscribed)
        }
    }

    private fun replaceListDelegateById(streamModelUi: StreamModelUi) {
        val memoryStubList: List<DelegateItem> = getMemoryStubList()
        val newStubList = mutableListOf<DelegateItem>()

        memoryStubList.forEach {
            val content = it.content()
            if (content is StreamModelUi) {
                if (it.id() == streamModelUi.id) {
                    val newStreamModel = streamModelUi.copy(isOpened = !streamModelUi.isOpened)
                    newStubList.add(StreamDelegateItem(id = streamModelUi.id, newStreamModel))
                    if (!streamModelUi.isOpened) {
                        newStreamModel.topics.forEach { topic ->
                            newStubList.add(TopicDelegateItem(id = topic.id, topic))
                        }
                    }
                } else {
                    newStubList.add(it)
                    if (content.isOpened) {
                        content.topics.forEach { topic ->
                            newStubList.add(TopicDelegateItem(id = topic.id, topic))
                        }
                    }
                }
            }
        }
        mainAdapter.submitList(newStubList.toList())
        updateMemoryStubByNewList(newStubList)
    }

    private fun getMemoryStubList(): List<DelegateItem> {
        return if (amISubscribed) {
            listOfStreamIfIAmSubscribed
        } else {
            listOfStreamIfIAmNotSubscribed
        }
    }

    private fun updateMemoryStubByNewList(newList: List<DelegateItem>) {
        if (amISubscribed) {
            listOfStreamIfIAmSubscribed = newList
        } else {
            listOfStreamIfIAmNotSubscribed = newList
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private val listOfTopicsByStreamId = mapOf(
        1 to listOf<TopicModelUi>(
            TopicModelUi(8, "#TopicModel 1", 12),
            TopicModelUi(9, "#TopicModel 2", 12),
            TopicModelUi(10, "#TopicModel 3", 12),),

        2 to listOf<TopicModelUi>(
            TopicModelUi(11, "#TopicModel 4", 12),
            TopicModelUi(12, "#TopicModel 5", 12),
            TopicModelUi(13, "#TopicModel 6", 12),),

        3 to listOf<TopicModelUi>(
            TopicModelUi(14, "#TopicModel 7", 12),
            TopicModelUi(15, "#TopicModel 8", 12),
            TopicModelUi(16, "#TopicModel 9", 12)),

        4 to listOf<TopicModelUi>(
            TopicModelUi(17, "#TopicModel 10", 12),
            TopicModelUi(18, "#TopicModel 11", 12),
            TopicModelUi(19, "#TopicModel 12", 12),),

        5 to listOf<TopicModelUi>(
            TopicModelUi(20, "#TopicModel 13", 12),
            TopicModelUi(21, "#TopicModel 14", 12),
            TopicModelUi(22, "#TopicModel 15", 12),),

        6 to listOf<TopicModelUi>(
            TopicModelUi(23, "#TopicModel 16", 12),
            TopicModelUi(24, "#TopicModel 17", 12),
            TopicModelUi(25, "#TopicModel 18", 12)),

        7 to listOf<TopicModelUi>(
            TopicModelUi(26, "#TopicModel 19", 12),
            TopicModelUi(27, "#TopicModel 20", 12),
            TopicModelUi(28, "#TopicModel 21", 12)))

    private var listOfStreamIfIAmSubscribed = listOf<DelegateItem>(
        StreamDelegateItem(id = 1, StreamModelUi(id = 1,"StreamModel 1", false,
            topics = listOfTopicsByStreamId[1]!!)),
        StreamDelegateItem(id = 2, StreamModelUi(id = 2,"StreamModel 2", false,
            topics = listOfTopicsByStreamId[2]!!)),
        StreamDelegateItem(id = 3, StreamModelUi(id = 3,"StreamModel 3", false,
            topics = listOfTopicsByStreamId[3]!!)))

    private var listOfStreamIfIAmNotSubscribed = listOf<DelegateItem>(
        StreamDelegateItem(id = 1, StreamModelUi(id = 1,"StreamModel 1", false,
            topics = listOfTopicsByStreamId[1]!!)),
        StreamDelegateItem(id = 2, StreamModelUi(id = 2,"StreamModel 2", false,
            topics = listOfTopicsByStreamId[2]!!)),
        StreamDelegateItem(id = 3, StreamModelUi(id = 3,"StreamModel 3", false,
            topics = listOfTopicsByStreamId[3]!!)),
        StreamDelegateItem(id = 4, StreamModelUi(id = 4,"StreamModel 4", false,
            topics = listOfTopicsByStreamId[4]!!)),
        StreamDelegateItem(id = 5, StreamModelUi(id = 5,"StreamModel 5", false,
            topics = listOfTopicsByStreamId[5]!!)),
        StreamDelegateItem(id = 6, StreamModelUi(id = 6,"StreamModel 6", false,
            topics = listOfTopicsByStreamId[6]!!)),
        StreamDelegateItem(id = 7, StreamModelUi(id = 7,"StreamModel 7", false,
            topics = listOfTopicsByStreamId[7]!!))
    )

    companion object {
        fun createInstance(
            amISubscribed: Boolean,
            openMessage: (
                streamModelUi: StreamModelUi,
                topicModelUi: TopicModelUi
            ) -> Unit
        ) = StreamFragment().apply {
            this.amISubscribed = amISubscribed
            this.openMessage = openMessage
        }
    }
}