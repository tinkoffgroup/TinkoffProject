package com.tinkoff.education.fragmentshomework4.features.people.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.tinkoff.education.fragmentshomework4.features.people.model.UserUi

class UserDiffCallback : DiffUtil.ItemCallback<UserUi>() {

    override fun areItemsTheSame(oldItem: UserUi, newItem: UserUi): Boolean {
        return oldItem === newItem
    }

    override fun areContentsTheSame(oldItem: UserUi, newItem: UserUi): Boolean {
        return oldItem == newItem
    }
}