package com.tinkoff.education.fragmentshomework4.features.anotherprofile

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.tinkoff.education.fragmentshomework4.databinding.FragmentAnotherProfileBinding
import com.tinkoff.education.fragmentshomework4.features.people.model.UserUi
import com.tinkoff.education.fragmentshomework4.features.util.extensions.loadImage

class AnotherProfileFragment : Fragment() {

    private var _binding: FragmentAnotherProfileBinding? = null
    private val binding get() = _binding!!

    private val args: AnotherProfileFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAnotherProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerButton()
        fillUserLayout(args.userUi)
    }

    private fun fillUserLayout(userUi: UserUi) {
        Log.d("AnotherProfile", "$userUi")
        binding.avatar.loadImage(userUi.userUrl)
        binding.userName.text = userUi.userName
    }

    private fun observerButton() {
        binding.backArrow.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}